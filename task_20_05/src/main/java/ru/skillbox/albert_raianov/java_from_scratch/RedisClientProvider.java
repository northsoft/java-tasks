package ru.skillbox.albert_raianov.java_from_scratch;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;

import java.util.LinkedList;

public class RedisClientProvider implements AutoCloseable {
    private static LinkedList<RedisClientProvider> selfInstances = new LinkedList<>();
    private static RedissonClient clientInstance = null;

    public RedissonClient getClient() {
        if (clientInstance == null) {
            Config config = new Config();
            config.useSingleServer().setAddress("redis://127.0.0.1:6379");
            clientInstance = Redisson.create(config);
        }
        return clientInstance;
    }
    public RedisClientProvider() {
        selfInstances.add(this);
    }

    @Override
    public void close() throws Exception {
        selfInstances.remove(this);
        if (selfInstances.isEmpty() && clientInstance != null) {
            clientInstance.shutdown();
            clientInstance = null;
        }
    }
}
