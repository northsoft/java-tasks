package ru.skillbox.albert_raianov.java_from_scratch;

import org.redisson.api.RScoredSortedSet;
import org.redisson.api.RedissonClient;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.Random;

public class Main {
    public static void main(String[] args) {
        reportCities();
        emulateDatingSite();
    }

    public static void reportCities() {
        try (RedisClientProvider redisProvider = new RedisClientProvider()) {
            RedissonClient redisson = redisProvider.getClient();
            RScoredSortedSet<String> cities = redisson.getScoredSortedSet("cities");
            cities.addAll(Map.of(
                    "Batumi", 6774.0,
                    "Belgrad", 11883.0,
                    "Berlin", 8324.0,
                    "Madrid", 10506.0,
                    "Tallinn", 12687.0,
                    "Tbilisi", 4937.0,
                    "Rome", 12170.0,
                    "Sofia", 10907.0,
                    "Varna", 14467.0
            ));
            System.out.println("Most cheap cities to visit:");
            cities.entryRange(Double.NEGATIVE_INFINITY, false, Double.POSITIVE_INFINITY, false, 0, 3)
                    .forEach(mostCheapEntry -> {
                        System.out.printf("%8.2f: %s%n", mostCheapEntry.getScore(), mostCheapEntry.getValue());
                    });
            System.out.println("Most expensive cities to visit:");
            cities.entryRangeReversed(Double.NEGATIVE_INFINITY, false, Double.POSITIVE_INFINITY, false, 0, 3)
                    .forEach(mostCheapEntry -> {
                        System.out.printf("%8.2f: %s%n", mostCheapEntry.getScore(), mostCheapEntry.getValue());
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void emulateDatingSite() {
        try (
            RedisClientProvider redisProvider = new RedisClientProvider();
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))
        ) {
            RScoredSortedSet<Integer> users = redisProvider.getClient().getScoredSortedSet("DatingUsers");
            Random random = new Random();
            final int nUsers = 20;
            for (int i = 0; i < nUsers; ++i) {
                users.add((double) System.currentTimeMillis(), i);
            }
            System.out.println("Press Enter to stop emulation");
            while (!reader.ready()) {
                int userId = users.first();
                if (random.nextInt(10) == 0) {
                    System.out.printf("User %d has purchased a subscription%n", userId);
                }
                System.out.printf("User %d is displayed%n", userId);
                users.add((double) System.currentTimeMillis(), userId);
                Thread.sleep(1000);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}