package org.example.app.services;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.example.web.dto.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class BookService {
    private final ProjectRepository<Book> bookRepo;
    private final Logger logger = LogManager.getLogger(BookService.class);

    @Autowired
    public BookService(ProjectRepository<Book> bookRepo) {
        this.bookRepo = bookRepo;
    }

    public List<Book> getAllBooks() {
        return bookRepo.retreiveAll();
    }

    public void saveBook(Book book) {
        bookRepo.store(book);
    }

    public boolean removeBookById(Integer bookIdToRemove) {
        return bookRepo.removeItemById(bookIdToRemove);
    }

    public int removeBookByRegex(String regex) {
        Pattern pattern = Pattern.compile(regex);
        logger.info("Regex \"" + regex + "\" compiled, " + getAllBooks().size() + " books in repo");

        List<Integer> idsToDelete = getAllBooks().stream()
                .filter(book -> {
                        return Stream.of(book.getAuthor(), book.getTitle(), book.getSize().toString())
                            .anyMatch(str -> pattern.matcher(str).find());
                })
                .map(Book::getId).collect(Collectors.toList());

        int result = idsToDelete.size();
        idsToDelete.forEach(this::removeBookById);
        return result;
    }

    public void defaultInit(){
        logger.info("default INIT in book service");
    }

    public void defaultDestroy(){
        logger.info("default DESTROY in book service");
    }
}
