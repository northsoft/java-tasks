package org.example.web.controllers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.example.app.services.BookService;
import org.example.web.dto.Book;
import org.example.web.dto.BookIdToRemove;
import org.example.web.dto.BookRegexToRemove;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;

@Controller
@RequestMapping(value = "/books")
@Scope("singleton")
public class BookShelfController {

    private final Logger logger = LogManager.getLogger(BookShelfController.class);
    private final BookService bookService;

    @Autowired
    public BookShelfController(BookService bookService) {
        this.bookService = bookService;
    }

    private void addToModelWithoutOverwrite(Model modelToAppendTo, String attributeName, Object objectToAppend) {
        if (!modelToAppendTo.containsAttribute(attributeName)) {
            modelToAppendTo.addAttribute(attributeName, objectToAppend);
        }
    }

    private void prepareModel(Model model) {
        addToModelWithoutOverwrite(model, "book",               new Book());
        addToModelWithoutOverwrite(model, "bookIdToRemove",     new BookIdToRemove());
        addToModelWithoutOverwrite(model, "bookRegexToRemove",  new BookRegexToRemove());
        addToModelWithoutOverwrite(model, "bookList",           bookService.getAllBooks());
    }

    @GetMapping("/shelf")
    public String books(Model model) {
        logger.info(this.toString());
        prepareModel(model);
        return "book_shelf";
    }

    @PostMapping("/save")
    public String saveBook(@Valid Book book, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("book", book);
            prepareModel(model);
            return "book_shelf";
        } else {
            logger.info("Requested to save: " + book.toString());
            bookService.saveBook(book);
            logger.info("current repository size: " + bookService.getAllBooks().size());
            return "redirect:/books/shelf";
        }
    }

    @PostMapping("/remove")
    public String removeBook(@Valid BookIdToRemove bookIdToRemove, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            prepareModel(model);
            return "book_shelf";
        }
        bookService.removeBookById(bookIdToRemove.getId());
        return "redirect:/books/shelf";
    }

    @PostMapping("/removeByRegex")
    public String removeByRegex(@Valid BookRegexToRemove bookRegexToRemove, BindingResult bindingResult, Model model) {
        logger.info("Requested removal by regex \"" + bookRegexToRemove.getRegex() + "\"");
        if (bindingResult.hasErrors()) {
            prepareModel(model);
            logger.info("Rejected because of validation failure");
            return "book_shelf";
        }
        try {
            int nBooksRemoved = bookService.removeBookByRegex(bookRegexToRemove.getRegex());
            logger.info(nBooksRemoved + " books removed");
            //model.addAttribute("books_removed_by_regex", nBooksRemoved);
            return "redirect:/books/shelf";
        } catch (Exception e) {
            logger.warn("error on regexp-remove: " + e.getMessage());
            prepareModel(model);
            model.addAttribute("remove_by_regex_error", e.getMessage());
            return "book_shelf";
        }
    }

    @PostMapping("/uploadFile")
    public String uploadFile(@RequestParam("file") MultipartFile file, Model model) throws Exception {

        if (file.isEmpty()) {
            logger.warn("file is empty");
            model.addAttribute("upload_error", "File is required to be uploaded");
            prepareModel(model);
            return "book_shelf";
        }
        String name = file.getOriginalFilename();
        byte[] bytes = file.getBytes();

        //create dir
        String rootPath = System.getProperty("catalina.home");
        File dir = new File(rootPath + File.separator + "external_uploads");
        if (!dir.exists()) {
            dir.mkdirs();
        }

        //create file
        File serverFile = new File(dir.getAbsolutePath() + File.separator + name);
        BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
        stream.write(bytes);
        stream.close();

        logger.info("new file saved at: " + serverFile.getAbsolutePath());

        return "redirect:/books/shelf";
    }
}
