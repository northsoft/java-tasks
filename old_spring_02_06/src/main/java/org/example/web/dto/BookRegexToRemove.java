package org.example.web.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class BookRegexToRemove {
    @NotNull
    @Size(min = 1, max = 200)
    private String regex;

    public String getRegex() {
        return regex;
    }

    public void setRegex(String regex) {
        this.regex = regex;
    }
}
