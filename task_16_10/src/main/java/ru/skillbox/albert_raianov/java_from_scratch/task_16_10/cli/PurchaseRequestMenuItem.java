package ru.skillbox.albert_raianov.java_from_scratch.task_16_10.cli;

import ru.skillbox.albert_raianov.java_from_scratch.task_16_10.dao.PurchaseListKeyPojo;
import ru.skillbox.albert_raianov.java_from_scratch.task_16_10.dao.PurchaseListPojo;

public class PurchaseRequestMenuItem extends DaoRequestMenuItem {
    @Override
    public String getCaption() {
        return "Request by purchase";
    }

    @Override
    public void doActionAfterCheck() throws Exception {
        System.out.print("Please type a student name: ");
        String studentName = scanner.nextLine();
        System.out.print("Please type a course name: ");
        String courseName = scanner.nextLine();
        PurchaseListKeyPojo key = new PurchaseListKeyPojo(studentName, courseName);
        PurchaseListPojo plp = session.get(PurchaseListPojo.class, key);
        if (plp == null) {
            System.out.println("Requested record was not found");
        } else {
            System.out.printf("Result:%n%s", plp.toString());
        }
    }
}
