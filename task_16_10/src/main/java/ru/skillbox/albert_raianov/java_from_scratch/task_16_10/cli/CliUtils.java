package ru.skillbox.albert_raianov.java_from_scratch.task_16_10.cli;

import java.util.Scanner;

public class CliUtils {
    public static int readIntByLine(Scanner scanner) {
        String line = scanner.nextLine();
        return new Scanner(line).nextInt();
    }

    public static String strNTimes(String str, int n) {
        if (n == 0) {
            return "";
        }
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < n; ++i) {
            stringBuilder.append(str);
        }
        return stringBuilder.toString();
    }
}
