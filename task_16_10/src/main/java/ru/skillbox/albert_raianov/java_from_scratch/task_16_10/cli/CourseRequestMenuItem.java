package ru.skillbox.albert_raianov.java_from_scratch.task_16_10.cli;

import ru.skillbox.albert_raianov.java_from_scratch.task_16_10.dao.CoursePojo;
import ru.skillbox.albert_raianov.java_from_scratch.task_16_10.dao.PurchaseListKeyPojo;
import ru.skillbox.albert_raianov.java_from_scratch.task_16_10.dao.PurchaseListPojo;

public class CourseRequestMenuItem extends DaoRequestMenuItem {
    @Override
    public String getCaption() {
        return "Request by course";
    }

    @Override
    public void doActionAfterCheck() throws Exception {
        System.out.print("Please type a course ID: ");
        int courseId = CliUtils.readIntByLine(scanner);
        CoursePojo course = session.get(CoursePojo.class, courseId);
        if (course == null) {
            System.out.println("Requested record was not found");
        } else {
            System.out.printf("Result:%n%s", course.toString());
        }
    }
}
