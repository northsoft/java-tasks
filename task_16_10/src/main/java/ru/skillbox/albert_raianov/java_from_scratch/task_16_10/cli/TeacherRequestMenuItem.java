package ru.skillbox.albert_raianov.java_from_scratch.task_16_10.cli;

import ru.skillbox.albert_raianov.java_from_scratch.task_16_10.dao.CoursePojo;
import ru.skillbox.albert_raianov.java_from_scratch.task_16_10.dao.TeacherPojo;

public class TeacherRequestMenuItem extends DaoRequestMenuItem {
    @Override
    public String getCaption() {
        return "Request by teacher";
    }

    @Override
    public void doActionAfterCheck() throws Exception {
        System.out.print("Please type a teacher ID: ");
        int teacherId = CliUtils.readIntByLine(scanner);
        TeacherPojo teacher = session.get(TeacherPojo.class, teacherId);
        if (teacher == null) {
            System.out.println("Requested record was not found");
        } else {
            System.out.printf("Result:%n%s", teacher.toString());
        }
    }
}
