package ru.skillbox.albert_raianov.java_from_scratch.task_16_10.dao;

import ru.skillbox.albert_raianov.java_from_scratch.task_16_10.cli.CliUtils;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Entity
@Table(name = "Courses")
public class CoursePojo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String name;

    private int duration;

    @Enumerated(EnumType.STRING)
    @Column(columnDefinition = "enum")
    private CourseType type;

    private String description;

    @ManyToOne(cascade = CascadeType.ALL)
    private TeacherPojo teacher;

    @Column(name = "students_count")
    private int studentsCount;

    private int price;

    @Column(name = "price_per_hour")
    private double pricePerHour;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "Subscriptions",
            joinColumns = {@JoinColumn(name = "course_id")},
            inverseJoinColumns = {@JoinColumn(name = "student_id")}
    )
    private List<StudentPojo> students;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "course_id")
    private List<SubscriptionPojo> subscriptions;

    public CoursePojo() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public CourseType getType() {
        return type;
    }

    public void setType(CourseType type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public TeacherPojo getTeacher() {
        return teacher;
    }

    public void setTeacher(TeacherPojo teacher) {
        this.teacher = teacher;
    }

    public int getStudentsCount() {
        return studentsCount;
    }

    public void setStudentsCount(int studentsCount) {
        this.studentsCount = studentsCount;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public double getPricePerHour() {
        return pricePerHour;
    }

    public void setPricePerHour(double pricePerHour) {
        this.pricePerHour = pricePerHour;
    }

    public List<StudentPojo> getStudents() {
        return students;
    }

    public void setStudents(List<StudentPojo> students) {
        this.students = students;
    }

    public List<SubscriptionPojo> getSubscriptions() {
        return subscriptions;
    }

    public void setSubscriptions(List<SubscriptionPojo> subscriptions) {
        this.subscriptions = subscriptions;
    }

    public String toString(int nTabs) {
        String tabs     = CliUtils.strNTimes("\t", nTabs);
        String moreTabs = CliUtils.strNTimes("\t", nTabs + 1);
        String ls = System.lineSeparator();
        String studentsStr;
        if (students.isEmpty()) {
            studentsStr = tabs + "students       = <no records>" + ls;
        } else {
            studentsStr = students.stream()
                    .map(studentPojo -> moreTabs + "\"" + studentPojo.getName() + "\"")
                    .collect(Collectors.joining(System.lineSeparator()));
            studentsStr =   tabs + "students       = [" + ls +
                            studentsStr                 + ls +
                            tabs + "]"                  + ls;
        }
        String subsStr;
        if (subscriptions.isEmpty()) {
            subsStr = tabs + "subscriptions  = <no records>" + ls;
        } else {
            subsStr = subscriptions.stream()
                    .map(subPojo -> String.format("%s%s: \"%s\" / \"%s\"",
                            moreTabs, subPojo.getSubscriptionDate(),
                            subPojo.getCourse().getName(), subPojo.getStudent().getName()))
                    .collect(Collectors.joining("\n"));

            subsStr =   tabs + "subscriptions  = {" + ls +
                        subsStr                     + ls +
                        tabs + "}"                  + ls;
        }
        return  tabs + "id             = "   + id            +        ls +
                tabs + "name           = \"" + name          + "\"" + ls +
                tabs + "duration       = "   + duration      +        ls +
                tabs + "type           = "   + type          +        ls +
                tabs + "description    = \"" + description   +        ls +
                tabs + "teacher        = {"                  +        ls +
                teacher.toString(nTabs + 1)                        +
                tabs + "}"                                   +        ls +
                tabs + "students_count = "   + studentsCount +        ls +
                studentsStr                                              +
                subsStr                                                  +
                tabs + "price          = "   + price         +        ls +
                tabs + "price_per_hour = "   + pricePerHour  +        ls;
    }

    @Override
    public String toString() {
        return toString(0);
    }
}
