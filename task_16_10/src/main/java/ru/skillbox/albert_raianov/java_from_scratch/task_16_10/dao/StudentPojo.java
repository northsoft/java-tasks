package ru.skillbox.albert_raianov.java_from_scratch.task_16_10.dao;

import ru.skillbox.albert_raianov.java_from_scratch.task_16_10.cli.CliUtils;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Entity
@Table(name = "Students")
public class StudentPojo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String name;

    private int age;

    @Column(name = "registration_date")
    private Date registrationDate;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "Subscriptions",
            joinColumns = {@JoinColumn(name = "student_id")},
            inverseJoinColumns = {@JoinColumn(name = "course_id")}
    )
    List<CoursePojo> courses;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "student_id")
    List<SubscriptionPojo> subscriptions;

    public StudentPojo() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Date getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    public List<CoursePojo> getCourses() {
        return courses;
    }

    public void setCourses(List<CoursePojo> courses) {
        this.courses = courses;
    }

    public List<SubscriptionPojo> getSubscriptions() {
        return subscriptions;
    }

    public void setSubscriptions(List<SubscriptionPojo> subscriptions) {
        this.subscriptions = subscriptions;
    }

    public String toString(int nTabs) {
        String tabs = CliUtils.strNTimes("\t", nTabs);
        String ls = System.lineSeparator();
        String coursesStr;
        String moreTabs = CliUtils.strNTimes("\t", nTabs + 1);
        if (courses.isEmpty()) {
            coursesStr = tabs + "courses  = <no records>" + ls;
        } else {
            coursesStr = courses.stream()
                    .map(coursePojo -> moreTabs + "\"" + coursePojo.getName() + "\"")
                    .collect(Collectors.joining(ls));
            coursesStr =    tabs + "courses  = [" + ls +
                            coursesStr                  + ls +
                            tabs + "]"                  + ls;
        }
        String subsStr;
        if (subscriptions.isEmpty()) {
            subsStr = tabs + "subscrs  = <no records>" + ls;
        } else {
            subsStr = subscriptions.stream()
                    .map(subPojo -> moreTabs + String.format("%s : %s / %s",
                            subPojo.getSubscriptionDate(), subPojo.getStudent().getName(), subPojo.getCourse().getName()))
                    .collect(Collectors.joining(ls));
            subsStr =   tabs + "subscrs  = [" + ls +
                        subsStr              + ls +
                        tabs + "]"           + ls;
        }
        return  tabs + "id       = "   + id               +        ls +
                tabs + "name     = \"" + name             + "\"" + ls +
                tabs + "age      = "   + age              +        ls +
                coursesStr                                           +
                subsStr                                              +
                tabs + "reg.date = "   + registrationDate +        ls;
    }

    @Override
    public String toString() {
        return toString(0);
    }
}
