package ru.skillbox.albert_raianov.java_from_scratch.task_16_10.cli;

import ru.skillbox.albert_raianov.java_from_scratch.task_16_10.dao.StudentPojo;
import ru.skillbox.albert_raianov.java_from_scratch.task_16_10.dao.TeacherPojo;

public class StudentRequestMenuItem extends DaoRequestMenuItem {
    @Override
    public String getCaption() {
        return "Request by student";
    }

    @Override
    public void doActionAfterCheck() throws Exception {
        System.out.print("Please type a student ID: ");
        int studentId = CliUtils.readIntByLine(scanner);
        StudentPojo student = session.get(StudentPojo.class, studentId);
        if (student == null) {
            System.out.println("Requested record was not found");
        } else {
            System.out.printf("Result:%n%s", student.toString());
        }
    }
}
