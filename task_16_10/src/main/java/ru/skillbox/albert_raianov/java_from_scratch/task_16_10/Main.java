package ru.skillbox.albert_raianov.java_from_scratch.task_16_10;

import org.hibernate.Session;
import ru.skillbox.albert_raianov.java_from_scratch.task_16_10.cli.*;
import ru.skillbox.albert_raianov.java_from_scratch.task_16_10.dao.SessionProvider;

import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        try (SessionProvider sessionProvider = new SessionProvider();
             Session session = sessionProvider.getFactory().openSession()
        ) {
            Scanner scanner = new Scanner(System.in);

            List<DaoRequestMenuItem> menuItems = List.of(
                    new SubscriptionRequestMenuItem(),
                    new     PurchaseRequestMenuItem(),
                    new      StudentRequestMenuItem(),
                    new      TeacherRequestMenuItem(),
                    new       CourseRequestMenuItem());
            menuItems.forEach(menuItem -> {
                menuItem.setScanner(scanner);
                menuItem.setSession(session);
            });
            Menu menu = new Menu(scanner, menuItems);
            menu.run();
        } catch (Exception e) {
            System.out.println("Error:");
            e.printStackTrace();
        }
    }
}
