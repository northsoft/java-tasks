package ru.skillbox.albert_raianov.java_from_scratch.task_16_10.dao;

import ru.skillbox.albert_raianov.java_from_scratch.task_16_10.cli.CliUtils;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "PurchaseList")
public class PurchaseListPojo {
    @EmbeddedId
    private PurchaseListKeyPojo id;

    @Column(name = "student_name", insertable = false, updatable = false)
    private String studentName;

    @Column(name = "course_name", insertable = false, updatable = false)
    private String courseName;

    @Column
    private int price;

    @Column(name = "subscription_date")
    private Date date;

    public PurchaseListPojo() { }

    public PurchaseListKeyPojo getId() {
        return id;
    }

    public void setId(PurchaseListKeyPojo id) {
        this.id = id;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String toString(int nTabs) {
        String tabs = CliUtils.strNTimes("\t", nTabs);
        String ls = System.lineSeparator();
        return  tabs + "studentName = " + studentName + ls +
                tabs + "courseName  = " + courseName  + ls +
                tabs + "date        = " + date        + ls +
                tabs + "price       = " + price       + ls;
    }

    @Override
    public String toString() {
        return toString(0);
    }
}
