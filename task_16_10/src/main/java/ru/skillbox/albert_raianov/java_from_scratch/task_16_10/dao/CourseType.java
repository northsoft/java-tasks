package ru.skillbox.albert_raianov.java_from_scratch.task_16_10.dao;

public enum CourseType {
    DESIGN,
    PROGRAMMING,
    MARKETING,
    MANAGEMENT,
    BUSINESS
}
