package ru.skillbox.albert_raianov.java_from_scratch.task_16_10.dao;

import ru.skillbox.albert_raianov.java_from_scratch.task_16_10.cli.CliUtils;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Entity
@Table(name = "Teachers")
public class TeacherPojo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String name;

    private int salary;

    private int age;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "teacher_id")
    List<CoursePojo> courses;

    public TeacherPojo() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public List<CoursePojo> getCourses() {
        return courses;
    }

    public void setCourses(List<CoursePojo> courses) {
        this.courses = courses;
    }

    public String toString(int nTabs) {
        String tabs = CliUtils.strNTimes("\t", nTabs);
        String ls = System.lineSeparator();
        String coursesStr;
        if (this.courses.isEmpty()) {
            coursesStr = tabs + "courses = <no records>" +  ls;
        } else {
            String moreTabs = CliUtils.strNTimes("\t", nTabs + 1);
            coursesStr = courses.stream()
                    .map(coursePojo -> moreTabs + "\"" + coursePojo.getName() + "\"")
                    .collect(Collectors.joining(ls));

            coursesStr =
                            tabs + "courses = {" + ls +
                            coursesStr           + ls +
                            tabs + "}"           + ls;
        }
        return  tabs + "id      = "   + id     +        ls +
                tabs + "name    = \"" + name   + "\"" + ls +
                tabs + "salary  = "   + salary +        ls +
                coursesStr                                 +
                tabs + "age     = "   + age    +        ls;
    }

    @Override
    public String toString() {
        return toString(0);
    }
}
