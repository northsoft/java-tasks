package ru.skillbox.albert_raianov.java_from_scratch.task_16_10.cli;

import org.hibernate.Session;

import java.util.Scanner;

public abstract class DaoRequestMenuItem implements MenuItem {
    protected Scanner scanner;
    protected Session session;

    public Scanner getScanner() {
        return scanner;
    }

    public void setScanner(Scanner scanner) {
        this.scanner = scanner;
    }

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    @Override
    public void doAction() throws Exception {
        if (scanner == null || session == null) {
            throw new Exception("Console scanner or database session are not ready");
        }
        doActionAfterCheck();
    }

    public abstract void doActionAfterCheck() throws Exception;
}
