package ru.skillbox.albert_raianov.java_from_scratch.task_16_10.dao;

import ru.skillbox.albert_raianov.java_from_scratch.task_16_10.cli.CliUtils;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "Subscriptions")
public class SubscriptionPojo {
    @EmbeddedId
    private SubscriptionKeyPojo id;

    /*
    @Column(name = "student_id", insertable = false, updatable = false)
    private int studentId;
     */

    @Column(name = "course_id", insertable = false, updatable = false)
    private int courseId;

    @Column(name = "subscription_date")
    private Date subscriptionDate;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "student_id", insertable = false, updatable = false)
    private StudentPojo student;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "course_id", insertable = false, updatable = false)
    private CoursePojo course;

    public SubscriptionPojo() {
    }

    public SubscriptionKeyPojo getId() {
        return id;
    }

    public void setId(SubscriptionKeyPojo id) {
        this.id = id;
    }

    public int getCourseId() {
        return courseId;
    }

    public void setCourseId(int courseId) {
        this.courseId = courseId;
    }

    public Date getSubscriptionDate() {
        return subscriptionDate;
    }

    public void setSubscriptionDate(Date subscriptionDate) {
        this.subscriptionDate = subscriptionDate;
    }

    public StudentPojo getStudent() {
        return student;
    }

    public void setStudent(StudentPojo student) {
        this.student = student;
    }

    public CoursePojo getCourse() {
        return course;
    }

    public void setCourse(CoursePojo course) {
        this.course = course;
    }

    public String toString(int nTabs) {
        String tabs = CliUtils.strNTimes("\t", nTabs);
        String ls = System.lineSeparator();
        return  tabs + "student   = {"                   + ls +
                student.toString(nTabs + 1)             +
                tabs + "}"                               + ls +
                tabs + "course    = {"                   + ls +
                course.toString(nTabs + 1)              +
                tabs + "}"                               + ls +
                tabs + "subs.Date = " + subscriptionDate + ls;
    }

    @Override
    public String toString() {
        return toString(0);
    }
}
