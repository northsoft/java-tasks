package ru.skillbox.albert_raianov.java_from_scratch.task_16_10.dao;

import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

public class SessionProvider implements AutoCloseable {
    private SessionFactory factory;

    public SessionFactory getFactory() {
        return factory;
    }

    public SessionProvider() {
        StandardServiceRegistryBuilder ssrb = new StandardServiceRegistryBuilder();
        StandardServiceRegistry ssr = ssrb.configure("hibernate.cfg.xml").build();
        MetadataSources ms = new MetadataSources(ssr);
        Metadata metadata = ms.getMetadataBuilder().build();
        factory = metadata.getSessionFactoryBuilder().build();
    }

    @Override
    public void close() throws Exception {
        factory.close();
    }
}

