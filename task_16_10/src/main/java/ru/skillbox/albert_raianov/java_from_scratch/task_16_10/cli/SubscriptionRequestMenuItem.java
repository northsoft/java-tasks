package ru.skillbox.albert_raianov.java_from_scratch.task_16_10.cli;

import ru.skillbox.albert_raianov.java_from_scratch.task_16_10.dao.PurchaseListKeyPojo;
import ru.skillbox.albert_raianov.java_from_scratch.task_16_10.dao.PurchaseListPojo;
import ru.skillbox.albert_raianov.java_from_scratch.task_16_10.dao.SubscriptionKeyPojo;
import ru.skillbox.albert_raianov.java_from_scratch.task_16_10.dao.SubscriptionPojo;

public class SubscriptionRequestMenuItem extends DaoRequestMenuItem {
    @Override
    public String getCaption() {
        return "Request by subscription";
    }

    @Override
    public void doActionAfterCheck() throws Exception {
        System.out.print("Please type a student id: ");
        int studentId = CliUtils.readIntByLine(scanner);
        System.out.print("Please type a course id: ");
        int courseId = CliUtils.readIntByLine(scanner);
        SubscriptionKeyPojo id = new SubscriptionKeyPojo(studentId, courseId);
        SubscriptionPojo subscription = session.get(SubscriptionPojo.class, id);
        if (subscription == null) {
            System.out.println("Requested record was not found");
        } else {
            System.out.printf("Result:%n%s", subscription.toString());
        }
    }
}
