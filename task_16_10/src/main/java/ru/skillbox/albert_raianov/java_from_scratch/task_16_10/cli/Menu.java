package ru.skillbox.albert_raianov.java_from_scratch.task_16_10.cli;

import java.util.*;

public class Menu {
    private ArrayList<MenuItem> items;
    private Scanner scanner;

    public Menu(Scanner scanner) {
        this.scanner = scanner;
    }

    public Menu(Scanner scanner, List<? extends MenuItem> menuItems) {
        this.scanner = scanner;
        this.items = new ArrayList<>(menuItems);
    }

    public void addItem(MenuItem item) {
        items.add(item);
    }

    private int readInt() throws Exception {
        int attempt = 0;
        final int maxAttempts = 3;
        do {
            try {
                String line = scanner.nextLine();
                int result = new Scanner(line).nextInt();
                if (result >= 0 && result <= items.size() + 1) {
                    return result;
                } else {
                    ++attempt;
                    System.out.printf("Input should be in range [1, %d]%n", items.size() + 1);
                }
            } catch (InputMismatchException e) {
                ++attempt;
                System.out.println("Unable to process integer input");
            }
        } while (attempt < maxAttempts);
        throw new InputMismatchException("Multiple failures in input processing");
    }

    public void run() {
        int exitChoice = items.size() + 1, currentChoice;
        do {
            System.out.println("Please type your choice:");
            for (int i = 0; i < items.size(); ++i) {
                System.out.printf("%d — %s%n", i + 1, items.get(i).getCaption());
            }
            System.out.printf("%d — exit%n", items.size() + 1);
            try {
                currentChoice = readInt();
            } catch (Exception e) {
                System.out.printf("Unable to read your choice: %s%n", e.getMessage());
                return;
            }
            if (currentChoice <= items.size()) {
                try {
                    items.get(currentChoice - 1).doAction();
                } catch (Exception e) {
                    System.out.println("Menu: unable to do selected action:");
                    e.printStackTrace();
                }
            }
        } while (currentChoice != exitChoice);
    }
}
