package ru.skillbox.albert_raianov.java_from_scratch.task_16_10.cli;

public interface MenuItem {
    String getCaption();
    void doAction() throws Exception;
}
