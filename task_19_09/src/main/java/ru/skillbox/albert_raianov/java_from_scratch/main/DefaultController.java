package ru.skillbox.albert_raianov.java_from_scratch.main;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.skillbox.albert_raianov.java_from_scratch.main.model.ToDoListItem;
import ru.skillbox.albert_raianov.java_from_scratch.main.model.ToDoListItemRepository;

import java.util.List;
import java.util.Random;

@Controller
public class DefaultController {
    @Autowired
    ToDoListItemRepository repo;
    @RequestMapping("/")
    public String index(Model model) {
        List<ToDoListItem> todoItems = ToDoListStorage.getInstance(repo).getAllBooks();
        model.addAttribute("todoItems", todoItems);

        return "index";
    }
}
