package ru.skillbox.albert_raianov.java_from_scratch.main;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ru.skillbox.albert_raianov.java_from_scratch.main.model.ToDoListItem;
import ru.skillbox.albert_raianov.java_from_scratch.main.model.ToDoListItemRepository;

import java.util.*;

@RestController
public class ToDoListItemsController {
    @Autowired
    private ToDoListItemRepository repo;
    @GetMapping("/ping")
    public int ping() {
        return new Random().nextInt();
    }

    private Map<ToDoListItemStatus, Integer> priority = Map.of(
            ToDoListItemStatus.ACTIVE, 0,
            ToDoListItemStatus.POSTPONED, 1,
            ToDoListItemStatus.COMPLETED, 3
    );

    @GetMapping("/todoListItems")
    public List<ToDoListItem> list() {
        return ToDoListStorage.getInstance(repo).getAllBooks().stream()
                .sorted((o1, o2) -> {
                    int p1 = priority.get(o1.getStatus());
                    int p2 = priority.get(o2.getStatus());
                    if (p1 != p2) {
                        return p1 - p2;
                    }
                    if (o1.getStatus() != ToDoListItemStatus.POSTPONED) {
                        return o1.getCreationTime().compareTo(o2.getCreationTime());
                    } else {
                        return o1.getReactivationTime().compareTo(o2.getReactivationTime());
                    }
                }).toList();
    }

    @GetMapping("/todoListItems/{id}")
    public ResponseEntity<ToDoListItem> getItem(@PathVariable int id) {
        Optional<ToDoListItem> optItem = ToDoListStorage.getInstance(repo).getItem(id);
        if (optItem.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        } else {
            return new ResponseEntity<>(optItem.get(), HttpStatus.OK);
        }
    }

    @PostMapping("/todoListItems")
    public ResponseEntity<Integer> addItem(ToDoListItem newItem) {
        if (newItem.getId() != null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
        ToDoListItem createdItem = ToDoListStorage.getInstance(repo).addItem(newItem);
        return new ResponseEntity<>(createdItem.getId(), HttpStatus.OK);
    }

    @DeleteMapping("/todoListItems/{id}")
    public ResponseEntity<Void> deleteItem(@PathVariable int id) {
        boolean deleteSuccess = ToDoListStorage.getInstance(repo).deleteItem(id);
        return new ResponseEntity<>(deleteSuccess ? HttpStatus.NO_CONTENT : HttpStatus.NOT_FOUND);
    }

    @PutMapping("/todoListItems")
    public ResponseEntity<Void> updateItem(ToDoListItem updatedItem) {
        try {
            ToDoListStorage.getInstance(repo).updateItem(updatedItem);
            return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
        } catch (NoSuchElementException e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }
}
