package ru.skillbox.albert_raianov.java_from_scratch.main;

import ru.skillbox.albert_raianov.java_from_scratch.main.model.ToDoListItem;
import ru.skillbox.albert_raianov.java_from_scratch.main.model.ToDoListItemRepository;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

public class ToDoListStorage {
    private ToDoListItemRepository repo;
    private static ToDoListStorage instance = null;
    private static final Object instanceLock = new Object();
    private final Object listLock = new Object();

    private ToDoListStorage(ToDoListItemRepository repo) {
        this.repo = repo;
        if (repo.count() == 0) {
            List<String> defaultTasks = List.of(
                    "Collect pants",
                    "...",
                    "Profit!"
                    // South Park is a registered trademark of Comedy Partners, NY
            );
            for (String defaultTask : defaultTasks) {
                int minusHours = defaultTasks.size() - defaultTasks.indexOf(defaultTask);
                addItem(new ToDoListItem(
                        null,
                        defaultTask,
                        ToDoListItemStatus.ACTIVE,
                        LocalDateTime.now().minus(minusHours, ChronoUnit.HOURS),
                        null
                ));
            }
        }
    }

    public static ToDoListStorage getInstance(ToDoListItemRepository repo) {
        synchronized (instanceLock) {
            if (instance == null) {
                instance = new ToDoListStorage(repo);
            }
            return instance;
        }
    }

    public List<ToDoListItem> getAllBooks() {
        Iterable<ToDoListItem> iterable = repo.findAll();
        List<ToDoListItem> result = new LinkedList<>();
        iterable.forEach(result::add);
        return result;
    }

    public Optional<ToDoListItem> getItem(int id) {
        synchronized (listLock) {
            return repo.findById(id);
        }
    }

    public ToDoListItem addItem(ToDoListItem newItem) {
        synchronized (listLock) {
            String newText = newItem.getText() != null ? newItem.getText() : "[empty]";
            ToDoListItemStatus newStatus = newItem.getStatus() != null ? newItem.getStatus() : ToDoListItemStatus.ACTIVE;
            LocalDateTime newCreationTime = newItem.getCreationTime() != null ? newItem.getCreationTime() : LocalDateTime.now();
            return repo.save(new ToDoListItem(null, newText, newStatus, newCreationTime, newItem.getReactivationTime()));
        }
    }

    public boolean deleteItem(int id) {
        synchronized (listLock) {
            if (repo.existsById(id)) {
                repo.deleteById(id);
                return true;
            } else {
                return false;
            }
        }
    }

    public void updateItem(ToDoListItem updatedItem) throws Exception {
        synchronized (listLock) {
            Optional<ToDoListItem> optOldItem = repo.findById(updatedItem.getId());
            if (optOldItem.isEmpty()) {
                throw new NoSuchElementException();
            }
            ToDoListItem oldItem = optOldItem.get();
            int changes = 0;
            if (updatedItem.getStatus() != null) {
                if (updatedItem.getStatus() != ToDoListItemStatus.POSTPONED) {
                    oldItem.setStatus(updatedItem.getStatus());
                    oldItem.setReactivationTime(null);
                } else {
                    if (updatedItem.getReactivationTime() == null ||
                            updatedItem.getReactivationTime().compareTo(LocalDateTime.now()) < 0) {
                        throw new IllegalArgumentException();
                    }
                    oldItem.setStatus(ToDoListItemStatus.POSTPONED);
                    oldItem.setReactivationTime(updatedItem.getReactivationTime());
                }
                ++changes;
            }
            if (updatedItem.getText() != null) {
                String newText = updatedItem.getText().trim();
                if (newText.isBlank()) {
                    throw new IllegalArgumentException();
                }
                oldItem.setText(newText);
                ++changes;
            }
            if (changes == 0) {
                throw new UnsupportedOperationException();
            }
            repo.save(oldItem);
        }
    }
}
