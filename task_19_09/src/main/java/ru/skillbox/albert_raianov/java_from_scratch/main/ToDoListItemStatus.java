package ru.skillbox.albert_raianov.java_from_scratch.main;

public enum ToDoListItemStatus {ACTIVE, COMPLETED, POSTPONED}
