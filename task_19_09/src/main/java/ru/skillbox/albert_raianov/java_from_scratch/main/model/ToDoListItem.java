package ru.skillbox.albert_raianov.java_from_scratch.main.model;

import ru.skillbox.albert_raianov.java_from_scratch.main.ToDoListItemStatus;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDateTime;

@Entity
public class ToDoListItem {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Integer id;
    String text;
    ToDoListItemStatus status;
    LocalDateTime creationTime;
    LocalDateTime reactivationTime;

    public ToDoListItem() { }

    public ToDoListItem(Integer id, String text, ToDoListItemStatus status,
                        LocalDateTime creationTime, LocalDateTime reactivationTime) {
        this.id = id;
        this.text = text;
        this.status = status == null ? ToDoListItemStatus.ACTIVE : status;
        this.creationTime = creationTime == null ? LocalDateTime.now() : creationTime;
        this.reactivationTime = reactivationTime;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public ToDoListItemStatus getStatus() {
        return status;
    }

    public void setStatus(ToDoListItemStatus status) {
        this.status = status;
    }


    public LocalDateTime getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(LocalDateTime creationTime) {
        this.creationTime = creationTime;
    }

    public LocalDateTime getReactivationTime() {
        return reactivationTime;
    }

    public void setReactivationTime(LocalDateTime reactivationTime) {
        this.reactivationTime = reactivationTime;
    }
}
