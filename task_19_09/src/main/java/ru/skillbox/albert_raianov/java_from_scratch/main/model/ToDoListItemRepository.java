package ru.skillbox.albert_raianov.java_from_scratch.main.model;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ToDoListItemRepository extends CrudRepository<ToDoListItem, Integer> {
}
