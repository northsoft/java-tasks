var noticeCss   = {'background-color':  'blue'};
var successCss  = {'background-color':  'green'};
var waitCss     = {'background-color':  'yellow'};
var errorCss    = {'background-color':  'red'};

var $pingJqxhr = null;
var $listJqxhr = null;
var $readJqxhr = null;
var readJqxhrId = null;
var addLastSubmission = 0;
var $createJqxhr = null;
var currentSelectedId = null;
var $deleteJqxhr = null;
var $putJqxhr = null;
var putLastSubmission = 0;

var classesByStatus = {
    'ACTIVE': 'active_task',
    'COMPLETED': 'completed_task',
    'POSTPONED': 'postponed_task'
};

$(function(){
    $("#alive_test").click(onAliveClick);
    $("#refresh").click(onRefreshClick);
    $('#add_item_submit').click(addTodoItemClick);
    $('#add_item_text').keyup(function(e){
        if (e.keyCode == 13) {
            addTodoItemClick();
        }
    });
    $('#single_todo_item_delete').click(deleteTodoItemClick);
    $('#single_todo_item_rename').click(renameTodoItemClick);
    $('#single_todo_item_rename_submit').click(renameSubmit);
    $('#single_todo_item_rename_new_text').keyup(function(e){
        if (e.keyCode == 13) {
            renameSubmit();
        }
        if (e.keyCode == 27) {
            renameCancelClick(true);
        }
        console.log('Pressed key ' + e.keyCode);
    });
    $('#single_todo_item_rename_cancel').click(renameCancelClick);
    $('#single_todo_item_mark_as_done').click(markAsDoneClick);
    $('#single_todo_item_reactivate').click(reactivateClick);
    $('#single_todo_item_postpone').click(postponeClick);
    $('#single_todo_item_postpone_till_date').keyup(function(e) {
        if (e.keyCode == 13) {
            postponeSubmit();
        }
        if (e.keyCode == 27) {
            postponeCancel();
        }
    });
    $('#single_todo_item_postpone_submit').click(postponeSubmit);
    $('#single_todo_item_postpone_cancel').click(postponeCancel);
});

function addTodoItem() {
    $addStatus = $("#add_item_status");
    var text = new String($("#add_item_text").val()).trim();
    if (text == '') {
        return;
    }
    if ($createJqxhr != null) {
        safeAbort($createJqxhr);
        $createJqxhr = null;
        $addStatus.css(noticeCss).text('Create request was aborted');
        return;
    }
    $createJqxhr = $.ajax({
        url: '/todoListItems',
        data: {
            'text': text
        },
        method: 'POST',
        error: function($jqXhr, status, error) {
            $createJqxhr = null;
            $addStatus.css(errorCss).text("Error processing request (" + textStatus + "): " + errorThrown);
        },
        success: function(data, status, $jqXhr) {
            $createJqxhr = null;
            $addStatus.css(successCss).text('Item added with ID ' + data);
            querySingleItem(data);
        }
    });
    $addStatus.css(waitCss).text('Creating…');
}

function addTodoItemClick() {
    $addStatus = $("#add_item_status");
    var secondsSinceLastSubmission = (Date.now() - addLastSubmission) / 1000;
    if (secondsSinceLastSubmission < 5) {
        var message = 'It\'s only ' + secondsSinceLastSubmission.toFixed(2) +
            ' seconds passed since last submissions';
        $addStatus.css(noticeCss).text(message);
    } else {
        addTodoItem();
        addLastSubmission = Date.now();
    }
    return false;
}

function querySingleItem(requestedId) {
    safeAbort($readJqxhr);
    readJqxhrId = requestedId;
    $readJqxhr = $.ajax({
        url: "/todoListItems/" + requestedId,
        error: function($jqXhr, textStatus, errorThrown){
            $readRequestStatus
                .css(errorCss)
                .text("Error processing request (" + textStatus + "): " + errorThrown);
            $readJqxhr = null;
            readJqxhrId = null;
        },
        success: function(data, textStatus, $jqXhr){
            $readJqxhr = null;
            readJqxhrId = null;
            try {
                $readRequestStatus.html('').css("background-color", "");
                currentSelectedId = data.id;
                $("#single_todo_item_id").text(data.id);
                $("#single_todo_item_text").text(data.text);
                switch (data.status) {
                case 'ACTIVE':
                case 'COMPLETED':
                    $("#single_todo_item_status").text(data.status);
                    break;
                case 'POSTPONED':
                    $("#single_todo_item_status").text('POSTPONED till ' + data.reactivationTime);
                }
                $("#single_todo_item_creation_date").text(data.creationTime);

                $("#single_todo_item_reactivate").css("display", data.status != 'ACTIVE' ? "" : "none");
                $("#single_todo_item_mark_as_done").css("display", data.status != 'COMPLETED' ? "" : "none");
                $("#single_todo_item_postpone").css("display", data.status != 'COMPLETED' ? "" : "none");
                $("#single_todo_item_contents").css("display", "");

                $("#add_item_status").text('').css('display', '');
                $('#single_todo_item_update_status').text('').css('display', '');
            } catch (e) {
                $readRequestStatus.css(errorCss).text("Error parsing response");
                console.log('Error parsing READ request: ' + e);
            }
        }
    });
}

function onTodoListItemClick(event) {
    var requestedId = $(event.target).attr('data-id');
    console.log('onTodoListItemClick: got ID ' + requestedId);
    $readRequestStatus = $("#read_request_status");
    if (readJqxhrId == requestedId && $readJqxhr != null) {
        safeAbort($readJqxhr);
        $readJqxhr = null;
        readJqxhrId = null;
        $readRequestStatus.css(noticeCss).text("Read operation was aborted");
    } else {
        querySingleItem(requestedId);
    }
    event.preventDefault();
    event.stopPropagation();
    return false;
}

function onAliveClick() {
    $pingAliveResponse = $("#alive_response");
    if ($pingJqxhr) {
        $pingJqxhr.abort();
        $pingJqxhr = null;
        $pingAliveResponse.css(noticeCss).text("Ping operation was aborted");
    } else {
        $pingAliveResponse.css(waitCss).text("Querying…");
        $pingJqxhr = $.ajax({
            url: "/ping",
            success: function(data, textStatus, $jqXhr){
                $pingAliveResponse.css(successCss).text("Server is alive (" + textStatus + "): " + data);
                $pingJqxhr = null;
            },
            error: function($jqXhr, textStatus, errorThrown){
                $pingAliveResponse
                    .css(errorCss)
                    .text("Error processing request (" + textStatus + "): " + errorThrown);
                $pingJqxhr = null;
            }
        });
    }
}

function onRefreshClick() {
    $todolist = $("#todolist");
    if ($listJqxhr != null) {
        $listJqxhr.abort();
        $todolist.css(noticeCss).text("List operation was aborted");
        $listJqxhr = null;
    } else {
        $todolist.css(waitCss).text("Refreshing…");
        $listJqxhr = $.ajax({
            url: "/todoListItems",
            error: function($jqXhr, textStatus, errorThrown){
                $listJqxhr = null;
                $todolist
                    .css(errorCss)
                    .text("Error processing request (" + textStatus + "): " + errorThrown);
            },
            success: function(data, textStatus, $jqXhr){
                $listJqxhr = null;
                try {
                    $todolist.html('').css("background-color", "");
                    for (var idx in data) {
                        var todoItemObj = data[idx];
                        var $div = $todolist.append('<div>');
                        var todoItemHtml =
                            '<a class="todo_item ' + classesByStatus[todoItemObj.status] +'" ' +
                            'data-id="' + todoItemObj.id + '" href="#">' +
                            todoItemObj.text + '</a>';
                        $div.append(todoItemHtml)
                            .unbind('click')
                            .on('click', onTodoListItemClick);
                    }
                    $("#single_todo_item_contents").css('display', 'none');
                } catch (e) {
                    $todolist.css(errorCss).text("Error parsing response");
                    console.log('Error parsing LIST request: ' + e);
                }
            }
        });
    }
}

function deleteTodoItemClick() {
    $status = $('#read_request_status');
    if (currentSelectedId == null) {
        return;
    }
    if ($deleteJqxhr != null) {
        safeAbort($deleteJqxhr);
        $deleteJqxhr = null;
        $status.css(noticeCss).text('Delete request aborted');
        return;
    }
    $deleteJqxhr = $.ajax({
        url: '/todoListItems/' + currentSelectedId,
        method: 'DELETE',
        error: function($jqXhr, status, error){
            $deleteJqxhr = null;
            $status.css(errorCss).text("Error processing request (" + textStatus + "): " + errorThrown);
        },
        success: function(data, status, $jqXhr){
            $deleteJqxhr = null;
            $status.css(successCss).text('Item with ID ' + currentSelectedId + ' was removed');
            onRefreshClick();
        }
    });
    $status.css(waitCss).text('Processing delete request…');
}

function renameTodoItemClick() {
    $('#single_todo_item_text').css('display', 'none');
    $('#single_todo_item_rename_block').css('display', '');
    $('#single_todo_item_rename_new_text').val($('#single_todo_item_text').text());
}

function renameCancelClick(needAbort = true) {
    $('#single_todo_item_text').css('display', '');
    $('#single_todo_item_rename_block').css('display', 'none');
    if (needAbort) {
        safeAbort($putJqxhr);
    }
}

/*
 putData = {
    statusElemId: string,
    data: {id: nonNullInteger; otherFields: values},
    errorCallback: optFunction,
    successCallback: optFunction(data)
 }
 */

function putDataError(putData) {
    if (typeof(putData) != 'object') {
        return 'typeof(putData) is ' + typeof(putData) + ', expected \'object\'';
    }
    if (typeof(putData.data) != 'object') {
        return 'typeof(putData.data) is ' + typeof(putData.data) + ', expected \'object\'';
    }
    var statusElemId = putData.statusElemId;
    if (typeof(statusElemId) != 'string') {
        return 'typeof(putData.statusElemId) is \'' + typeof(statusElemId) + '\', expected \'string\'';
    }
    var $statusElem = $('#' + statusElemId);
    var statusElemSize = $statusElem.length;
    if (statusElemSize != 1) {
        return '$(#' + statusElemId + ').length == ' + statusElemSize + ', expected 1';
    }
    var id = putData.data.id;
    if (typeof(id) != 'string' && typeof(id) != 'number') {
        return 'typeof(putData.data.id) is ' + typeof(id) + ', expected \'string\' or \'number\'';
    }
    if (typeof(id) == 'string' && isNaN(id)) {
        return 'putData.data.id == \'' + id + '\' and is not a number';
    }
    var callbacks = ['errorCallback', 'successCallback'];
    for (var callbackNameIdx in callbacks) {
        var callbackIdx = callbacks[callbackNameIdx];
        var callback = putData.data[callbackIdx];
        if (callback && typeof(callback) != 'function') {
            return 'putData.' + callbackIdx + ' exists and is \'' + typeof(callback) + '\', ' +
                'should be \'function\'';
        }
    }
    return null;
}

function putSubmit(putData) {
    var error = putDataError(putData);
    if (error != null) {
        console.log('putData error: ' + error);
        return;
    }
    var $status = $('#' + putData.statusElemId);
    var secondsSinceLastPutSubmission = (Date.now() - putLastSubmission) / 1000;
    if (secondsSinceLastPutSubmission < 5) {
        var msg = 'It\'s only ' + secondsSinceLastPutSubmission.toFixed(2) +
            ' seconds since last update query';
        $status.css(noticeCss).text(msg);
        return;
    }
    safeAbort($putJqxhr);
    curId = putData.data.id;
    $putJqxhr = $.ajax({
        'url':      '/todoListItems',
        'method':   'put',
        'data':     putData.data,
        'error':    function($jqXhr, textStatus, errorThrown) {
            $status.css(errorCss).text('Error processing update query (' + textStatus + '): ' + errorThrown);
            $putJqxhr = null;
            if (putData.errorCallback) {
                putData.errorCallback();
            }
        },
        'success':  function(data, status, $jqXhr) {
            $status.css(successCss).text('Item updated');
            if (putData.successCallback) {
                putData.successCallback();
            }
            querySingleItem(putData.data.id);
        }
    });
    putLastSubmission = Date.now();
    $status.css(waitCss).text('Updating…');
}

function renameSubmit() {
    putSubmit({
        'statusElemId': 'single_todo_item_update_status',
        'data': {
            'id': currentSelectedId,
            'text': $('#single_todo_item_rename_new_text').val()
         },
        'errorCallback': null,
        'successCallback': function(data) {
            $('#single_todo_item_text').css('display', '');
            $('#single_todo_item_rename_block').css('display', 'none');
        }
    });
}

function markAsDoneClick() {
    putSubmit({
        'statusElemId': 'single_todo_item_update_status',
        'data': {
            'id': currentSelectedId,
            'status': 'COMPLETED'
         },
        'errorCallback': null,
        'successCallback': null
    });
}

function reactivateClick() {
    putSubmit({
        'statusElemId': 'single_todo_item_update_status',
        'data': {
            'id': currentSelectedId,
            'status': 'ACTIVE'
         },
        'errorCallback': null,
        'successCallback': null
    });
}

function postponeClick() {
    $('#single_todo_item_postpone_form').css('display', '');
    var nowTimestamp = new Date().getTime();
    var nextTimeStamp = nowTimestamp + 24 * 3600 * 1000;
    var nextDate = new Date(nextTimeStamp);
    $('#single_todo_item_postpone_till_date').val(nextDate.toISOString());
}

function postponeCancel() {
    $('#single_todo_item_postpone_form').css('display', 'none');
}

function postponeSubmit() {
    var newDateText = $('#single_todo_item_postpone_till_date').val();
    if (isNaN(Date.parse(newDateText))) {
        $('#single_todo_item_update_status').css(errorCss).text('«' + newDateText + '» is not valid date');
        return;
    }
    var newIsoStr = new Date(Date.parse(newDateText)).toISOString();
    putSubmit({
        'statusElemId': 'single_todo_item_update_status',
        'data': {
            'id': currentSelectedId,
            'status': 'POSTPONED',
            'reactivationTime': newIsoStr
         },
        'errorCallback': null,
        'successCallback': function() {
            $('#single_todo_item_postpone_form').css('display', 'none');
        }
    });
}

function safeAbort($jqXhr) {
    if ($jqXhr && $jqXhr.abort && typeof($jqXhr.abort) == 'function') {
        $jqXhr.abort();
    }
}