package ru.skillbox.albert_raianov.java_from_scratch.task_15_14;

import ru.skillbox.albert_raianov.java_from_scratch.task_15_14.metro_map.dao.*;
import ru.skillbox.albert_raianov.java_from_scratch.task_15_14.metro_map.dao.dom.DomReader;
import ru.skillbox.albert_raianov.java_from_scratch.task_15_14.metro_map.dao.dumb.DumbWriter;
import ru.skillbox.albert_raianov.java_from_scratch.task_15_14.metro_map.dao.json.JsonReader;
import ru.skillbox.albert_raianov.java_from_scratch.task_15_14.metro_map.dao.json.JsonWriter;
import ru.skillbox.albert_raianov.java_from_scratch.task_15_14.metro_map.dao.json.SplitterForConnectedStations;
import ru.skillbox.albert_raianov.java_from_scratch.task_15_14.metro_map.domain.Line;
import ru.skillbox.albert_raianov.java_from_scratch.task_15_14.metro_map.domain.Metro;

import java.util.Comparator;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        try {
            demoHtmlToJson();
            demoJsonToOutput();
        } catch (Exception e) {
            System.out.println("It happened:");
            e.printStackTrace();
        }
    }

    public final static String PATH_TO_JSON = "src/main/resources/map.json";
    private static void demoHtmlToJson() throws Exception {
        PojoReader domReader = new DomReader("https://skillbox-java.github.io/");
        Metro metro = new Metro(domReader);
        PojoWriter jsonWriter = new JsonWriter(PATH_TO_JSON);
        metro.saveToPojoWriter(jsonWriter);
    }

    private static void demoJsonToOutput() throws Exception {
        PojoReader jsonReader = new JsonReader(PATH_TO_JSON);
        Metro metro = new Metro(jsonReader);
        for (Line line : metro.getLines()) {
            System.out.printf("Линия %s «%s»:%n", line.getId(), line.getName());
            line.getStations().stream()
                    .map(station -> String.format("%2d: %s", station.getIndex() + 1, station.getName()))
                    .forEach(System.out::println);
            System.out.println();
        }

        DumbWriter dumbWriter = new DumbWriter();
        metro.saveToPojoWriter(dumbWriter);
        SplitterForConnectedStations splitter = new SplitterForConnectedStations(dumbWriter.getLinePojo(), dumbWriter.getStationPojo());
        Set<Set<SingleStationPojo>> hubs = splitter.getDirectlyConnectedStations();
        System.out.printf("Всего %d узлов, на которых станции соединены каждая с каждой%n", hubs.size());
    }

    private static void dumpPojoReader(PojoReader pojoReader) {
        System.out.println("INFO: got lines:");
        pojoReader.readLinePojos().stream()
                .sorted(Comparator.comparing(LinePojo::getId))
                .map(LinePojo::getName)
                .forEach(System.out::println);
        System.out.println();

        System.out.println("INFO: got stations:");
        pojoReader.readStationPojos().stream()
                .sorted(Comparator.comparing(IndexedStationPojo::getLineId).thenComparing(IndexedStationPojo::getIndexInLine))
                .forEach(System.out::println);
        System.out.println();
    }
}
