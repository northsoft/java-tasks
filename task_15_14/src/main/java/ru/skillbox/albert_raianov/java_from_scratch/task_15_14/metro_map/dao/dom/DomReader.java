package ru.skillbox.albert_raianov.java_from_scratch.task_15_14.metro_map.dao.dom;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import ru.skillbox.albert_raianov.java_from_scratch.task_15_14.metro_map.dao.IndexedStationPojo;
import ru.skillbox.albert_raianov.java_from_scratch.task_15_14.metro_map.dao.LinePojo;
import ru.skillbox.albert_raianov.java_from_scratch.task_15_14.metro_map.dao.PojoReader;
import ru.skillbox.albert_raianov.java_from_scratch.task_15_14.metro_map.dao.SingleStationPojo;
import ru.skillbox.albert_raianov.java_from_scratch.task_15_14.util.SetUtils;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static java.util.Collections.unmodifiableSet;

public class DomReader implements PojoReader {
    private Set<LinePojo> parsedLines = new HashSet<>();
    private Set<IndexedStationPojo> parsedStations = new HashSet<>();

    @Override
    public Set<LinePojo> readLinePojos() {
        return unmodifiableSet(parsedLines);
    }

    @Override
    public Set<IndexedStationPojo> readStationPojos() {
        return unmodifiableSet(parsedStations);
    }

    public DomReader(String url) throws Exception {
        Logger logger = LogManager.getRootLogger();
        try {
            Document doc = Jsoup.connect(url).maxBodySize(0).get();
            Elements stationHeaders = doc.select("span.js-metro-line");
            for (Element singleStationHeader : stationHeaders) {
                processLine(singleStationHeader);
            }
        } catch (Exception e) {
            throw new Exception("Unable to process DOM DAO read call", e);
        }
    }

    private void processLine(Element singleLineHeader) {
        validateLineHeaderSpan(singleLineHeader);
        String lineName = singleLineHeader.text();
        String lineId = singleLineHeader.attr("data-line");
        if (lineName.isBlank() || lineId.isBlank()) {
            throw new RuntimeException("Unable to extract data for line");
        }
        LinePojo linePojo = new LinePojo(lineId, lineName);
        this.parsedLines.add(linePojo);

        Element linesParentDiv = singleLineHeader.parent();
        validateLineDiv(linesParentDiv);
        Element divWithStations = linesParentDiv.nextElementSibling();
        if (divWithStations == null) {
            throw new RuntimeException("No next div after line header");
        }
        Elements linesStationParagraphs = divWithStations.select("p.single-station");
        if (linesStationParagraphs.isEmpty()) {
            throw new RuntimeException("No stations found in line " + lineId);
        }
        for (Element singleStationParagraph : linesStationParagraphs) {
            processStationParagraph(singleStationParagraph, lineId);
        }
    }

    private void processStationParagraph(Element singleStationParagraph, String lineId) {
        Element spanWithStationIndex = singleStationParagraph.select("span.num").first();
        Element spanWithStationName = singleStationParagraph.select("span.name").first();
        if (spanWithStationIndex == null || spanWithStationName == null) {
            throw new RuntimeException("No suitable content in paragraph with station");
        }
        int stationIndex = getStationIndex(spanWithStationIndex);

        Elements connectedStationSpans = singleStationParagraph.select("span.t-icon-metroln");
        Set<SingleStationPojo> connectedStations = connectedStationSpans.stream()
                .map(DomReader::lineIconSpanToStation)
                .collect(Collectors.toSet());

        String stationName = spanWithStationName.text().trim();
        IndexedStationPojo stationPojo = new IndexedStationPojo(lineId, stationName, stationIndex, connectedStations);
        this.parsedStations.add(stationPojo);
    }

    private void validateLineDiv(Element stationsParentDiv) {
        if (stationsParentDiv == null) {
            throw new RuntimeException("NPE at validating line");
        }
        if (!stationsParentDiv.tagName().equals("div")) {
            throw new RuntimeException(String.format("Unexpected tag name: \"%s\", expected: \"div\"",
                    stationsParentDiv.tagName()));
        }
        Set<String> expectedClasses = Set.of("js-toggle-depend", "s-depend-control-single", "s-depend-control-active");
        Set<String> diff = SetUtils.substract(stationsParentDiv.classNames(), expectedClasses);
        if (!diff.isEmpty()) {
            throw new RuntimeException("Unexpected classes: " + diff);
        }
    }

    private void validateLineHeaderSpan(Element header) {
        if (!header.tagName().equals("span")) {
            throw new RuntimeException(String.format(
                    "Unexpected tag name: \"%s\", expected: \"span\"", header.tagName()));
        }
        Set<String> requiredClassesForSpanWithLineCaption = Set.of(
                "js-metro-line", "t-metrostation-list-header", "t-icon-metroln");
        Set<String> diff = SetUtils.substract(requiredClassesForSpanWithLineCaption, header.classNames());
        if (diff.size() > 0) {
            throw new RuntimeException("Expected class names: " + diff);
        }
    }

    private int getStationIndex(Element spanWithLineIndex) {
        String lineIndexFromTag = spanWithLineIndex.text();
        if (!lineIndexFromTag.matches("\\s*\\d+\\.\\s*")) {
            throw new RuntimeException("Invalid content \"" + lineIndexFromTag + "\" from tag with station index");
        }
        Pattern pattern = Pattern.compile("[\\dA-Z]+");
        Matcher matcher = pattern.matcher(lineIndexFromTag);
        boolean findOk = matcher.find();
        if (!findOk) {
            throw new RuntimeException(String.format("Unexpected failure on parsing line index from string \"%s\"",
                    lineIndexFromTag));
        }
        String resultStr = lineIndexFromTag.substring(matcher.start(), matcher.end());
        return Integer.parseInt(resultStr);
    }

    private static SingleStationPojo lineIconSpanToStation(Element lineIconSpan) {
        if (!lineIconSpan.tagName().equals("span")) {
            throw new RuntimeException(String.format("Unexpected tag: \"%s\", expected: \"span\"",
                    lineIconSpan.tagName()));
        }
        String stationName = lineIconSpanToStationName(lineIconSpan);
        String lineId = lineIconSpanToLineId(lineIconSpan);
        return new SingleStationPojo(stationName, lineId);
    }

    private static String lineIconSpanToLineId(Element lineIconSpan) {
        Set<String> classNames = lineIconSpan.classNames();
        if (classNames.size() != 2) {
            throw new RuntimeException("Unexpected number of classes for span: " + classNames.size());
        }
        if (!classNames.contains("t-icon-metroln")) {
            throw new RuntimeException("Expected class name: t-icon-metroln");
        }
        Optional<String> lineIdClassName = classNames.stream()
                .filter(className -> className.matches("ln-[\\dA-Z]+"))
                .findAny();
        if (lineIdClassName.isEmpty()) {
            throw new RuntimeException("Class name with line id was not found, classes: " + classNames);
        }
        return lineIdClassName.get().substring(3);
    }

    private static String lineIconSpanToStationName(Element lineIconSpan) {
        String title = lineIconSpan.attr("title");
        if (title.isBlank()) {
            throw new RuntimeException("Expected title attribute");
        }
        Pattern pattern = Pattern.compile("«[^»]+»");
        Matcher matcher = pattern.matcher(title);
        boolean findOk = matcher.find();
        if (!findOk) {
            throw new RuntimeException(String.format("Expected station name quoted in \"«\" and \"»\", got \"%s\"",
                    title));
        }
        int stationNameStart = matcher.start() + 1;
        int stationNameEnd = matcher.end() - 1;
        if (stationNameStart > stationNameEnd) {
            throw new RuntimeException("Blank station name provided");
        }
        return title.substring(stationNameStart, stationNameEnd);
    }

}
