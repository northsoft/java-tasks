package ru.skillbox.albert_raianov.java_from_scratch.task_15_14.metro_map.dao.dumb;

import ru.skillbox.albert_raianov.java_from_scratch.task_15_14.metro_map.dao.IndexedStationPojo;
import ru.skillbox.albert_raianov.java_from_scratch.task_15_14.metro_map.dao.LinePojo;
import ru.skillbox.albert_raianov.java_from_scratch.task_15_14.metro_map.dao.PojoWriter;

import java.util.Collections;
import java.util.List;

public class DumbWriter implements PojoWriter {
    private List<LinePojo> linePojo;
    private List<IndexedStationPojo> stationPojo;
    @Override
    public void setLinePojo(List<LinePojo> linePojo) {
        this.linePojo = linePojo;
    }

    @Override
    public void setStationPojo(List<IndexedStationPojo> stationPojo) {
        this.stationPojo = stationPojo;
    }

    @Override
    public void commit() throws Exception {

    }

    public List<LinePojo> getLinePojo() {
        return Collections.unmodifiableList(this.linePojo);
    }

    public List<IndexedStationPojo> getStationPojo() {
        return Collections.unmodifiableList(this.stationPojo);
    }
}
