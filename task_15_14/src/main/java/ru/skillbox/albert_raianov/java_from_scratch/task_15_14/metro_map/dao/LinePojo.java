package ru.skillbox.albert_raianov.java_from_scratch.task_15_14.metro_map.dao;

import ru.skillbox.albert_raianov.java_from_scratch.task_15_14.metro_map.domain.Line;

import java.util.Objects;

public class LinePojo {
    private String id;
    private String name;

    public LinePojo(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public LinePojo(Line line) {
        this.id = line.getId();
        this.name = line.getName();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LinePojo linePojo = (LinePojo) o;
        return Objects.equals(id, linePojo.id) && Objects.equals(name, linePojo.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }

    @Override
    public String toString() {
        return "LinePojo{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
