package ru.skillbox.albert_raianov.java_from_scratch.task_15_14.metro_map.dao;

import ru.skillbox.albert_raianov.java_from_scratch.task_15_14.metro_map.domain.Station;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public class IndexedStationPojo {
    private String lineId;
    private String name;
    private int indexInLine;
    private Set<SingleStationPojo> connectedStations;

    public IndexedStationPojo(String lineId, String name, int index) {
        this.lineId = lineId;
        this.name = name;
        this.indexInLine = index;
        this.connectedStations = new HashSet<>();
    }

    public IndexedStationPojo(String lineId, String name, int indexInLine, Set<SingleStationPojo> connectedStations) {
        this.lineId = lineId;
        this.name = name;
        this.indexInLine = indexInLine;
        this.connectedStations = connectedStations;
    }

    public IndexedStationPojo(Station station) {
        this.lineId = station.getLine().getId();
        this.name = station.getName();
        this.indexInLine = station.getIndex();
        this.connectedStations = station.getConnections().stream()
                .map(SingleStationPojo::new)
                .collect(Collectors.toSet());
    }

    public String getLineId() {
        return lineId;
    }

    public void setLineId(String lineId) {
        this.lineId = lineId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIndexInLine() {
        return indexInLine;
    }

    public void setIndexInLine(int indexInLine) {
        this.indexInLine = indexInLine;
    }

    public Set<SingleStationPojo> getConnectedStations() {
        return connectedStations;
    }

    public void setConnectedStations(Set<SingleStationPojo> connectedStations) {
        this.connectedStations = connectedStations;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IndexedStationPojo that = (IndexedStationPojo) o;
        return indexInLine == that.indexInLine &&
                Objects.equals(lineId, that.lineId) &&
                Objects.equals(name, that.name) &&
                Objects.equals(connectedStations, that.connectedStations);
    }

    @Override
    public int hashCode() {
        return Objects.hash(lineId, name, indexInLine);
    }

    @Override
    public String toString() {
        return "StationPojo{" +
                "lineId='" + lineId + '\'' +
                ", name='" + name + '\'' +
                ", indexInLine=" + indexInLine +
                ", outerItems=" + connectedStations +
                '}';
    }
}
