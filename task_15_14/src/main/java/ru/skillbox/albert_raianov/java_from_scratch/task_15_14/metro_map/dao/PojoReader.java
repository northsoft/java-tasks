package ru.skillbox.albert_raianov.java_from_scratch.task_15_14.metro_map.dao;

import java.util.Set;

public interface PojoReader {
    Set<LinePojo> readLinePojos();
    Set<IndexedStationPojo> readStationPojos();
}
