package ru.skillbox.albert_raianov.java_from_scratch.task_15_14.metro_map.dao;

import java.util.List;

public interface PojoWriter {
    void setLinePojo(List<LinePojo> linePojo);
    void setStationPojo(List<IndexedStationPojo> stationPojo);
    void commit() throws Exception;
}
