package ru.skillbox.albert_raianov.java_from_scratch.task_15_14.metro_map.dao.json;

import ru.skillbox.albert_raianov.java_from_scratch.task_15_14.metro_map.dao.IndexedStationPojo;
import ru.skillbox.albert_raianov.java_from_scratch.task_15_14.metro_map.dao.LinePojo;
import ru.skillbox.albert_raianov.java_from_scratch.task_15_14.metro_map.dao.SingleStationPojo;
import ru.skillbox.albert_raianov.java_from_scratch.task_15_14.util.UnorderedPair;

import java.util.*;
import java.util.stream.Collectors;

public class SplitterForConnectedStations {
    Set<LinePojo> pojoLines;
    Set<IndexedStationPojo> pojoIndexedStations;
    HashMap<SingleStationPojo, IndexedStationPojo> singleStationToIndexed = new HashMap<>();
    HashMap<SingleStationPojo, Set<SingleStationPojo>> stationToConnected = new HashMap<>();

    public SplitterForConnectedStations(List<LinePojo> pojoLines, List<IndexedStationPojo> pojoIndexedStations) {
        this.pojoLines = new HashSet<>(pojoLines);
        this.pojoIndexedStations = new HashSet<>(pojoIndexedStations);
        initStationsCache();
    }

    private void initStationsCache() {
        for (IndexedStationPojo indexedStation : this.pojoIndexedStations) {
            SingleStationPojo correspondingSingleStation = new SingleStationPojo(indexedStation);
            this.singleStationToIndexed.put(correspondingSingleStation, indexedStation);
        }
        for (IndexedStationPojo innerStation : this.pojoIndexedStations) {
            SingleStationPojo innerAsSingle = new SingleStationPojo(innerStation);
            for (SingleStationPojo outerStation : innerStation.getConnectedStations()) {
                getConnectionsForStation(innerAsSingle).add(outerStation);
            }
        }
    }



    private Set<SingleStationPojo> getConnectionsForStation(SingleStationPojo station) {
        return stationToConnected.computeIfAbsent(station, k -> new HashSet<>());
    }

    public Set<Set<SingleStationPojo>> getDirectlyConnectedStations() {
        Set<Set<SingleStationPojo>> maybeIndirectlyConnectedStations = getMaybeIndirectlyConnectedStations();
        Set<Set<SingleStationPojo>> result = new HashSet<>();
        for (Set<SingleStationPojo> singleSet : maybeIndirectlyConnectedStations) {
            Set<Set<SingleStationPojo>> setsOfDirectlyConnectedStations = splitToDirectlyConnected(singleSet);
            result.addAll(setsOfDirectlyConnectedStations);
        }
        return result;
    }

    private Set<Set<SingleStationPojo>> splitToDirectlyConnected(Set<SingleStationPojo> maybeIndirectlyConnected) {
        if (maybeIndirectlyConnected.size() == 2) {
            return Set.of(maybeIndirectlyConnected);
        }
        Set<UnorderedPair<SingleStationPojo>> connections = new HashSet<>();
        for (SingleStationPojo station : maybeIndirectlyConnected) {
            for (SingleStationPojo outerStation : singleStationToIndexed.get(station).getConnectedStations()) {
                connections.add(new UnorderedPair<>(station, outerStation));
            }
        }
        Set<Set<SingleStationPojo>> resultHubs = new HashSet<>();
        Optional<UnorderedPair<SingleStationPojo>> optConnection;
        while ((optConnection = connections.stream().findAny()).isPresent()) {
            UnorderedPair<SingleStationPojo> connection = optConnection.get();
            connections.remove(connection);

            Set<SingleStationPojo> hub = new HashSet<>(connection.getAsSet());
            Set<SingleStationPojo> stationsLeft = connections.stream()
                    .map(UnorderedPair::getAsSet)
                    .flatMap(Set::stream)
                    .filter(stationPojo -> !hub.contains(stationPojo))
                    .collect(Collectors.toSet());

            for (SingleStationPojo candidate : stationsLeft) {
                boolean isConnectedWithAllFromResult = hub.stream()
                        .map(stationFromResultItem -> new UnorderedPair<>(candidate, stationFromResultItem))
                        .allMatch(connections::contains);
                if (isConnectedWithAllFromResult) {
                    hub.stream()
                            .map(stationFromResultItem -> new UnorderedPair<>(candidate, stationFromResultItem))
                            .forEach(connections::remove);
                    hub.add(candidate);
                }
            }
            resultHubs.add(hub);
        }
        return resultHubs;
    }

    private Set<Set<SingleStationPojo>> getMaybeIndirectlyConnectedStations() {
        Set<SingleStationPojo> allStations = pojoIndexedStations.stream()
                .filter(indexedStation -> indexedStation.getConnectedStations().size() > 0)
                .map(SingleStationPojo::new)
                .collect(Collectors.toSet());

        Set<Set<SingleStationPojo>> result = new HashSet<>();
        Optional<SingleStationPojo> optStartStation;
        while ((optStartStation = allStations.stream().findAny()).isPresent()) {
            SingleStationPojo startStation = optStartStation.get();
            allStations.remove(startStation);
            Set<SingleStationPojo> stationsToProcess = new HashSet<>(Set.of(optStartStation.get()));
            Set<SingleStationPojo> resultItem = new HashSet<>();

            Optional<SingleStationPojo> optCurStation;
            while ((optCurStation = stationsToProcess.stream().findAny()).isPresent()) {
                SingleStationPojo curStation = optCurStation.get();
                stationsToProcess.remove(curStation);
                resultItem.add(curStation);

                for (SingleStationPojo connectedStation : getConnectionsForStation(curStation)) {
                    if (resultItem.contains(connectedStation) || stationsToProcess.contains((connectedStation))) {
                        continue;
                    }
                    allStations.remove(connectedStation);
                    stationsToProcess.add(connectedStation);
                }
            }
            result.add(resultItem);
        }
        return result;
    }


}
