package ru.skillbox.albert_raianov.java_from_scratch.task_15_14.metro_map.domain;

import ru.skillbox.albert_raianov.java_from_scratch.task_15_14.metro_map.dao.IndexedStationPojo;
import ru.skillbox.albert_raianov.java_from_scratch.task_15_14.metro_map.dao.LinePojo;
import ru.skillbox.albert_raianov.java_from_scratch.task_15_14.util.SetUtils;

import java.util.*;
import java.util.stream.Collectors;

public class Line {
    private final Metro metro;
    private final String id;
    private final String name;
    private List<Station> stations;
    private TreeMap<String, Station> nameToStation;

    Line(Metro metro, LinePojo linePojo) {
        this.metro = metro;
        this.id = linePojo.getId();
        this.name = linePojo.getName();
        this.stations = new ArrayList<>();
        this.nameToStation = new TreeMap<>();
    }

    public Metro getMetro() {
        return metro;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public List<Station> getStations() {
        return Collections.unmodifiableList(stations);
    }

    public Station getStationByName(String stationName) {
        Station result = nameToStation.get(stationName);
        if (result == null) {
            throw new NoSuchElementException("Station name \"" + stationName + "\" was not found");
        }
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Line line = (Line) o;
        return Objects.equals(id, line.id) && Objects.equals(name, line.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }

    @Override
    public String toString() {
        return "Line{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", stations=" + stations +
                '}';
    }

    void setStations(List<IndexedStationPojo> pojoStations) {
        if (pojoStations.isEmpty()) {
            throw new RuntimeException("Empty set of POJO stations provided to line " + this.id);
        }
        assertStationsBelongToLine(pojoStations);
        assertStationNamesAreUnique(pojoStations);
        assertIndexSequence(pojoStations);

        this.stations = pojoStations.stream()
                .map(stationPojo -> new Station(stationPojo.getName(), this))
                .collect(Collectors.toList());
        for (Station station : this.stations) {
            this.nameToStation.put(station.getName(), station);
        }
    }

    private void assertIndexSequence(List<IndexedStationPojo> pojoStations) {
        for (int i = 0; i < pojoStations.size(); ++i) {
            IndexedStationPojo curStationPojo = pojoStations.get(i);
            if (curStationPojo.getIndexInLine() != i + 1) {
                throw new RuntimeException(String.format("At line \"%s\" element %s has index %d, expected %d",
                        this.id, curStationPojo, curStationPojo.getIndexInLine(), i + 1));
            }
        }
    }

    private void assertStationsBelongToLine(List<IndexedStationPojo> pojoStations) {
        List<IndexedStationPojo> unexpectedStations = pojoStations.stream()
                .filter(stationPojo -> !stationPojo.getLineId().equals(this.id))
                .toList();
        if (!unexpectedStations.isEmpty()) {
            throw new RuntimeException("Stations according to POJO don't belong to line " + this.id + ":" + pojoStations);
        }
    }

    private void assertStationNamesAreUnique(List<IndexedStationPojo> pojoStations) {
        Set<IndexedStationPojo> nonUniqueStations = SetUtils.findNonUniqueElements(pojoStations);
        if (!nonUniqueStations.isEmpty()) {
            throw new RuntimeException("Non-unique stations at line " + this.id + "found: " + nonUniqueStations);
        }
    }
}
