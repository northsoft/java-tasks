package ru.skillbox.albert_raianov.java_from_scratch.task_15_14.metro_map.domain;

import org.apache.logging.log4j.LogManager;
import ru.skillbox.albert_raianov.java_from_scratch.task_15_14.metro_map.dao.*;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class Metro {
    private Set<Line> lines = new HashSet<>();
    private TreeMap<String, Line> namesToLines = new TreeMap<>();
    private TreeMap<String, Line> idsToLines = new TreeMap<>();


    public Metro(PojoReader pojoReader) throws Exception {
        try {
            assertPojoIsConsistent(pojoReader);
            for (LinePojo linePojo : pojoReader.readLinePojos()) {
                Line line = new Line(this, linePojo);
                addStation(line);
            }

            for (Line line : this.lines) {
                List<IndexedStationPojo> pojoStationsInLine = pojoReader.readStationPojos().stream()
                        .filter(stationPojo -> stationPojo.getLineId().equals(line.getId()))
                        .sorted(Comparator
                                .comparing(IndexedStationPojo::getLineId)
                                .thenComparing(IndexedStationPojo::getIndexInLine))
                        .toList();
                line.setStations(pojoStationsInLine);
            }

            Set<IndexedStationPojo> pojoStationsWithConnections = pojoReader.readStationPojos().stream()
                    .filter(stationPojo -> !stationPojo.getConnectedStations().isEmpty())
                    .collect(Collectors.toSet());
            for (IndexedStationPojo singlePojoStationWithConnection : pojoStationsWithConnections) {
                this.processConnectedPojos(singlePojoStationWithConnection);
            }
        } catch (Exception e) {
            throw new Exception("Unable to construct a Metro domain object", e);
        }
    }

    public Station getStationByLineAndName(String lineId, String stationName) {
        return this.getLineById(lineId).getStationByName(stationName);
    }

    public Station getStationByPojo(IndexedStationPojo stationPojo) {
        try {
            return this.getStationByLineAndName(stationPojo.getLineId(), stationPojo.getName());
        } catch (NoSuchElementException e) {
            throw new NoSuchElementException(String.format("No station according to POJO %s", stationPojo), e);
        }
    }

    public Set<Line> getLines() {
        return Collections.unmodifiableSet(this.lines);
    }

    public Line getLineById(String lineId) {
        Line result = this.idsToLines.get(lineId);
        if (result == null) {
            throw new NoSuchElementException(String.format("Line ID %s not found", lineId));
        }
        return result;
    }

    public void report() {
        System.out.println("———————————————————————— [ METRO DOMAIN OBJECT REPORT ] ————————————————————————");
        for (Line line : lines.stream().sorted(Comparator.comparing(Line::getId)).toList()) {
            System.out.printf("Line [%s] «%s»: %n", line.getId(), line.getName());
            for (Station station : line.getStations()) {
                Set<Station> outerStations = station.getConnections();
                System.out.printf("\tStation «%s», %d connections%n", station.getName(), outerStations.size());
                for (Station outerStation : outerStations) {
                    System.out.printf("\t\tConnected with: «%s» at «%s»%n",
                            outerStation.getName(), outerStation.getLine().getId());
                }
            }
        }
        System.out.println("———————————————————————— [/METRO DOMAIN OBJECT REPORT ] ————————————————————————");
    }

    void addStation(Line line) {
        if (lines.contains(line)) {
            return;
        }

        boolean lineWithSameIdExists   = this.idsToLines.containsKey(line.getId());
        boolean lineWithSameNameExists = this.namesToLines.containsKey(line.getName());
        if (!lineWithSameIdExists && !lineWithSameNameExists) {
            this.lines.add(line);
            namesToLines.put(line.getName(), line);
            idsToLines.put(line.getId(), line);
            return;
        }
        Line lineWithSameId = this.idsToLines.get(line.getId());
        Line lineWithSameName = this.namesToLines.get(line.getName());
        boolean isCollisionWithOneStation = lineWithSameId == lineWithSameName;
        if (isCollisionWithOneStation && line.getStations().isEmpty()) {
            LogManager.getRootLogger().info(String.format(
                    "Request to add existent line \"%s\" / \"%s\" with no stations was skipped",
                    line.getId(), line.getName()));
            return;
        }

        StringBuilder errorMessageBuilder = new StringBuilder(
                String.format("Attempt to add station: %s", line.toString()));
        if (isCollisionWithOneStation) {
            errorMessageBuilder.append(String.format(", existent station: %s", lineWithSameId.toString()));
        } else {
            if (lineWithSameId != null) {
                errorMessageBuilder.append(String.format(", existent station: %s", lineWithSameId.toString()));
            }
            if (lineWithSameName != null) {
                errorMessageBuilder.append(String.format(", existent station: %s", lineWithSameName.toString()));
            }
        }
        throw new RuntimeException(errorMessageBuilder.toString());
    }

    public void saveToPojoWriter(PojoWriter pojoWriter) throws Exception {
        try {
            List<LinePojo> linePojoList = this.exportLinesToPojos();
            List<IndexedStationPojo> stationPojoList = this.exportStationsToPojos();
            pojoWriter.setLinePojo(linePojoList);
            pojoWriter.setStationPojo(stationPojoList);
            pojoWriter.commit();
        } catch (Exception e) {
            throw new Exception("Unable to write data to POJO writer", e);
        }
    }

    private void processConnectedPojos(IndexedStationPojo singlePojoStationWithConnection) {
        if (singlePojoStationWithConnection.getConnectedStations().isEmpty()) {
            throw new IllegalArgumentException(String.format(
                    "Requested processing connected stations for POJO %s without connections",
                    singlePojoStationWithConnection));
        }
        Station station = this.getStationByPojo(singlePojoStationWithConnection);
        for (SingleStationPojo connectedStation : singlePojoStationWithConnection.getConnectedStations()) {
            Station outerStation = this.getStationByLineAndName(connectedStation.getLineId(), connectedStation.getStationName());
            station.connectWith(outerStation);
        }
    }

    private void assertPojoIsConsistent(PojoReader pojoReader) {
        assertNoBlankStrings(pojoReader);
        assertNoEmptyPojoLines(pojoReader);
        assertNoStationsWithOrphanLines(pojoReader);
        assertNoOrphanConnections(pojoReader);
    }

    private void assertNoBlankStrings(PojoReader pojoReader) {
        Map<String, Function<PojoReader, Stream<String>>> stringProviders = Map.of(
                "line ID",              reader -> reader.readLinePojos().stream().map(LinePojo::getId),
                "line name",            reader -> reader.readLinePojos().stream().map(LinePojo::getName),
                "station name",         reader -> reader.readStationPojos().stream().map(IndexedStationPojo::getName),
                "station line",         reader -> reader.readStationPojos().stream().map(IndexedStationPojo::getLineId),
                "connected line",       reader -> reader.readStationPojos().stream()
                                                            .map(IndexedStationPojo::getConnectedStations)
                                                            .flatMap(Set::stream)
                                                            .map(SingleStationPojo::getLineId),
                "connected station",    reader -> reader.readStationPojos().stream()
                                                            .map(IndexedStationPojo::getConnectedStations)
                                                            .flatMap(Set::stream)
                                                            .map(SingleStationPojo::getStationName));

        for (Map.Entry<String, Function<PojoReader, Stream<String>>> provider : stringProviders.entrySet()) {
            if (provider.getValue().apply(pojoReader).anyMatch(String::isBlank)) {
                throw new RuntimeException("Found blank " + provider.getKey() + "s");
            }
        }
    }

    private void assertNoEmptyPojoLines(PojoReader pojoReader) {
        Set<String> lineIdsFromPojoStations = pojoReader.readStationPojos().stream()
                .map(IndexedStationPojo::getLineId)
                .collect(Collectors.toSet());
        Set<LinePojo> emptyLines = pojoReader.readLinePojos().stream()
                .filter(linePojo -> !lineIdsFromPojoStations.contains(linePojo.getId()))
                .collect(Collectors.toSet());
        if (!emptyLines.isEmpty()) {
            throw new RuntimeException("Empty lines found: " + emptyLines);
        }
    }

    private void assertNoStationsWithOrphanLines(PojoReader pojoReader) {
        Set<String> lineIdsFromPojoLines = pojoReader.readLinePojos().stream()
                .map(LinePojo::getId)
                .collect(Collectors.toSet());
        Set<IndexedStationPojo> stationsWithOrphanLines = pojoReader.readStationPojos().stream()
                .filter(stationPojo -> !lineIdsFromPojoLines.contains(stationPojo.getLineId()))
                .collect(Collectors.toSet());
        if (!stationsWithOrphanLines.isEmpty()) {
            throw new RuntimeException("Stations with orphan lines found: " + stationsWithOrphanLines);
        }
    }

    private void assertNoOrphanConnections(PojoReader pojoReader) {
        Set<SingleStationPojo> existentStations = pojoReader.readStationPojos().stream()
                .map(stationPojo -> new SingleStationPojo(stationPojo.getName(), stationPojo.getLineId()))
                .collect(Collectors.toSet());

        Set<SingleStationPojo> orphanConnections = pojoReader.readStationPojos().stream()
                .flatMap(stationPojo -> stationPojo.getConnectedStations().stream())
                .filter(connectedStation -> !existentStations.contains(connectedStation))
                .collect(Collectors.toSet());

        if (!orphanConnections.isEmpty()) {
            throw new RuntimeException("Found orphan connections: " + orphanConnections);
        }
    }

    public List<LinePojo> exportLinesToPojos() {
        return this.lines.stream()
                .map(LinePojo::new)
                .sorted(Comparator.comparing(LinePojo::getId))
                .toList();
    }

    private List<IndexedStationPojo> exportStationsToPojos() {
        return lines.stream()
                .map(Line::getStations)
                .flatMap(List::stream)
                .map(IndexedStationPojo::new)
                .toList();
    }
}
