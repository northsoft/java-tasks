package ru.skillbox.albert_raianov.java_from_scratch.task_15_14.metro_map.dao.json;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import ru.skillbox.albert_raianov.java_from_scratch.task_15_14.metro_map.dao.IndexedStationPojo;
import ru.skillbox.albert_raianov.java_from_scratch.task_15_14.metro_map.dao.LinePojo;
import ru.skillbox.albert_raianov.java_from_scratch.task_15_14.metro_map.dao.PojoWriter;
import ru.skillbox.albert_raianov.java_from_scratch.task_15_14.metro_map.dao.SingleStationPojo;

import java.io.File;
import java.io.FileWriter;
import java.util.*;

public class JsonWriter implements PojoWriter {
    String filename;
    List<LinePojo> linePojoList;
    List<IndexedStationPojo> stationPojoList;

    public JsonWriter(String filename) throws Exception {
        try {
            File file = new File(filename);
            if (!file.exists()) {
                boolean created = file.createNewFile();
                if (!created) {
                    throw new Exception("Unable to create file \"" + filename + "\"");
                }
            }
            if (!file.isFile()) {
                throw new Exception("Object \"" + filename + "\" already exists and is not a file");
            }
            if (!file.canWrite()) {
                throw new Exception("File \"" + filename + "\" is not available to write");
            }
            this.filename = filename;
        } catch (Exception e) {
            throw new Exception("Unable to create a writer with filename \"" + filename + "\"", e);
        }
    }
    @Override
    public void setLinePojo(List<LinePojo> linePojo) {
        this.linePojoList = linePojo;
    }

    @Override
    public void setStationPojo(List<IndexedStationPojo> stationPojo) {
        this.stationPojoList = stationPojo;
    }

    @Override
    public void commit() throws Exception {
        FileWriter writer = null;
        try {
            if (this.linePojoList == null || this.stationPojoList == null) {
                throw new RuntimeException(String.format("Json writer is not ready (lines: %s, stations: %s)",
                        this.linePojoList == null ? "null " : "not null",
                        this.stationPojoList == null ? "null" : "not null"));
            }
            JSONObject rootObject = createRootObject();
            writer = new FileWriter(this.filename);
            writer.write(rootObject.toJSONString());
        } catch (Exception e) {
            throw new Exception("Failed to write data", e);
        } finally {
            if (writer != null) {
                writer.flush();
                writer.close();
            }
        }
    }

    private JSONObject createRootObject() {
        JSONArray linesArray = createLinesArray();
        JSONObject stationsObject = createStationsObject();
        JSONArray connectionsArray = createConnectionsArray();
        return new JSONObject(Map.of(
                "lines",        linesArray,
                "stations",     stationsObject,
                "connections",  connectionsArray));
    }

    private JSONArray createLinesArray() {
        List<JSONObject> resultObjects = linePojoList.stream()
                .map(linePojo -> new JSONObject(Map.of(
                        "number",   linePojo.getId(),
                        "name", linePojo.getName())))
                .toList();
        JSONArray result = new JSONArray();
        result.addAll(resultObjects);
        return result;
    }

    private JSONObject createStationsObject() {
        List<String> lineIds = linePojoList.stream()
                .map(LinePojo::getId)
                .sorted()
                .toList();
        Map<String, Object> result = new TreeMap<>();
        for (String lineId : lineIds) {
            List<String> stationNames = stationPojoList.stream()
                    .filter(stationPojo -> stationPojo.getLineId().equals(lineId))
                    .sorted(Comparator.comparing(IndexedStationPojo::getIndexInLine))
                    .map(IndexedStationPojo::getName)
                    .toList();
            JSONArray stationArray = new JSONArray();
            stationArray.addAll(stationNames);
            result.put(lineId, stationArray);
        }
        return new JSONObject(result);
    }

    private JSONArray createConnectionsArray() {
        SplitterForConnectedStations splitter = new SplitterForConnectedStations(linePojoList, stationPojoList);
        Set<Set<SingleStationPojo>> connections = splitter.getDirectlyConnectedStations();
        JSONArray result = new JSONArray();
        for (Set<SingleStationPojo> connection : connections) {
            JSONArray resultItem = new JSONArray();
            for (SingleStationPojo station : connection) {
                JSONObject jsonStation = new JSONObject(Map.of(
                        "line", station.getLineId(),
                        "name", station.getStationName()
                ));
                resultItem.add(jsonStation);
            }
            result.add(resultItem);
        }
        return result;
    }


}
