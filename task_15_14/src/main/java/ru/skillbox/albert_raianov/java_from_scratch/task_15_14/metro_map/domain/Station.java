package ru.skillbox.albert_raianov.java_from_scratch.task_15_14.metro_map.domain;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class Station {
    private String name;
    private Line line;
    private Set<Station> connectedStations;
    public String getName() {
        return name;
    }

    public Line getLine() {
        return line;
    }

    Station(String name, Line line) {
        this.name = name;
        this.line = line;
        this.connectedStations = new HashSet<>();
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder(String.format("\"%s\" at \"%s\"", name, line.getId()));
        if (this.connectedStations.isEmpty()) {
            stringBuilder.append(" (no connected stations)");
        } else {
            String connections = this.connectedStations.stream()
                    .map(station -> String.format("{\"%s\" @ \"%s\"}", station.getName(), station.getLine().getId()))
                    .collect(Collectors.joining(", "));
            stringBuilder.append(String.format(", connectedStations: {%s}", connections));
        }
        return String.format("Station{%s}", stringBuilder);
    }

    public boolean isConnectedWith(Station anotherStation) {
        if (this == anotherStation) {
            throw new IllegalArgumentException("Self-connect query has undefined result");
        }
        return this.connectedStations.contains(anotherStation);
    }

    public int getIndex() {
        int result = this.line.getStations().indexOf(this);
        if (result < 0) {
            throw new RuntimeException(String.format(
                    "Inconsistent domain object: station %s is associated with line %s, which has no station above",
                    this, this.getLine()));
        }
        return result;
    }

    public Set<Station> getConnections() {
        return Collections.unmodifiableSet(this.connectedStations);
    }

    void connectWith(Station anotherStation) {
        if (this == anotherStation) {
            throw new IllegalArgumentException("Requested self-connect at " + this);
        }
        boolean isAlreadyConnected = this.connectedStations.contains(anotherStation);
        if (!isAlreadyConnected) {
            this.connectedStations.add(anotherStation);
            boolean anotherStationIsAlreadyConnected = anotherStation.isConnectedWith(this);
            if (!anotherStationIsAlreadyConnected) {
                anotherStation.connectWith(this);
            }
        }
    }
}
