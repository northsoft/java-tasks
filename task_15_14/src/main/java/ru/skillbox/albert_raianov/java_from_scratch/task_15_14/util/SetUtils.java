package ru.skillbox.albert_raianov.java_from_scratch.task_15_14.util;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class SetUtils {

    public static <T> Set<T> substract(Set<T> setToSubstractFrom, Set<T> setToSubstract) {
        return setToSubstractFrom.stream()
                .filter(setToSubstractFromEntry -> !setToSubstract.contains(setToSubstractFromEntry))
                .collect(Collectors.toSet());
    }

    public static <T> long numberOfOccurrences(List<T> list, T item) {
        return list.stream().filter(checkedElem -> checkedElem.equals(item)).count();
    }

    public static <T> Set<T> findNonUniqueElements(List<T> list) {
        return list.stream()
                .filter(checkedItem -> numberOfOccurrences(list, checkedItem) > 1)
                .collect(Collectors.toSet());
    }
}
