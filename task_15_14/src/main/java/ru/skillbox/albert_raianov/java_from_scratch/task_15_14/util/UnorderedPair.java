package ru.skillbox.albert_raianov.java_from_scratch.task_15_14.util;

import java.util.Collections;
import java.util.Objects;
import java.util.Set;

public class UnorderedPair<T> {
    private Set<T> storage;

    public UnorderedPair(T first, T second) {
        this.storage = Set.of(first, second);
    }

    public Set<T> getAsSet() {
        return Collections.unmodifiableSet(storage);
    }

    public boolean contains(T object) {
        return storage.contains(object);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        try {
            UnorderedPair<T> that = (UnorderedPair<T>) o;
            return Objects.equals(storage, that.storage);
        } catch (ClassCastException e) {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(storage);
    }
}
