package ru.skillbox.albert_raianov.java_from_scratch.task_15_14.metro_map.dao;

import org.json.simple.JSONObject;
import ru.skillbox.albert_raianov.java_from_scratch.task_15_14.metro_map.domain.Station;

import java.util.Objects;

public class SingleStationPojo {
    private String stationName;
    private String lineId;

    public SingleStationPojo(String stationName, String lineId) {
        this.stationName = stationName;
        this.lineId = lineId;
    }

    public SingleStationPojo(IndexedStationPojo indexedStationPojo) {
        this.stationName = indexedStationPojo.getName();
        this.lineId = indexedStationPojo.getLineId();
    }

    public SingleStationPojo(Station station) {
        this.stationName = station.getName();
        this.lineId = station.getLine().getId();
    }

    public SingleStationPojo(JSONObject jsonObject) {
        Object objStationName = jsonObject.get("name");
        if (objStationName == null) {
            throw new RuntimeException("No name in JSON object");
        }
        this.stationName = (String) objStationName;
        if (this.stationName.isBlank()) {
            throw new RuntimeException("Station name in JSON object is blank");
        }
        Object objLineId = jsonObject.get("line");
        if (objLineId == null) {
            throw new RuntimeException("No line ID in JSON object");
        }
        this.lineId = (String) objLineId;
        if (this.lineId.isBlank()) {
            throw new RuntimeException("Line ID in JSON object is blank");
        }
    }

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    public String getLineId() {
        return lineId;
    }

    public void setLineId(String lineId) {
        this.lineId = lineId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SingleStationPojo that = (SingleStationPojo) o;
        return Objects.equals(stationName, that.stationName) && Objects.equals(lineId, that.lineId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(stationName, lineId);
    }

    @Override
    public String toString() {
        return "SingleStationPojo{" +
                "stationName='" + stationName + '\'' +
                ", lineId='" + lineId + '\'' +
                '}';
    }
}
