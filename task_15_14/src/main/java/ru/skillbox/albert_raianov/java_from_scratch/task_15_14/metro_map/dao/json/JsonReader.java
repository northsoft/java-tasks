package ru.skillbox.albert_raianov.java_from_scratch.task_15_14.metro_map.dao.json;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import ru.skillbox.albert_raianov.java_from_scratch.task_15_14.metro_map.dao.IndexedStationPojo;
import ru.skillbox.albert_raianov.java_from_scratch.task_15_14.metro_map.dao.LinePojo;
import ru.skillbox.albert_raianov.java_from_scratch.task_15_14.metro_map.dao.PojoReader;
import ru.skillbox.albert_raianov.java_from_scratch.task_15_14.metro_map.dao.SingleStationPojo;

import java.io.FileReader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class JsonReader implements PojoReader {
    private Set<LinePojo> resultLines;
    private Set<IndexedStationPojo> resultStationPojos;
    private Map<SingleStationPojo, IndexedStationPojo> singlesToIndexed = new HashMap<>();

    public JsonReader(String pathToJson) throws Exception {
        try {
            FileReader fileReader = new FileReader(pathToJson);
            JSONParser parser = new JSONParser();
            Object jsonRoot = parser.parse(fileReader);
            processRoot((JSONObject) jsonRoot);
        } catch (Exception e) {
            throw new Exception("Unable to parse file \"" + pathToJson + "\"", e);
        }
    }

    @Override
    public Set<LinePojo> readLinePojos() {
        return resultLines;
    }

    @Override
    public Set<IndexedStationPojo> readStationPojos() {
        return resultStationPojos;
    }

    private void processRoot(JSONObject jsonRoot) {
        JSONArray lines = (JSONArray) jsonRoot.get("lines");
        JSONObject stations = (JSONObject) jsonRoot.get("stations");
        JSONArray hubs = (JSONArray) jsonRoot.get("connections");
        assertMapItemsAreNonNull(Map.of("lines", lines, "stations", stations, "hubs", hubs));

        processLines(lines);
        processStations(stations);
        processHubs(hubs);
    }

    private void processLines(JSONArray lines) {
        resultLines = new HashSet<>();
        for (Object obj : lines) {
            JSONObject jsonLineObject = (JSONObject) obj;
            Object lineId = jsonLineObject.get("number");
            Object lineName = jsonLineObject.get("name");
            assertMapItemsAreNonNull(Map.of("line ID", lineId, "line name", lineName));
            resultLines.add(new LinePojo((String) lineId, (String) lineName));
        }
    }

    private void processStations(JSONObject stations) {
        this.resultStationPojos = new HashSet<>();
        for (Object objLineId : stations.keySet()) {
            String lineId = (String) objLineId;
            JSONArray lineStations = (JSONArray) stations.get(lineId);
            for (int i = 0; i < lineStations.size(); ++i) {
                String stationName = (String) lineStations.get(i);
                IndexedStationPojo curStation =new IndexedStationPojo(lineId, stationName, i + 1);
                resultStationPojos.add(curStation);
                singlesToIndexed.put(new SingleStationPojo(curStation), curStation);
            }
        }
    }

    private void processHubs(JSONArray hubs) {
        for (Object objHub : hubs) {
            JSONArray hub = (JSONArray) objHub;
            for (Object objHubInnerStation : hub) {
                SingleStationPojo innerStation = new SingleStationPojo((JSONObject) objHubInnerStation);
                IndexedStationPojo stationInOutput = this.singlesToIndexed.get(innerStation);
                if (stationInOutput == null) {
                    throw new RuntimeException(String.format(
                            "Station %s in hub %s has no corresponding object in list of stations", innerStation, hub));
                }
                for (Object objHubOuterStation : hub) {
                    SingleStationPojo outerStation = new SingleStationPojo((JSONObject) objHubOuterStation);
                    if (outerStation.equals(innerStation)) {
                        continue;
                    }
                    stationInOutput.getConnectedStations().add(outerStation);
                }
            }
        }
    }

    void assertMapItemsAreNonNull(Map<String, Object> map) {
        for (Map.Entry<String, Object> entry : map.entrySet()) {
            if (entry.getValue() == null) {
                throw new RuntimeException(entry.getKey() + " is null");
            }
        }
    }

}
