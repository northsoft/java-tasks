import org.jooq.meta.jaxb.Logging

plugins {
	java
	id("org.springframework.boot") version "3.2.1"
	id("io.spring.dependency-management") version "1.1.4"
	id("nu.studer.jooq") version "8.2.1"
}

group = "ru.skillbox.albert_raianov"
version = "0.0.1-SNAPSHOT"

java {
	sourceCompatibility = JavaVersion.VERSION_21
}

configurations {
	compileOnly {
		extendsFrom(configurations.annotationProcessor.get())
	}
}

repositories {
	mavenCentral()
	maven {
		url = uri("https://plugins.gradle.org/n2/")
	}
}

dependencies {
	// databases
	implementation("jakarta.persistence:jakarta.persistence-api:3.2.0-M1")
	implementation("org.springframework.boot:spring-boot-starter-data-jdbc") {
		exclude(group="org.springframework.boot", module="spring-boot-starter-logging")
	}
	runtimeOnly("org.postgresql:postgresql")
	implementation("org.springframework.boot:spring-boot-starter-jooq") {
		exclude(group="org.springframework.boot", module="spring-boot-starter-logging")
	}
	jooqGenerator("org.postgresql:postgresql:42.6.0")

	// web
	implementation("org.springframework.boot:spring-boot-starter-thymeleaf") {
		exclude(group="org.springframework.boot", module="spring-boot-starter-logging")
	}
	implementation("org.springframework.boot:spring-boot-starter-web") {
		exclude(group="org.springframework.boot", module="spring-boot-starter-logging")
	}
	implementation("org.springframework.boot:spring-boot-starter-log4j2")
	compileOnly("org.projectlombok:lombok")
	annotationProcessor("org.projectlombok:lombok")
	testImplementation("org.springframework.boot:spring-boot-starter-test")
}


jooq {
	version.set("3.18.4")
	edition.set(nu.studer.gradle.jooq.JooqEdition.OSS)
	configurations {
		create("main") {
			generateSchemaSourceOnCompilation.set(true)
			jooqConfiguration.apply {
				logging = Logging.WARN
				jdbc.apply {
					driver = "org.postgresql.Driver"
					url = System.getenv("DB_URL") ?: "jdbc:postgresql://localhost:5432/contacts"
					user = System.getenv("DB_USERNAME") ?: "postgres"
					password = System.getenv("DB_PASSWORD") ?: "postgres"
				}
				generator.apply {
					name = "org.jooq.codegen.DefaultGenerator"
					database.apply {
						inputSchema = "contacts_schema"
					}
					generate.apply {
						isDeprecated = false
						isRecords = true
						isImmutablePojos = true
						isFluentSetters = true
					}
					target.apply {
						packageName = "ru.skillbox.albert_raianov.spring_3_8_webmvc.persistence.implementations.jooq.db"
						directory = "build/generated-src/jooq/main"
					}
					strategy.name = "org.jooq.codegen.DefaultGeneratorStrategy"
				}
			}
		}
	}
}

tasks.named<nu.studer.gradle.jooq.JooqGenerate>("generateJooq") {
	(launcher::set)
	(javaToolchains.launcherFor {
		languageVersion.set(JavaLanguageVersion.of(21))
	})
}

tasks.withType<Test> {
	useJUnitPlatform()
}
