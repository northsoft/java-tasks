CREATE SCHEMA IF NOT EXISTS contacts_schema;

DROP TABLE IF EXISTS contacts_schema.contacts;

CREATE TABLE contacts_schema.contacts(
    id          BIGSERIAL PRIMARY KEY,
    firstName   VARCHAR(255) NOT NULL,
    lastName    VARCHAR(255) NOT NULL,
    email       VARCHAR(255) NOT NULL,
    phone       VARCHAR(255) NOT NULL
);