# Task 3.8: List of contacts
This is a web application designed to store list of contacts in memory
or in database.

## TOC
- [1.](#intro) Intro (supposed ways of workflow)
- [2.](#ways_to_build_app) Ways to build the application
- [2.1.](#build_without_docker_db) Build without Docker-running database
- [2.2.](#build_with_docker_db) Build with Docker-running database
- [3.](#running) Running
- [3.1.](#running_without_docker) Running without Docker
- [3.2.](#running_with_docker) Building and running Docker image
- [4.](#configuration) Building and running Docker image
- [4.1.](#env_vars_list) List of environment variables
- [4.2.](#env_vars_for_jar) Setting environment variables for running JAR file
- [4.3.](#env_vars_for_docker) Setting environment variables for running Docker setup
- [5.](#copyright) Copyright

## <a name="intro" />1. Intro (supposed ways of workflow)
__Attention 1:__ hereinafter all commands mentioned here are supposed to be run
from the root directory of the application.

__Attention 2:__ this application requires a PostgreSQL instance running
to build and start. By default the application uses these credentials
to connect to PostgreSQL:  
- host name: `localhost`;  
- port: `5432`;  
- login: `postgres`;  
- password: `postgres`;  
- database: `contacts`.  
Hereinafter these credentials will be referred to as __default credentials__.
If you understand that you need to override default credentials, ways to do so
are provided.

This source uses Gradle to build JAR file itself. But this application
has a JOOQ persistence provider, so __during build__ it requires
__a running instance__ of PostgreSQL database. You may build JAR file
with PostgreSQL instance set up  [by yourself](#build_without_docker_db),
or launch PostgreSQL instance in [a Docker container](#build_with_docker_db).

After the JAR file is built, you can [launch it directly](#running_without_docker),
or [build and run a Docker container](#running_with_docker) with the application.

## <a name="ways_to_build_app" />2. Ways to build the application

### <a name="build_without_docker_db" />2.1. Build without Docker-running database
This is an option if you already have a PostgreSQL instance running
and you don't need to set it up.

You can build a JAR file with the command:
```
gradle bootJar
```

Also you can override default credentials by specifying environment variables
`DB_URL` _(in format `jdbc:postgresql://hostname:port/dbname`)_, `DB_USERNAME`
or `DB_PASSWORD` in a way specific to your shell. E.g. in POSIX-compliant shells
(tested in Bash under Linux) you may execute it this way:
```
DB_URL='jdbc:postgresql://mypgsqlhostname:2345/mydatabase' \
DB_USERNAME='admin' DB_PASSWORD='example' gradle bootJar
```

In Windows' CMD it can be done in another way (not tested!):
```
SET DB_URL=jdbc:postgresql://mypgsqlhostname:2345/mydatabase
SET DB_USERNAME=admin
SET DB_PASSWORD=example
gradle bootJar
```

### <a name="build_with_docker_db" />2.1. Build with Docker-running database
The source is already equipped with Docker-compose's YAML file,
`docker/docker-compose-dbonly.yaml`, destined to launch only
a PostgreSQL instance. So you can launch it via command:
```
docker compose -f docker/docker-compose-dbonly.yaml up -d
```

After PostgreSQL instance is up and port forwarding is in force, you can
build the application:
```
gradle bootJar
```

After the build is completed, you should stop the PostgreSQL instance
running in Docker container:
```
docker compose -f docker/docker-compose-dbonly.yaml down
```

**Attention:** do not forget to call the command above after the build
is competed, otherwise, even if you have stopped container with PostgreSQL,
settings on port forwarding will be still in force, and you can face
unexpected issues with further setup of containers.


## <a name="running" /> 3. Running

### <a name="running_without_docker" />3.1. Running without Docker
After successful build of a JAR file, in case if you have a PostgreSQL instance
launched with default credentials valid, you can launch the application directly:
```
java -jar build/libs/spring_3_8_contactlist-0.0.1-SNAPSHOT.jar
```

Also you may override default credentials:
```
java -DDB_URL=jdbc:postgresql://mypgsqlhostname:2345/dbname \
-DDB_USERNAME=myusername -DDB_PASSWORD=mypassword \
-jar build/libs/spring_3_8_contactlist-0.0.1-SNAPSHOT.jar
```

More examples are [in section 4.2](#env_vars_for_jar).
  
### <a name="running_with_docker" />3.2. Building and running Docker image
Another option available **after JAR file is built** is creating
a Docker image. Default setup supposes two containers, first for database
and second for application. Building and running application can be done
via the command below:
```
docker compose -f docker/docker-compose.yaml up
```

If you don't want application output to take console until you hit `Ctrl+C`,
you can launch this setup in detached mode:
```
docker compose -f docker/docker-compose.yaml up -d
```

You can shut the setup down by hitting `Ctrl+C` in a console if you did not
launch the setup in detached mode (with `-d` argument), or you can use
the command below:
```
docker compose -f docker/docker-compose.yaml down
```

## <a name="configuration" />4. Configuration
The application can be configured via environment variables. You can
take a look at the [list of environment variables](#env_vars_list) 
used by the application, and set them in a matter corresponding 
to [a JAR file](#env_vars_for_jar) or [a Docker setup](#env_vars_for_docker).

### <a name="env_vars_list" />4.1. List of environment variables
- `DB_URL` — URL passed to JDBC. Should be in format
`jdbc:postgresql://hostname:port/databasename`.
Default is `jdbc:postgresql://localhost:5432/contacts`.  
- `DB_USERNAME` — user name used to log into PostgreSQL instance.
Default is `postgres`.  
- `DB_PASSWORD` - password used to log into PostgreSQL instance.
Default is `postgres`.  
- `APP_AUTOINSERT_ENABLED` - boolean value to determine if some
hardcoded values should be inserted on application startup. Useful
for testing purposes. Possible values are `true` and `false`.
Default value is `false`.  
- `APP_PERSISTENCE_PROVIDER` - value to set persistence provider used
by application. Possible values are `inmemory`, `jdbc` and `jooq`. In case
of using `inmemory` persistence provider, no data is persisted between restarts
of the application. Default value is `jooq`.

### <a name="env_vars_for_jar" />4.2. Setting environment variables for running JAR file
If you launch your application via JAR file, you can specify values 
for environment variables in command line using `-D` switch. Example 1:
```
java -DAPP_AUTOINSERT_ENABLED=true -jar build/libs/spring_3_8_contactlist-0.0.1-SNAPSHOT.jar
```

Also you can specify multiple values. Example 2:
```
java -DDB_USERNAME=mylogin -DDB_PASSWORD=mypassword -jar build/libs/spring_3_8_contactlist-0.0.1-SNAPSHOT.jar
```

Example 3:
```
java \
-DDB_URL=jdbc:postgresql://mypgsqlhostname:2345/mycontactsdb \
-DDB_USERNAME=mylogin \
-DDB_PASSWORD=mypassword \
-DAPP_PERSISTENCE_PROVIDER=jdbc \
-jar build/libs/spring_3_8_contactlist-0.0.1-SNAPSHOT.jar
```

### <a name="env_vars_for_docker" />4.3. Setting environment variables for running Docker setup
First of all, environment variable `DB_URL` with proper hostname is already set
in `docker/docker-compose.yaml` file, so you are not required
to override `DB_URL` just because of using Docker setup. The tested way to pass
environment variables with `docker compose` is to use
`docker compose run webapp` command with `-e`, instead of `docker compose up`, 
with further shutdown via `docker compose down --remove-orphans`. 
So, the example of passing environment variables to Docker setup is:
```
docker compose -f docker/docker-compose.yaml run --service-ports -e APP_AUTOINSERT_ENABLED=true -d webapp
```

## <a name="copyright" />5. Copyright
&copy; Albert Rayanov, January 2024, city of Darmstadt, Hesse, Germany.  
[My Java repository](https://gitlab.com/northsoft/java-tasks), 
[my CV](https://docdro.id/AjhwsdR), 
[my LinkedIn](https://www.linkedin.com/in/albert-rayanov/).
