#!/bin/bash

# This script downloads rendered versions of HTML pages, substitutes a path of local CSS script,
# so in order to verify design of pages there is no need to rebuild the whole application.

# General settings

remote_root_url='http://localhost:8088/'; # slash at the end
remote_page_urls=(
  ''
  'contacts/create'
  'contacts/edit/100500'
);
local_page_filenames=(
  'index'
  'contacts_create'
  'client_side_error'
);
local_output_dir='/tmp/webdev/'; # slash at the end
local_css_file='/home/northsoft/java-tasks/spring_3_8_contactlist/src/main/resources/static/css/common.css';

# Miscellaneous settings

local_output_extension='.html';
error_output='/dev/stderr';
local_page_title_caption='[LOCAL, %s] ';
local_page_title_time_format='%Y.%m.%d %H:%M:%S';

# Error reporting and logging
readonly LOG_FATAL=1;
readonly LOG_ERROR=2;
readonly LOG_WARN=3;
readonly LOG_INFO=4;
readonly LOG_DEBUG=5;
log_level=$LOG_INFO;

verbose='';
if [[ "$log_level" -ge "$LOG_DEBUG" ]]; then
  verbose='--verbose';
fi;

function log {
  if [[ $# != 2 ]]; then
    if [[ "$log_level" -ge "$LOG_FATAL" ]]; then
      echo "FATAL: log: misuse of function, usage: log level message" > "$error_output";
      exit 1;
    fi;
  fi;
  level="$1";
  msg="$2";

  if [[ $level -gt $log_level ]]; then
    return;
  fi;

  level_msgs=([1]='FATAL' [2]='ERROR' [3]='WARN' [4]='INFO' [5]='DEBUG');
  if [[ ! -v "level_msgs[$level]" ]]; then
    echo "FATAL: log: incorrect level ($level)" > "$error_output";
    exit 1;
  fi;
  level_msg="${level_msgs[$level]}";
  echo "$level_msg: $msg" > "$error_output";
  if [[ "$level" -le $LOG_FATAL ]]; then
    echo "[script terminates abnormally]" > "$error_output";
    exit 1;
  fi;
}

# main functions

function assert_executable_exists {
  fn='assert_executable_exists';
  if [[ $# -ne 1 ]]; then
    log $LOG_FATAL "$fn: misuse, usage: $fn command";
  fi;
  command="$1";
  which "$command" > '/dev/null'; ret=$?;
  if [[ "$ret" != 0 ]]; then
    log $LOG_FATAL "$fn: unable to locate $command";
  fi;
}

function init_check {
  fn='init_check';
  if [[ "${#remote_page_urls[@]}" != "${#local_page_filenames[@]}" ]]; then
    msg="lengths of remote_page_urls(${#remote_page_urls[@]})";
    msg="$msg and local_page_filenames(${#local_page_filenames[@]}) do not match";
    log $LOG_FATAL "$fn: $msg";
  fi;

  if [[ ! -f "$local_css_file" ]]; then
    log $LOG_FATAL "$fn: local CSS file was not found on path «$local_css_file»";
  fi;

  if [[ ! -d "$local_output_dir" ]]; then
    mkdir $verbose --parents "$local_output_dir"; ret="$?";
    if [[ "$ret" != 0 || ! -d "$local_output_dir" ]]; then
      log $LOG_FATAL "$fn: unable to create output directory «$local_output_dir»";
    fi;
  fi;

  assert_executable_exists 'curl';
  assert_executable_exists 'grep';

  log $LOG_DEBUG "$fn: All initial checks succeeded";
}

function assert_file_is_writable {
  fn='assert_file_is_writable';
  if [[ $# -ne 1 ]]; then
    log $LOG_FATAL "$fn: misuse, usage: $fn file";
  fi;
  file="$1";
  if [[ ! -w "$file" ]]; then
    touch "$file";
    if [[ ! -w "$file" ]]; then
      log $LOG_FATAL "$fn: file «$file» is not writable";
    fi;
  fi;
}

function convert_local_file {
  fn='convert_local_file';
  if [[ $# -ne 2 ]]; then
    log $LOG_FATAL "$fn: misuse, usage: $fn downloaded_remote_file target_local_file";
  fi;
  file_to_convert="$1";
  target_local_file="$2";

  if ! grep -q "$sed_cmd1_regexp" "$file_to_convert" || ! grep -q "$sed_cmd2_regexp" "$file_to_convert"; then
    log $LOG_ERROR "$fn: downloaded file does not contain expected regular expressions";
    return 1;
  fi;

  local_date_time="$(date +"$local_page_title_time_format")"
  local_page_title_caption_rendered="$(printf "$local_page_title_caption" "$local_date_time")";

  sed_cmd1_regexp='\(^[ ]*\)[\<]title[\>]\([^\>]*\)[\<]/title[\>]$';
  sed_cmd1_replacement='\1<title>'"$local_page_title_caption_rendered"'\2</title>';
  sed_cmd1_composed="s#$sed_cmd1_regexp#$sed_cmd1_replacement#g";

  sed_cmd2_regexp='^\([ ]*\)[\<]link[ ]rel="stylesheet"[ ]href="[^"]*"[ ]/[\>]$';
  sed_cmd2_replacement='\1<link rel="stylesheet" href="file://'"$local_css_file"'" />';
  sed_cmd2_composed="s#$sed_cmd2_regexp#$sed_cmd2_replacement#g";

  sed "$sed_cmd1_composed" "$file_to_convert" | sed "$sed_cmd2_composed" > "$target_local_file";
  return 0;
}

function process_url_to_local_file {
  fn='process_url_to_local_file';
  if [[ $# -ne 2 ]]; then
    log $LOG_FATAL "$fn: misuse, usage: $fn remote_url local_file";
  fi;
  remote_url="$1";
  local_file="$2";
  copy_of_remote_file="$(mktemp)";
  log $LOG_DEBUG "$fn: processing URL «$remote_url» to file «$local_file» with temporary file «$copy_of_remote_file»";

  assert_file_is_writable "$local_file";
  assert_file_is_writable "$copy_of_remote_file";

  curl_report_opts_per_level=(
    [1]='--silent'
    [2]='--silent --show-error'
    [3]='--silent --show-error'
    [4]=''
    [5]='--verbose'
  );
  curl_report_opts="${curl_report_opts_per_level[$log_level]}";
  curl --stderr "$error_output" $curl_report_opts --clobber --output "$copy_of_remote_file" "$remote_url"; ret=$?;
  if [[ $ret != 0 ]]; then
    log $LOG_ERROR "$fn: unable to load URL «$remote_url» to file «$copy_of_remote_file», curl error $ret";
    return 1;
  fi;
  convert_local_file "$copy_of_remote_file" "$local_file";
}

function main {
  init_check;
  for ((idx=0; idx<${#remote_page_urls[@]}; ++idx)); do
    remote_url="$remote_root_url${remote_page_urls[$idx]}";
    local_output_file="$local_output_dir${local_page_filenames[$idx]}$local_output_extension";
    process_url_to_local_file "$remote_url" "$local_output_file";
  done;
}

main;
