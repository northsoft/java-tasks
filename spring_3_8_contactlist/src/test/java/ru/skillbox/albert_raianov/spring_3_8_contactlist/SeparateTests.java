package ru.skillbox.albert_raianov.spring_3_8_contactlist;

import org.junit.jupiter.api.Test;
import ru.skillbox.albert_raianov.spring_3_8_contactlist.data.Contact;

import static org.junit.jupiter.api.Assertions.*;
class SeparateTests {
    @Test
    void contact_isValidPhoneNumber() {
        assertTrue(Contact.isValidPhoneNumber("+7(953)789-01-23"));
        assertTrue(Contact.isValidPhoneNumber("+374 55 456 789"));
        assertTrue(Contact.isValidPhoneNumber("+491742790123"));
        assertFalse(Contact.isValidPhoneNumber("2+3+4+5+6"));
        assertFalse(Contact.isValidPhoneNumber("123)4567"));
        assertFalse(Contact.isValidPhoneNumber("123(4567"));
    }
}
