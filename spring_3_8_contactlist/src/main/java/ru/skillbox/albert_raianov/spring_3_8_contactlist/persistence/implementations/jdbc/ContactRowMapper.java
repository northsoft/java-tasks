package ru.skillbox.albert_raianov.spring_3_8_contactlist.persistence.implementations.jdbc;

import org.springframework.jdbc.core.RowMapper;
import ru.skillbox.albert_raianov.spring_3_8_contactlist.data.Contact;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ContactRowMapper implements RowMapper<Contact> {
    @Override
    public Contact mapRow(ResultSet rs, int rowNum) throws SQLException {
        return new Contact(
                rs.getLong(Contact.Fields.id),
                rs.getString(Contact.Fields.firstName),
                rs.getString(Contact.Fields.lastName),
                rs.getString(Contact.Fields.email),
                rs.getString(Contact.Fields.phone)
        );
    }
}
