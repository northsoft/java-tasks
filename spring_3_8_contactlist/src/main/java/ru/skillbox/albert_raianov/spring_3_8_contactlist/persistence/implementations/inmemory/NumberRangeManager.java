package ru.skillbox.albert_raianov.spring_3_8_contactlist.persistence.implementations.inmemory;

import java.util.TreeMap;

public class NumberRangeManager {
    private final TreeMap<Long, Long> storedRanges = new TreeMap<>();

    public void addNumber(long value) {
        if (storedRanges.isEmpty()) {
            storedRanges.put(value, value);
            return;
        }

        if (valueIsAlreadyStored(value)) {
            return;
        }

        if (valueWillJoinTwoRanges(value)) {
            long leftValueBeforeKey = storedRanges.floorKey(value);
            long leftValueAfterKey = storedRanges.ceilingKey(value);
            long rightValueAfterKey = storedRanges.get(leftValueAfterKey);
            storedRanges.remove(leftValueAfterKey);
            storedRanges.put(leftValueBeforeKey, rightValueAfterKey);
            return;
        }

        if (valueWillJoinToLeftRange(value)) {
            long leftValueBeforeKey = storedRanges.floorKey(value);
            storedRanges.put(leftValueBeforeKey, value);
            return;
        }

        if (valueWillJoinToRightRange(value)) {
            long leftValueAfterKey = storedRanges.ceilingKey(value);
            long rightValueAfterKey = storedRanges.get(leftValueAfterKey);
            storedRanges.remove(leftValueAfterKey);
            storedRanges.put(value, rightValueAfterKey);
            return;
        }

        storedRanges.put(value, value);
    }

    public void removeNumber(long valueToRemove) {
        if (storedRanges.isEmpty() || !valueIsAlreadyStored(valueToRemove)) {
            return;
        }

        if (valueIsSingle(valueToRemove)) {
            storedRanges.remove(valueToRemove);
            return;
        }

        if (valueIsInsideRangeBorders(valueToRemove)) {
            long oldLeftValue = storedRanges.floorKey(valueToRemove);
            long oldRightValue = storedRanges.get(oldLeftValue);
            storedRanges.put(oldLeftValue, valueToRemove - 1);
            storedRanges.put(valueToRemove + 1, oldRightValue);
            return;
        }

        if (valueIsAtLeftBorder(valueToRemove)) {
            long oldRightValue = storedRanges.get(valueToRemove);
            storedRanges.remove(valueToRemove);
            storedRanges.put(valueToRemove + 1, oldRightValue);
            return;
        }

        if (valueIsAtRightBorder(valueToRemove)) {
            long oldLeftValue = storedRanges.floorKey(valueToRemove);
            storedRanges.put(oldLeftValue, valueToRemove - 1);
        }
    }

    public long getFirstFreeValue() {
        if (storedRanges.isEmpty() || storedRanges.firstKey() != 1) {
            return 1;
        }
        return storedRanges.get(storedRanges.firstKey()) + 1;
    }

    private boolean valueIsAlreadyStored(long value) {
        Long leftValueAfterKey = storedRanges.ceilingKey(value);
        if (leftValueAfterKey != null && leftValueAfterKey == value) {
            return true;
        }

        Long leftValueBeforeKey = storedRanges.floorKey(value);
        if (leftValueBeforeKey != null) {
            long rightValueBeforeKey = storedRanges.get(leftValueBeforeKey);
            return rightValueBeforeKey >= value;
        }

        return false;
    }

    private boolean valueWillJoinTwoRanges(long valueToAdd) {
        Long leftValueBeforeKey = storedRanges.floorKey(valueToAdd);
        if (leftValueBeforeKey == null) {
            return false;
        }
        Long leftValueAfterKey = storedRanges.ceilingKey(valueToAdd);
        if (leftValueAfterKey == null) {
            return false;
        }
        long rightValueBeforeKey = storedRanges.get(leftValueBeforeKey);
        return rightValueBeforeKey == valueToAdd - 1 && leftValueAfterKey == valueToAdd + 1;
    }

    private boolean valueWillJoinToLeftRange(long valueToAdd) {
        Long leftValueBeforeKey = storedRanges.floorKey(valueToAdd);
        if (leftValueBeforeKey == null) {
            return false;
        }
        long rightValueBeforeKey = storedRanges.get(leftValueBeforeKey);
        return rightValueBeforeKey == valueToAdd - 1;
    }

    private boolean valueWillJoinToRightRange(long valueToAdd) {
        Long leftValueAfterKey = storedRanges.ceilingKey(valueToAdd);
        if (leftValueAfterKey == null) {
            return false;
        }

        return leftValueAfterKey == valueToAdd + 1;
    }

    private boolean valueIsInsideRangeBorders(long value) {
        Long leftValueBeforeKey = storedRanges.floorKey(value);
        if (leftValueBeforeKey == null) {
            return false;
        }
        long rightValue = storedRanges.get(leftValueBeforeKey);
        return leftValueBeforeKey < value && value < rightValue;
    }

    private boolean valueIsSingle(long value) {
        Long leftValueBeforeKey = storedRanges.floorKey(value);
        if (leftValueBeforeKey == null) {
            return false;
        }
        long rightValue = storedRanges.get(leftValueBeforeKey);
        return leftValueBeforeKey == value && rightValue == value;
    }

    private boolean valueIsAtLeftBorder(long value) {
        Long leftValueBeforeKey = storedRanges.floorKey(value);
        if (leftValueBeforeKey == null) {
            return false;
        }
        long rightValue = storedRanges.get(leftValueBeforeKey);
        return leftValueBeforeKey == value && rightValue > value;
    }

    private boolean valueIsAtRightBorder(long value) {
        Long leftValueBeforeKey = storedRanges.floorKey(value);
        if (leftValueBeforeKey == null) {
            return false;
        }
        long rightValue = storedRanges.get(leftValueBeforeKey);
        return leftValueBeforeKey < value && rightValue == value;
    }
}
