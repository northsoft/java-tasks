package ru.skillbox.albert_raianov.spring_3_8_contactlist.persistence.common;

import java.util.List;
import ru.skillbox.albert_raianov.spring_3_8_contactlist.persistence.exceptions.PersistenceException;


/**
 * This is&nbsp;a&nbsp;simple interface supposed to&nbsp;unify basic approaches of&nbsp;persisting <b>values</b>
 * in&nbsp;various storages and&nbsp;via&nbsp;various tools.<br />
 * Hereinafter:
 * <ul>
 *     <li>instances of&nbsp;{@link Type} are&nbsp;referred&nbsp;to as&nbsp;<b>values</b>;</li>
 *     <li>persisted <b>values</b> are&nbsp;referred&nbsp;to as&nbsp;<b>records</b>;</li>
 *     <li>
 *         an&nbsp;implementation (some) of&nbsp;this interface is&nbsp;referred&nbsp;to
 *         as&nbsp;<b>an&nbsp;Implementation</b>.
 *     </li>
 * </ul>
 * Any <b>Implementation</b> is&nbsp;supposed&nbsp;to:
 * <ul>
 *     <li>verify <b>values</b> and&nbsp;ids via routines of&nbsp;{@link RepositoryVerifier};</li>
 *     <li>set ids of&nbsp;<b>values</b> for newly created <b>records</b>;</li>
 *     <li>
 *         throw {@link ru.skillbox.albert_raianov.spring_3_8_contactlist.persistence.exceptions.PersistenceException
 *         PersistenceException} in&nbsp;case of&nbsp;any&nbsp;misuse.
 *     </li>
 * </ul>
 * Any <b>Implementation</b> is recommended to be&nbsp;enabled according to&nbsp;unique value of&nbsp;property
 * <code>contactlist.persistence.provider</code>,
 * e.g.&nbsp;via <code>@ConditionalOnProperty(name = "contactlist.persistence.provider",
 * havingValue = "my_some_impl_name")</code>.
 * @param <Type> a&nbsp;type for&nbsp;a&nbsp;single <b>value</b> supposed to&nbsp;be&nbsp;persisted. Should have
 * mutable&nbsp;<code>id</code> of&nbsp;type <code>Long</code>, and hence implement {@link ClassWithLongId}.
 * <i>The&nbsp;{@link Long}&nbsp;type is&nbsp;hardcoded (instead of&nbsp;being&nbsp;set as&nbsp;an&nbsp;interface
 * type argument), because generation of&nbsp;<code>id</code> for&nbsp;a&nbsp;newly created <b>record</b>
 * is&nbsp;a&nbsp;responsibility of&nbsp;<b>an&nbsp;Implementation</b>, and&nbsp;in&nbsp;most cases there&nbsp;is
 * no&nbsp;clear answer, how&nbsp;<b>an&nbsp;Implementation</b> should generate a&nbsp;consequent value
 * of&nbsp;a&nbsp;user&#x2011;provided type for&nbsp;<code>id</code>.</i>
 * @author Albert Rayanov
 * @see ClassWithLongId
 * @see RepositoryVerifier
 * @see org.springframework.boot.autoconfigure.condition.ConditionalOnProperty ConditionalOnProperty
 */

public interface PersistenceProvider<Type extends ClassWithLongId> {

    /**
     * Queries all <b>records</b> previously persisted by&nbsp;<b>an&nbsp;Implementation</b>.
     * @return list of&nbsp;previously persisted <b>values</b>.
     * Sorting order is&nbsp;defined by&nbsp;<b>an&nbsp;Implementation</b>.
     */
    List<Type> getAll() throws PersistenceException;

    /**
     * Queries <b>record</b> with specified&nbsp;<code>id</code>.
     * @param id the&nbsp;non&#x2011;null&nbsp;id of&nbsp;a&nbsp;<b>record</b> supposed to&nbsp;be&nbsp;returned.
     * @return <b>record</b> with requested&nbsp;<code>id</code>.
     *
     * @throws ru.skillbox.albert_raianov.spring_3_8_contactlist.persistence.exceptions.RecordNotFoundException
     * if&nbsp;the&nbsp;requested value is&nbsp;not&nbsp;found
     *
     * @throws ru.skillbox.albert_raianov.spring_3_8_contactlist.persistence.exceptions.IllegalArgumentException
     * if&nbsp;<code>id</code> is&nbsp;<code>null</code>.
     */
    Type getById(Long id) throws PersistenceException;

    /**
     * Persists passed <b>value</b> as&nbsp;a&nbsp;unique, not&nbsp;persisted previously.
     * @param value non&#x2011;null <b>value</b> with mandatory null&nbsp;<code>id</code>.
     * This method internally calls <code>value</code>'s {@link ClassWithLongId#setId setId} to&nbsp;set
     * <code>id</code> according to&nbsp;approach of&nbsp;id&nbsp;generation strategy used
     * by&nbsp;<b>an&nbsp;Implementation</b>.
     * @throws ru.skillbox.albert_raianov.spring_3_8_contactlist.persistence.exceptions.IllegalArgumentException
     * if&nbsp;<code>id</code> is&nbsp;<code>null</code>.
     */

    void createNewAndSetId(Type value) throws PersistenceException;

    /**
     * Updates data for&nbsp;a&nbsp;single <b>record</b> persisted previously. The&nbsp;<b>record</b> to&nbsp;update
     * is&nbsp;chosen by&nbsp;<code>value</code>'s {@link ClassWithLongId#getId() getId()} call. The&nbsp;data
     * to&nbsp;set is&nbsp;taken from&nbsp;<code>value</code>. There&nbsp;is no&nbsp;way to&nbsp;change&nbsp;id
     * of&nbsp;a&nbsp;<b>record</b>.
     * @param value source of&nbsp;the&nbsp;data to&nbsp;be&nbsp;set in&nbsp;<b>record</b>, except&nbsp;<code>id</code>.
     * The&nbsp;<code>id</code> from&nbsp;<code>value</code> is&nbsp;used to&nbsp;identify <b>record</b> to&nbsp;update.
     * @throws ru.skillbox.albert_raianov.spring_3_8_contactlist.persistence.exceptions.IllegalArgumentException
     * if&nbsp;<code>value</code> is&nbsp;<code>null</code>, or&nbsp;its {@link ClassWithLongId#getId() getId()}
     * returns&nbsp;<code>null</code>.
     * @throws ru.skillbox.albert_raianov.spring_3_8_contactlist.persistence.exceptions.RecordNotFoundException
     * if&nbsp;a&nbsp;record identified by&nbsp;<code>value</code>'s {@link ClassWithLongId#getId() getId()}
     * is&nbsp;not&nbsp;found.
     */
    void update(Type value) throws PersistenceException;

    /**
     * Deletes <b>record</b> identified by&nbsp;<code>id</code>.
     * @param id The&nbsp;non&#x2011;null&nbsp;id of&nbsp;<b>record</b> to&nbsp;delete.
     * @throws ru.skillbox.albert_raianov.spring_3_8_contactlist.persistence.exceptions.IllegalArgumentException
     * if&nbsp;<code>id</code> is&nbsp;<code>null</code>
     * @throws ru.skillbox.albert_raianov.spring_3_8_contactlist.persistence.exceptions.RecordNotFoundException
     * if&nbsp;a&nbsp;record identified&nbsp;by&nbsp;<code>id</code> is&nbsp;not&nbsp;found.
     */
    void delete(Long id) throws PersistenceException;

    /**
     * Performs a&nbsp;batch&#x2011;insert operation for&nbsp;provided list of&nbsp;<b>values</b> and&nbsp;sets ids
     * after successful completion.
     * @param values list of&nbsp;<b>values</b> to&nbsp;insert. No&nbsp;<code>null</code> list items are&nbsp;allowed.
     * Every <b>value</b> from this list should have <code>id</code> equal&nbsp;to&nbsp;<code>null</code>.
     * @throws ru.skillbox.albert_raianov.spring_3_8_contactlist.persistence.exceptions.IllegalArgumentException
     * if&nbsp;<code>values</code> contain&nbsp;a&nbsp;<code>null</code> <b>value</b>, or&nbsp;a&nbsp;<b>value</b>
     * with {@link ClassWithLongId#getId() id} not&nbsp;equal&nbsp;to&nbsp;<code>null</code>.
     */
    void batchInsertAndSetIds(List<Type> values) throws PersistenceException;
}
