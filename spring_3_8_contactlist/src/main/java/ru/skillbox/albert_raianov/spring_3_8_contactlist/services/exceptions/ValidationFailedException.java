package ru.skillbox.albert_raianov.spring_3_8_contactlist.services.exceptions;

public class ValidationFailedException extends ServiceLayerReportableException {
    public ValidationFailedException(String message) {
        super(message, "Please verify your input data and try again");
    }
}
