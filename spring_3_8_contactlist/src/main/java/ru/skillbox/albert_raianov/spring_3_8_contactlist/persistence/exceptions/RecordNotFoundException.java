package ru.skillbox.albert_raianov.spring_3_8_contactlist.persistence.exceptions;

/**
 * This exception is&nbsp;thrown when an&nbsp;implementation of&nbsp;{@link
 * ru.skillbox.albert_raianov.spring_3_8_contactlist.persistence.common.PersistenceProvider PersistenceProvider}
 * is&nbsp;requested to&nbsp;perform some operation on&nbsp;an&nbsp;existent record (like query, update or&nbsp;delete),
 * but the&nbsp;requested record was not found, e.&nbsp;g.&nbsp;it&nbsp;was never persisted before, or&nbsp;it&nbsp;is
 * already deleted.
 */
public class RecordNotFoundException extends PersistenceException {
    public RecordNotFoundException() {
    }

    public RecordNotFoundException(String message) {
        super(message);
    }

    public RecordNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public RecordNotFoundException(Throwable cause) {
        super(cause);
    }

    public RecordNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
