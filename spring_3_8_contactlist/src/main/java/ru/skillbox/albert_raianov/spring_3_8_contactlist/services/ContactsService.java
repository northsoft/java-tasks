package ru.skillbox.albert_raianov.spring_3_8_contactlist.services;

import ru.skillbox.albert_raianov.spring_3_8_contactlist.data.Contact;
import ru.skillbox.albert_raianov.spring_3_8_contactlist.services.exceptions.ServiceLayerException;

import java.util.List;

public interface ContactsService {
    List<Contact> getAllContacts() throws ServiceLayerException;

    Contact getContactById(Long id) throws ServiceLayerException;

    void createNewContact(Contact contact) throws ServiceLayerException;

    void updateContact(Contact contact) throws ServiceLayerException;

    void deleteContact(Long id) throws ServiceLayerException;

    void batchInsert(List<Contact> contacts) throws ServiceLayerException;
}
