package ru.skillbox.albert_raianov.spring_3_8_contactlist.persistence.implementations.inmemory;

import java.util.HashMap;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Repository;
import ru.skillbox.albert_raianov.spring_3_8_contactlist.persistence.common.ClassWithLongId;
import ru.skillbox.albert_raianov.spring_3_8_contactlist.persistence.common.PersistenceProvider;
import ru.skillbox.albert_raianov.spring_3_8_contactlist.persistence.common.RepositoryVerifier;
import ru.skillbox.albert_raianov.spring_3_8_contactlist.persistence.exceptions.PersistenceException;
import ru.skillbox.albert_raianov.spring_3_8_contactlist.persistence.exceptions.RecordNotFoundException;
import ru.skillbox.albert_raianov.spring_3_8_contactlist.util.ExceptionWrapper;


@Repository
@ConditionalOnProperty(name = "contactlist.persistence.provider", havingValue = "inmemory")
@RequiredArgsConstructor
public class InMemoryContactsPersistenceProvider<Type extends ClassWithLongId> implements PersistenceProvider<Type> {
    private final ExceptionWrapper<PersistenceException> persistenceExceptionWrapper =
            new ExceptionWrapper<>(PersistenceException.class);

    @Override
    public List<Type> getAll() {
        return persistenceExceptionWrapper.wrap(
                () -> storedObjects.values().stream().toList(),
                "Unexpected exception during getAll()"
        );
    }

    private final HashMap<Long, Type> storedObjects = new HashMap<>();
    private final NumberRangeManager numberRangeManager = new NumberRangeManager();
    private final RepositoryVerifier<Type> repositoryVerifier;
    @Override
    public Type getById(Long id) throws PersistenceException {
        return persistenceExceptionWrapper.wrap(() -> {
            repositoryVerifier.assertCanGetById(id);
            return storedObjects.values().stream()
                    .filter(value -> value.getId().equals(id))
                    .findFirst()
                    .orElseThrow(() -> new RecordNotFoundException("Value with id " + id + " was not found"));
        },
        "Unexpected exception during getById(" + id + ")");
    }

    @Override
    public void createNewAndSetId(Type value) throws PersistenceException {
        persistenceExceptionWrapper.wrap(() -> {
            repositoryVerifier.assertCanCreateNew(value);
            Long newId = numberRangeManager.getFirstFreeValue();
            value.setId(newId);
            storedObjects.put(newId, value);
            numberRangeManager.addNumber(newId);
        }, "Unexpected exception during createNewAndSetId(" + value + ")");
    }

    @Override
    public void update(Type value) throws PersistenceException {
        persistenceExceptionWrapper.wrap(() -> {
            repositoryVerifier.assertCanUpdate(value);
            if (storedObjects.containsKey(value.getId())) {
                storedObjects.put(value.getId(), value);
            } else {
                throw new RecordNotFoundException("No key " + value.getId() + " found");
            }
        }, "Unexpected exception during update(" + value + ")");
    }

    @Override
    public void delete(Long id) {
        persistenceExceptionWrapper.wrap(() -> {
            repositoryVerifier.assertCanDelete(id);
            if (storedObjects.containsKey(id)) {
                storedObjects.remove(id);
                numberRangeManager.removeNumber(id);
            } else {
                throw new RecordNotFoundException("No key " + id + " found");
            }
        }, "Unexpected exception during delete(" + id + ")");
    }

    @Override
    public void batchInsertAndSetIds(List<Type> values) throws PersistenceException {
        persistenceExceptionWrapper.wrap(() -> {
            repositoryVerifier.assertCanBatchInsert(values);
            values.forEach(value -> {
                long newId = numberRangeManager.getFirstFreeValue();
                value.setId(newId);
                storedObjects.put(newId, value);
                numberRangeManager.addNumber(newId);
            });
        }, "Unexpected exception during batchInsertAndSetIds(" + values + ")");
    }
}

