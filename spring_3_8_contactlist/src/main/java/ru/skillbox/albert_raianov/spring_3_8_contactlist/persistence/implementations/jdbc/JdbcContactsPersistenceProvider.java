package ru.skillbox.albert_raianov.spring_3_8_contactlist.persistence.implementations.jdbc;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.dao.support.DataAccessUtils;
import org.springframework.jdbc.core.ArgumentPreparedStatementSetter;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapperResultSetExtractor;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import ru.skillbox.albert_raianov.spring_3_8_contactlist.data.Contact;
import ru.skillbox.albert_raianov.spring_3_8_contactlist.persistence.common.PersistenceProvider;
import ru.skillbox.albert_raianov.spring_3_8_contactlist.persistence.common.RepositoryVerifier;
import ru.skillbox.albert_raianov.spring_3_8_contactlist.persistence.exceptions.PersistenceException;
import ru.skillbox.albert_raianov.spring_3_8_contactlist.persistence.exceptions.RecordNotFoundException;
import ru.skillbox.albert_raianov.spring_3_8_contactlist.util.ExceptionWrapper;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Pattern;

@Repository
@ConditionalOnProperty(name = "contactlist.persistence.provider", havingValue = "jdbc")
@RequiredArgsConstructor
@Log4j2
public class JdbcContactsPersistenceProvider implements PersistenceProvider<Contact> {
    private final JdbcTemplate jdbcTemplate;
    private final RepositoryVerifier<Contact> repositoryVerifier;
    private final ExceptionWrapper<PersistenceException> persistenceExceptionWrapper =
              new ExceptionWrapper<>(PersistenceException.class);

    @Override
    public List<Contact> getAll() throws PersistenceException {
        return persistenceExceptionWrapper.wrap(() -> {
            log.debug("JdbcContactsPersistenceProvider.getAll()");
            String sql = "SELECT * FROM contacts_schema.contacts";
            return jdbcTemplate.query(sql, new ContactRowMapper());
        }, "Unexpected exception during getAll()");
    }

    @Override
    public Contact getById(Long id) throws PersistenceException {
        return persistenceExceptionWrapper.wrap(() -> {
            repositoryVerifier.assertCanGetById(id);
            log.debug("JdbcContactsPersistenceProvider.getById({})", id);
            String sql = "SELECT * FROM contacts_schema.contacts WHERE id = ?";
            Contact result = DataAccessUtils.singleResult(jdbcTemplate.query(
                    sql,
                    new ArgumentPreparedStatementSetter(new Object[]{id}),
                    new RowMapperResultSetExtractor<>(new ContactRowMapper(), 1)
            ));
            if (result == null) {
                throw new RecordNotFoundException("Value with id " + id + " was not found");
            }
            return result;
        }, "Unexpected exception during getById(" + id + ")");
    }

    @Override
    public void createNewAndSetId(Contact value) throws PersistenceException {
        persistenceExceptionWrapper.wrap(() -> {
            repositoryVerifier.assertCanCreateNew(value);
            log.debug("JdbcContactsPersistenceProvider.createNewAndSetId({})", value);
            String sql = "INSERT INTO contacts_schema.contacts(firstName, lastName, email, phone) VALUES(?, ?, ?, ?)";
            KeyHolder keyHolder = new GeneratedKeyHolder();
            jdbcTemplate.update(connection -> {
                PreparedStatement ps = connection.prepareStatement(sql, new String[]{Contact.Fields.id});
                ps.setString(1, value.getFirstName());
                ps.setString(2, value.getLastName());
                ps.setString(3, value.getEmail());
                ps.setString(4, value.getPhone());
                return ps;
            }, keyHolder);
            log.debug("Added value with id {}", keyHolder.getKey());
            value.setId(Objects.requireNonNull(keyHolder.getKey()).longValue());
        }, "Unexpected exception during createNewAndSetId(" + value + ")");
    }

    @Override
    public void update(Contact value) throws PersistenceException {
        persistenceExceptionWrapper.wrap(() -> {
            repositoryVerifier.assertCanUpdate(value);
            log.debug("JdbcContactsPersistenceProvider.update({})", value);
            String sql = "UPDATE contacts_schema.contacts " +
                    "SET firstName = ?, lastName = ?, email = ?, phone = ? WHERE id = ?";
            int nAffected = jdbcTemplate.update(
                    sql,
                    value.getFirstName(),
                    value.getLastName(),
                    value.getEmail(),
                    value.getPhone(),
                    value.getId()
            );
            if (nAffected == 0) {
                throw new RecordNotFoundException("Value with id " + value.getId() + " was not found");
            }
        }, "Unexpected exception during update(" + value + ")");
    }

    @Override
    public void delete(Long id) throws PersistenceException {
        persistenceExceptionWrapper.wrap(() -> {
            repositoryVerifier.assertCanDelete(id);
            log.debug("JdbcContactsPersistenceProvider.delete({})", id);

            String sql = "DELETE FROM contacts_schema.contacts WHERE id = ?";
            int nAffected = jdbcTemplate.update(sql, id);
            if (nAffected == 0) {
                throw new RecordNotFoundException("Value with id " + id + " was not found");
            }
        }, "Unexpected exception during delete(" + id + ")");
    }

    @Override
    public void batchInsertAndSetIds(List<Contact> values) throws PersistenceException {
        persistenceExceptionWrapper.wrap(() -> {
            repositoryVerifier.assertCanBatchInsert(values);

            String sql = "INSERT INTO contacts_schema.contacts(firstName, lastName, email, phone) VALUES(?, ?, ?, ?)";
            KeyHolder keyHolder = new GeneratedKeyHolder();
            jdbcTemplate.batchUpdate(
                    (Connection connection) -> connection.prepareStatement(sql, new String[]{"id"}),
                    new BatchPreparedStatementSetter() {
                        @Override
                        public void setValues(PreparedStatement ps, int i) throws SQLException {
                            Contact value = values.get(i);
                            ps.setString(1, value.getFirstName());
                            ps.setString(2, value.getLastName());
                            ps.setString(3, value.getEmail());
                            ps.setString(4, value.getPhone());
                        }

                        @Override
                        public int getBatchSize() {
                            return values.size();
                        }
                    },
                    keyHolder
            );
            List<Map<String, Object>> keyList = keyHolder.getKeyList();
            assertKeyListIsCorrect(keyList, values.size());
            for (int i = 0; i < values.size(); ++i) {
                values.get(i).setId(Long.parseLong(keyList.get(i).get("id").toString()));
            }
        }, "Unexpected exception during batchInsertAndSetIds(" + values + ")");
    }

    private void assertKeyListIsCorrect(List<Map<String, Object>> keyList, int expectedSize) {
        if (keyList.size() != expectedSize) {
            throw new PersistenceException(String.format(
                    "Error during batch insert: got %d values to insert and %d keys from DBMS (%s)",
                    expectedSize,
                    keyList.size(),
                    keyList
            ));
        }
        Pattern numericPattern = Pattern.compile("\\d+");
        keyList.forEach((Map<String, Object> keyToObject) -> {
            if (!keyToObject.containsKey("id")) {
                throw new PersistenceException(String.format(
                        "Key list (%s) has no \"id\" key in record (%s)", keyList, keyToObject
                ));
            }
            boolean idValueMatchesInteger = numericPattern.matcher(keyToObject.get("id").toString()).matches();
            if (!idValueMatchesInteger) {
                throw new PersistenceException(String.format(
                        "Key list (%s) in record (%s) has \"id\" (%s) which is not a key",
                        keyList, keyToObject, keyToObject.get("id")
                ));
            }
        });
    }
}
