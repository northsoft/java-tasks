package ru.skillbox.albert_raianov.spring_3_8_contactlist.services;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.skillbox.albert_raianov.spring_3_8_contactlist.data.Contact;
import ru.skillbox.albert_raianov.spring_3_8_contactlist.services.exceptions.ServiceLayerException;
import ru.skillbox.albert_raianov.spring_3_8_contactlist.util.ExceptionWrapper;

import java.util.List;

@Component
@ConditionalOnProperty("contactlist.autoinsert.enabled")
@RequiredArgsConstructor
public class ContactsAutomaticInsertService {
        private final ContactsService contactsService;
        private final ExceptionWrapper<ServiceLayerException> serviceExceptionWrapper =
                new ExceptionWrapper<>(ServiceLayerException.class);
        @EventListener(ApplicationStartedEvent.class)
        public void addSampleRecords() {
            serviceExceptionWrapper.wrap(() -> {
                List<Contact> contacts = List.of(
                        new Contact(
                                null,
                                "Альберт",
                                "Раянов",
                                "albert@novosibirsk.ru",
                                "+7(953)7890123"
                        ),
                        new Contact(
                                null,
                                "ԱԼԲԵՐՏ",
                                "ՌԱՅԱՆՈՎ",
                                "albert@yerevan.am",
                                "+374(55)456789"
                        ),
                        new Contact(
                                null,
                                "Albert",
                                "Rajanow",
                                "albert@darmstadt.de",
                                "+49(174)2791357"
                        )
                );
                contactsService.batchInsert(contacts);
            }, "Unable to perform automatic insertion on startup");
        }

}
