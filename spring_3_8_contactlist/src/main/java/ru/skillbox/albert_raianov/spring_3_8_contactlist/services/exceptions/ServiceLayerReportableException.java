package ru.skillbox.albert_raianov.spring_3_8_contactlist.services.exceptions;

import lombok.Getter;

@Getter
public class ServiceLayerReportableException extends ServiceLayerException {
    private final String description;

    public ServiceLayerReportableException(String message, String description, Throwable cause) {
        super(message, cause);
        this.description = description;
    }

    public ServiceLayerReportableException(String message, String description) {
        super(message);
        this.description = description;
    }
}
