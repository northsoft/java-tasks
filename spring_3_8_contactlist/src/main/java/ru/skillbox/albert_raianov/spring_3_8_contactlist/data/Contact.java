package ru.skillbox.albert_raianov.spring_3_8_contactlist.data;

import java.util.Optional;
import jakarta.persistence.Column;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldNameConstants;
import ru.skillbox.albert_raianov.spring_3_8_contactlist.persistence.common.ClassWithLongId;
import ru.skillbox.albert_raianov.spring_3_8_contactlist.services.exceptions.ValidationFailedException;


/* Classes are used instead of records because records cannot be accepted by Spring WebMVC in POST queries.
* @Column annotations are needed by JOOQ, without them there is a mapping mismatch */

@Data
@FieldNameConstants
@NoArgsConstructor
@AllArgsConstructor
public class Contact implements ClassWithLongId {
    @Column(name = "id")
    private Long id;

    @Column(name = "firstName")
    private String firstName;

    @Column(name = "lastName")
    private String lastName;

    @Column(name = "email")
    private String email;

    @Column(name = "phone")
    private String phone;

    public static boolean isValidPhoneNumber(String phoneNumber) {
        phoneNumber = phoneNumber.replaceAll("[ -]", "");
        while (phoneNumber.contains("(")) {
            int openBraceIndex = phoneNumber.indexOf('(');
            int closeBraceIndex = phoneNumber.indexOf(')', openBraceIndex);
            if (closeBraceIndex < 0) {
                return false;
            }
            phoneNumber =
                    phoneNumber.substring(0, openBraceIndex - 1) +
                    phoneNumber.substring(openBraceIndex+1, closeBraceIndex - 1) +
                    phoneNumber.substring(closeBraceIndex + 1);
        }
        return phoneNumber.matches("\\+?[0-9]{5,}");
    }

    private Optional<String> getValidationErrorMessage(boolean idRequiredPresence) {
        if (idRequiredPresence && id == null) {
            return Optional.of("Id should be provided");
        }
        if (!idRequiredPresence && id != null) {
            return Optional.of("Id should not be provided");
        }
        if (firstName == null) {
            return Optional.of("First name should be provided");
        }
        if (firstName.trim().length() < 2) {
            return Optional.of("First name is too short");
        }
        if (lastName == null) {
            return Optional.of("Last name should be provided");
        }
        if (lastName.trim().length() < 2) {
            return Optional.of("Last name is too short");
        }
        if (email == null) {
            return Optional.of("Email should be provided");
        }
        String emailRegexp = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:" +
                "[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\" +
                "[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+" +
                "[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}" +
                "(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:" +
                "(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|" +
                "\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"; // © emailregex.com
        if (!email.matches(emailRegexp)) {
            return Optional.of("Email is incorrect");
        }
        if (phone == null) {
            return Optional.of("Phone should be provided");
        }
        if (!isValidPhoneNumber(phone)) {
            return Optional.of("Phone number is incorrect");
        }

        return Optional.empty();
    }

    public void assertIsValid(boolean idRequiredPresence) {
        Optional<String> validationErrorMessage = getValidationErrorMessage(idRequiredPresence);
        if (validationErrorMessage.isPresent()) {
            throw new ValidationFailedException(validationErrorMessage.get());
        }
    }
}
