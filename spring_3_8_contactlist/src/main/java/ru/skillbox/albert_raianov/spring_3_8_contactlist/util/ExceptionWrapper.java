package ru.skillbox.albert_raianov.spring_3_8_contactlist.util;

import java.lang.reflect.Constructor;
import java.util.Arrays;
import java.util.function.Supplier;

/**
 * This class is&nbsp;supposed to&nbsp;wrap exceptions thrown in&nbsp;some block of&nbsp;code to&nbsp;a&nbsp;specified
 * wrapper exception class. Example:
 * <pre>
 * // Example of instantiation:
 * ExceptionWrapper&lt;MyServiceLayerException&gt; wrapper =
 *     new ExceptionWrapper&lt;&gt;(MyServiceLayerException.class);
 *
 * // e.g. if any call to object dao will throw DaoException, then&hellip;
 * wrapper.wrap(() -> dao.persistData(myData), "Error persisting data");
 * return wrapper.wrap(() -> dao.retrieveData(someQuery), "Error requesting data");
 *
 * // there will be thrown this:
 * // new MyServiceLayerException(
 * //         "Error persisting/requesting data: ...",
 * //         originalException
 * // );
 * </pre>
 * @param <ExceptionType> type to&nbsp;wrap exceptions&nbsp;to. {@link ExceptionType} should&nbsp;be descendant
 * from&nbsp;{@link RuntimeException}, because it&nbsp;is&nbsp;possible to&nbsp;throw&nbsp;them in&nbsp;lambdas.<br />
 * The {@link ExceptionType} <u>must</u> have a&nbsp;constructor with exactly two&nbsp;arguments,
 * first of&nbsp;type {@link String} exactly, second of&nbsp;type {@link Throwable} exactly. This constructor
 * will&nbsp;be called via&nbsp;Reflection&nbsp;API.
 * It&nbsp;is&nbsp;possible for {@link ExceptionType} to&nbsp;have other constructors.
 */
public class ExceptionWrapper<ExceptionType extends RuntimeException> {
    private final Class<ExceptionType> exceptionTypeClass;
    private final Constructor<ExceptionType> storedConstructor;

    /**
     * This constructor creates {@link ExceptionWrapper}. Example:
     * <pre>
     * ExceptionWrapper&lt;MyServiceLayerException&gt; wrapper =
     *     new ExceptionWrapper&lt;&gt;(MyServiceLayerException.class);
     * </pre>
     * @param exceptionTypeClass should be a&nbsp;<code>.class</code> of&nbsp;{@link ExceptionType}.
     */
    public ExceptionWrapper(Class<ExceptionType> exceptionTypeClass) {
        this.exceptionTypeClass = exceptionTypeClass;
        String[] requiredArguments = new String[]{"java.lang.String", "java.lang.Throwable"};

        @SuppressWarnings("unchecked")
        Constructor<ExceptionType> foundConstructor = (Constructor<ExceptionType>)
                Arrays.stream(exceptionTypeClass.getDeclaredConstructors())
                .filter(ctor -> Arrays.equals(
                        Arrays.stream(ctor.getParameterTypes()).map(Class::getCanonicalName).toArray(),
                        requiredArguments
                ))
                .findFirst()
                .orElseThrow(
                        () -> new RuntimeException(String.format(
                                "Unable to initialize ExceptionWrapper for a class %s with ctors %s, " +
                                "required constructor is (java.lang.String, java.lang.Throwable)",
                                exceptionTypeClass.getCanonicalName(),
                                Arrays.toString(exceptionTypeClass.getDeclaredConstructors())
                        ))
                );

        this.storedConstructor = foundConstructor;
    }

    /**
     * This method wraps exceptions thrown during <code>action</code> to&nbsp;{@link ExceptionType}.
     * This method is&nbsp;supposed to&nbsp;be&nbsp;used with routines using <code>return</code> statement
     * with&nbsp;some value.
     *
     * @param action Programmatic routine, which can&nbsp;cause some inner exception, required to&nbsp;wrap.
     * Can&nbsp;be an&nbsp;instance of&nbsp;{@link Supplier}, or&nbsp;method reference, or&nbsp;lambda.
     *
     * @param errorMessage Message to&nbsp;be&nbsp;set in&nbsp;front of&nbsp;thrown exception. If&nbsp;there&nbsp;is
     * an&nbsp;exception to&nbsp;wrap called <code>exceptionToWrap</code>, the&nbsp;message of&nbsp;resulting exception
     * will&nbsp;be&nbsp;composed this&nbsp;way:
     * <pre>
     *     errorMessage + ": " + exceptionToWrap.getMessage()
     * </pre>
     *
     * @return value returned by&nbsp;<code>action</code>
     *
     * @param <ReturnType> Type of&nbsp;value returned by&nbsp;<code>action</code>.
     * @throws ExceptionType Type of&nbsp;exception to&nbsp;wrap inner exceptions&nbsp;to.
     */
    public <ReturnType> ReturnType wrap(Supplier<ReturnType> action, String errorMessage) throws ExceptionType {
        Exception exceptionToWrap;
        try {
            return action.get();
        } catch (Exception e) {
            exceptionToWrap = e;
        }
        throw wrapThrownException(exceptionToWrap, errorMessage);
    }

    /**
     * This method wraps exceptions thrown during <code>action</code> to&nbsp;{@link ExceptionType}.
     * This method is&nbsp;supposed to&nbsp;be&nbsp;used with routines NOT using <code>return</code> statement
     * (at&nbsp;least with&nbsp;some value).
     *
     * @param action Programmatic routine, which can&nbsp;cause some inner exception, required to&nbsp;wrap.
     * Can&nbsp;be an&nbsp;instance of&nbsp;{@link Runnable}, or&nbsp;method reference, or&nbsp;lambda.
     *
     * @param errorMessage Message to&nbsp;be&nbsp;set in&nbsp;front of&nbsp;thrown exception. If&nbsp;there&nbsp;is
     * an&nbsp;exception to&nbsp;wrap called <code>exceptionToWrap</code>, the&nbsp;message of&nbsp;resulting exception
     * will&nbsp;be&nbsp;composed this&nbsp;way:
     * <pre>
     *     errorMessage + ": " + exceptionToWrap.getMessage()
     * </pre>
     */
    public void wrap(Runnable action, String errorMessage) {
        Exception exceptionToWrap;
        try {
            action.run();
            return;
        } catch (Exception e) {
            exceptionToWrap = e;
        }
        throw wrapThrownException(exceptionToWrap, errorMessage);
    }

    private ExceptionType wrapThrownException(Exception exceptionToWrap, String message) throws ExceptionType {
        if (exceptionTypeClass.isInstance(exceptionToWrap)) {
            @SuppressWarnings("unchecked")
            ExceptionType alreadyWrappedException = (ExceptionType) exceptionToWrap;
            return alreadyWrappedException;
        }

        try {
            return storedConstructor.newInstance(
                    message + ": " + exceptionToWrap.getMessage(),
                    exceptionToWrap
            );
        } catch (Exception constructFailedException) {
            if (constructFailedException instanceof RuntimeException) {
                throw (RuntimeException) constructFailedException;
            } else {
                throw new RuntimeException("Unable to call constructor of exception", constructFailedException);
            }
        }
    }
}
