package ru.skillbox.albert_raianov.spring_3_8_contactlist.persistence.exceptions;

/**
 * This exception is&nbsp;thrown in&nbsp;case when an&nbsp;argument of&nbsp;some&nbsp;method is&nbsp;apriori illegal.
 * Usually this exception is&nbsp;thrown when a&nbsp;<code>null</code>&nbsp;value is provided when it&nbsp;is
 * not&nbsp;allowed, or&nbsp;vice&nbsp;versa, when a&nbsp;non&#x2011;null&nbsp;value is&nbsp;provided when
 * it&nbsp;should be&nbsp;<code>null</code>.
 */
public class IllegalArgumentException extends PersistenceException {
    public IllegalArgumentException() {
    }

    public IllegalArgumentException(String message) {
        super(message);
    }

    public IllegalArgumentException(String message, Throwable cause) {
        super(message, cause);
    }

    public IllegalArgumentException(Throwable cause) {
        super(cause);
    }

    public IllegalArgumentException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
