package ru.skillbox.albert_raianov.spring_3_8_contactlist.persistence.common;


import java.util.List;
import java.util.stream.IntStream;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;
import ru.skillbox.albert_raianov.spring_3_8_contactlist.persistence.exceptions.IllegalArgumentException;


/**
 * This class provides common routines for&nbsp;implementations of&nbsp;{@link PersistenceProvider}
 * (hereinafter referred&nbsp;to as&nbsp;<b>Implementations</b>).
 * to&nbsp;verify that&nbsp;arguments of&nbsp;calls are&nbsp;legal. Users of&nbsp;<b>Implementations</b>
 * are&nbsp;not&nbsp;required to&nbsp;call methods of&nbsp;this class. These methods should&nbsp;be&nbsp;called
 * by&nbsp;<b>Implementations</b> internally.
 * @author Albert Rayanov
 * @see PersistenceProvider
 * @see ClassWithLongId
 */
@Component
@Log4j2
public class RepositoryVerifier<Type extends ClassWithLongId> {
    public void assertCanGetById(Long id) {
        if (id == null) {
            log.error("assertCanGetById(null)");
            throw new IllegalArgumentException("Value cannot be queried by null id");
        }
    }

    public void assertCanCreateNew(Type value) {
        if (value == null) {
            log.error("assertCanCreateNew(null)");
            throw new IllegalArgumentException("Null value cannot be inserted");
        }
        if (value.getId() != null) {
            log.error("assertCanCreateNew: value.getId() == null");
            throw new IllegalArgumentException("Id of a value to insert should be null");
        }
    }

    public void assertCanUpdate(Type value) {
        if (value == null) {
            log.error("assertCanUpdate(null)");
            throw new IllegalArgumentException("Null value cannot be updated");
        }
        if (value.getId() == null) {
            log.error("assertCanUpdate: value.getId() == null");
            throw new IllegalArgumentException("Value with null id cannot be updated");
        }
    }

    public void assertCanDelete(Long id) {
        if (id == null) {
            log.error("assertCanDelete(null)");
            throw new IllegalArgumentException("Id to delete cannot be null");
        }
    }

    public void assertCanBatchInsert(List<Type> values) {
        if (values == null) {
            log.error("assertCanDelete(null)");
            throw new IllegalArgumentException("Cannot perform a batch insert for a null list");
        }
        IntStream.range(0, values.size()).forEach(i -> {
            Type value = values.get(i);
            if (value == null) {
                log.error("assertCanBatchInsert: values.get({}) == null", i);
                throw new IllegalArgumentException("Cannot batch insert a null value (at index " + i + ")");
            }
            if (value.getId() != null) {
                log.error("assertCanBatchInsert: values.get({}).getId != null", i);
                throw new IllegalArgumentException("Value to batch insert should have null id (at index " + i + ")");
            }
        });
    }
}
