package ru.skillbox.albert_raianov.spring_3_8_contactlist.persistence.common;

/**
 * This&nbsp;interface is&nbsp;supposed to&nbsp;be&nbsp;implemented by&nbsp;classes persisted
 * by&nbsp;{@link PersistenceProvider}.<br />
 * These classes are&nbsp;supposed to&nbsp;be&nbsp;mutable and&nbsp;have public getter and&nbsp;setter
 * for&nbsp;their&nbsp;<code>id</code> of&nbsp;type {@link Long}, because chosen implementation
 * of&nbsp;{@link PersistenceProvider} should be&nbsp;able to&nbsp;set&nbsp;<code>id</code> for&nbsp;a&nbsp;newly
 * created record in&nbsp;database.
 * @see PersistenceProvider
 */
public interface ClassWithLongId {
    Long getId();
    void setId(Long id);
}
