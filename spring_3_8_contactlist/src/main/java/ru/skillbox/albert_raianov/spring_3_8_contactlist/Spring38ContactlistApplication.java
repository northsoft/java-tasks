package ru.skillbox.albert_raianov.spring_3_8_contactlist;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Spring38ContactlistApplication {

	public static void main(String[] args) {
		SpringApplication.run(Spring38ContactlistApplication.class, args);
	}

}
