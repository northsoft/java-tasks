package ru.skillbox.albert_raianov.spring_3_8_contactlist.persistence.exceptions;

/**
 * This&nbsp;is the&nbsp;parent class for&nbsp;exceptions which&nbsp;are&nbsp;intended to&nbsp;be&nbsp;used
 * in&nbsp;classes implementing <code>PersistenceProvider</code> interface.
 */
public class PersistenceException extends RuntimeException {
    public PersistenceException() {
    }

    public PersistenceException(String message) {
        super(message);
    }

    public PersistenceException(String message, Throwable cause) {
        super(message, cause);
    }

    public PersistenceException(Throwable cause) {
        super(cause);
    }

    public PersistenceException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
