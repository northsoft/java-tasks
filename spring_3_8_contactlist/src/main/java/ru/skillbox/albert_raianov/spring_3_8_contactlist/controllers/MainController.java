package ru.skillbox.albert_raianov.spring_3_8_contactlist.controllers;

import java.util.function.Supplier;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import ru.skillbox.albert_raianov.spring_3_8_contactlist.data.Contact;
import ru.skillbox.albert_raianov.spring_3_8_contactlist.services.ContactsService;
import ru.skillbox.albert_raianov.spring_3_8_contactlist.services.exceptions.ServiceLayerReportableException;


@Controller
@RequiredArgsConstructor
@Log4j2
public class MainController {
    private final ContactsService contactsService;

    private String doWithErrorHandling(Model model, String whatsHappening, Supplier<String> action) {
        try {
            return action.get();
        } catch (ServiceLayerReportableException e) {
            log.warn("User error during " + whatsHappening, e);
            model.addAttribute("error", e);
            return "pages/user_error";
        } catch (Exception e) {
            log.error("Error during " + whatsHappening, e);
            return "pages/internal_error";
        }
    }

    @GetMapping("/")
    public String index(Model model) {
        return doWithErrorHandling(model, "rendering index page", () -> {
            model.addAttribute("contactList", contactsService.getAllContacts());
            return "pages/index";
        });
    }

    @GetMapping("/contacts/create")
    public String createForm(Model model) {
        model.addAttribute("contact", new Contact());
        return "pages/form";
    }

    @PostMapping("/contacts/create")
    public String createSave(@ModelAttribute Contact contact, Model model) {
        return doWithErrorHandling(model, "processing a request to create new user", () -> {
            contactsService.createNewContact(contact);
            return "redirect:/";
        });
    }

    @GetMapping("/contacts/edit/{id}")
    public String editForm(@PathVariable Long id, Model model) {
        return doWithErrorHandling(
                model,
                "processing a request to show an edit form for user \" + id",
                () -> {
                    model.addAttribute("contact", contactsService.getContactById(id));
                    return "pages/form";
                }
        );
    }

    @PostMapping("/contacts/edit")
    public String editSave(@ModelAttribute Contact contact, Model model) {
        return doWithErrorHandling(model, "processing an update request", () -> {
            contactsService.updateContact(contact);
            return "redirect:/";
        });
    }

    @GetMapping("/contacts/delete/{id}")
    public String deleteById(@PathVariable Long id, Model model) {
        return doWithErrorHandling(model, "deletion of a contact with id " + id, () -> {
            contactsService.deleteContact(id);
            return "redirect:/";
        });
    }
}
