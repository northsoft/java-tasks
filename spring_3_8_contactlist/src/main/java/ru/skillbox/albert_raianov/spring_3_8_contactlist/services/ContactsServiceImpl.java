package ru.skillbox.albert_raianov.spring_3_8_contactlist.services;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import ru.skillbox.albert_raianov.spring_3_8_contactlist.data.Contact;
import ru.skillbox.albert_raianov.spring_3_8_contactlist.persistence.common.PersistenceProvider;
import ru.skillbox.albert_raianov.spring_3_8_contactlist.persistence.exceptions.RecordNotFoundException;
import ru.skillbox.albert_raianov.spring_3_8_contactlist.services.exceptions.ServiceLayerException;
import ru.skillbox.albert_raianov.spring_3_8_contactlist.services.exceptions.ServiceLayerReportableException;
import ru.skillbox.albert_raianov.spring_3_8_contactlist.services.exceptions.ValidationFailedException;
import ru.skillbox.albert_raianov.spring_3_8_contactlist.util.ExceptionWrapper;

import java.util.List;

@Service
@RequiredArgsConstructor
@Log4j2
public class ContactsServiceImpl implements ContactsService {
    private final PersistenceProvider<Contact> contactsPersistenceProvider;
    private final ExceptionWrapper<ServiceLayerException> serviceExceptionWrapper =
            new ExceptionWrapper<>(ServiceLayerException.class);

    public List<Contact> getAllContacts() {
        return serviceExceptionWrapper.wrap(
                contactsPersistenceProvider::getAll,
                "unable to get all contacts"
        );
    }

    public Contact getContactById(Long id) {
        return serviceExceptionWrapper.wrap(() -> {
            try {
                return contactsPersistenceProvider.getById(id);
            } catch (RecordNotFoundException e) {
                throw new ServiceLayerReportableException(
                    "Contact with id " + id + " does not exist or is already deleted",
                    "Please return to index page, refresh a list of contacts and repeat your operation",
                    e
                );
            }
        }, "unable to get contact by id");
    }

    public void createNewContact(Contact contact) {
        serviceExceptionWrapper.wrap(() -> {
            contact.assertIsValid(false);
            contactsPersistenceProvider.createNewAndSetId(contact);
        }, "unable to create new contact");
    }

    public void updateContact(Contact contact) {
        serviceExceptionWrapper.wrap(() -> {
            try {
                contact.assertIsValid(true);
                contactsPersistenceProvider.update(contact);
            } catch (RecordNotFoundException e) {
                throw new ServiceLayerReportableException(
                    "Contact requested for update does not exist or is already deleted",
                    "Please return to index page, refresh contact list and repeat your operation",
                    e
                );
            }
        },
        "unable to update contact");
    }

    public void deleteContact(Long id) {
        serviceExceptionWrapper.wrap(() -> {
            try {
                contactsPersistenceProvider.delete(id);
            } catch (RecordNotFoundException e) {
                throw new ServiceLayerReportableException(
                    "Contact requested for removal does not exist or is already deleted",
                    "Please return to index page, refresh contact list and repeat your operation",
                    e
                );
            }
        },
        "unable to delete contact");
    }

    public void batchInsert(List<Contact> contacts) {
        serviceExceptionWrapper.wrap(() -> {
            int i = 0;
            try {
                for (i = 0; i < contacts.size(); ++i) {
                    contacts.get(i).assertIsValid(false);
                }
            } catch (ValidationFailedException e) {
                throw new ValidationFailedException("At item + " + i + ": " + e.getMessage());
            }

            contactsPersistenceProvider.batchInsertAndSetIds(contacts);
        }, "unable to perform batch insert");
    }
}
