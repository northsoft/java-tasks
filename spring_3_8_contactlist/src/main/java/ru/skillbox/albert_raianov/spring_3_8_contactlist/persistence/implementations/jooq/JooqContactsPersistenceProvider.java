package ru.skillbox.albert_raianov.spring_3_8_contactlist.persistence.implementations.jooq;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.jooq.DSLContext;
import org.jooq.impl.DSL;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Repository;
import ru.skillbox.albert_raianov.spring_3_8_contactlist.data.Contact;
import ru.skillbox.albert_raianov.spring_3_8_contactlist.persistence.common.PersistenceProvider;
import ru.skillbox.albert_raianov.spring_3_8_contactlist.persistence.common.RepositoryVerifier;
import ru.skillbox.albert_raianov.spring_3_8_contactlist.persistence.exceptions.PersistenceException;
import ru.skillbox.albert_raianov.spring_3_8_contactlist.persistence.exceptions.RecordNotFoundException;
import ru.skillbox.albert_raianov.spring_3_8_contactlist.util.ExceptionWrapper;
import ru.skillbox.albert_raianov.spring_3_8_webmvc.persistence.implementations.jooq.db.Tables;
import ru.skillbox.albert_raianov.spring_3_8_webmvc.persistence.implementations.jooq.db.tables.records.ContactsRecord;

import java.util.List;
import java.util.Optional;
import java.util.stream.IntStream;

@Repository
@RequiredArgsConstructor
@ConditionalOnProperty(name = "contactlist.persistence.provider", havingValue = "jooq")
@Log4j2
public class JooqContactsPersistenceProvider implements PersistenceProvider<Contact> {
    private final DSLContext dslContext;
    private final RepositoryVerifier<Contact> repositoryVerifier;
    private final ExceptionWrapper<PersistenceException> persistenceExceptionWrapper =
              new ExceptionWrapper<>(PersistenceException.class);

    @Override
    public List<Contact> getAll() throws PersistenceException {
        return persistenceExceptionWrapper.wrap(() -> {
            log.debug("JooqContactsPersistenceProvider.getAll()");
            return dslContext.selectFrom(Tables.CONTACTS).fetchInto(Contact.class);
        }, "Unexpected exception during getAll()");
    }

    @Override
    public Contact getById(Long id) throws PersistenceException {
        return persistenceExceptionWrapper.wrap(() -> {
            repositoryVerifier.assertCanGetById(id);
            log.debug("JooqContactsPersistenceProvider.getById({})", id);
            Optional<Contact> optContact = dslContext
                    .selectFrom(Tables.CONTACTS)
                    .where(Tables.CONTACTS.ID.eq(id))
                    .fetchOptional()
                    .map(contactsRecord -> contactsRecord.into(Contact.class));
            return optContact.orElseThrow(() -> new RecordNotFoundException("Contact with id " + id + " was not found"));
        }, "Unexpected exception during getById(" + id + ")");
    }

    @Override
    public void createNewAndSetId(Contact value) throws PersistenceException {
        persistenceExceptionWrapper.wrap(() -> {
            repositoryVerifier.assertCanCreateNew(value);
            log.debug("JooqContactsPersistenceProvider.createNewAndSetId({})", value);
            ContactsRecord contactsRecord = dslContext.newRecord(Tables.CONTACTS, value);
            contactsRecord.store();
            value.setId(contactsRecord.getId());
        }, "Unexpected exception during createNewAndSetId(" + value + ")");
    }

    @Override
    public void update(Contact value) throws PersistenceException {
        persistenceExceptionWrapper.wrap(() -> {
            repositoryVerifier.assertCanUpdate(value);
            log.debug("JooqContactsPersistenceProvider.update({})", value);

            Optional<ContactsRecord> maybeUpdatedRecord = dslContext
                    .update(Tables.CONTACTS)
                    .set(dslContext.newRecord(Tables.CONTACTS, value))
                    .where(Tables.CONTACTS.ID.eq(value.getId()))
                    .returning()
                    .fetchOptional();

            if (maybeUpdatedRecord.isEmpty()) {
                throw new RecordNotFoundException("Contact with id " + value.getId() + " was not found ");
            }

        }, "Unexpected exception during update(" + value + ")");
    }

    @Override
    public void delete(Long id) throws PersistenceException {
        persistenceExceptionWrapper.wrap(() -> {
            repositoryVerifier.assertCanDelete(id);
            log.debug("JooqContactsPersistenceProvider.delete({})", id);

            int nAffected = dslContext
                    .deleteFrom(Tables.CONTACTS)
                    .where(Tables.CONTACTS.ID.eq(id))
                    .execute();
            if (nAffected == 0) {
                throw new RecordNotFoundException("Unable to delete contact with id " + id);
            }
        }, "Unexpected exception during delete(" + id + ")");
    }

    @Override
    public void batchInsertAndSetIds(List<Contact> values) throws PersistenceException {
        persistenceExceptionWrapper.wrap(() -> {
            log.debug("JooqContactsPersistenceProvider.batchInsertAndSetIds({})", values);
            List<Long> ids = dslContext
                    .insertInto(
                            Tables.CONTACTS,
                            Tables.CONTACTS.FIRSTNAME,
                            Tables.CONTACTS.LASTNAME,
                            Tables.CONTACTS.EMAIL,
                            Tables.CONTACTS.PHONE
                    )
                    .valuesOfRows(
                            values.stream().map(
                                    t -> DSL.row(t.getFirstName(), t.getLastName(), t.getEmail(), t.getPhone())
                            )
                            .toList()
                    )
                    .returningResult(Tables.CONTACTS.ID)
                    .fetchInto(Long.class);

            if (ids.contains(null)) {
                throw new PersistenceException(String.format(
                        "Null id is not supposed to be returned from batch insert (response is %s)", ids
                ));
            }
            if (ids.size() != values.size()) {
                throw new PersistenceException(String.format(
                        "Mismatch between number of items to be batch-inserted (%d) and ids returned from DBMS (%d)",
                        values.size(), ids.size()
                ));
            }

            IntStream.range(0, ids.size()).forEach(i -> values.get(i).setId(ids.get(i)));
        }, "Unexpected exception during batchInsert(" + values + ")");
    }
}
