package ru.skillbox.albert_raianov.cliutils.console_proxy;

import java.io.BufferedReader;
import java.io.PrintStream;
import org.springframework.beans.factory.DisposableBean;

public abstract class AbstractConsoleInteractionProxy implements AutoCloseable, DisposableBean {
    public abstract BufferedReader getBufferedReader();

    public abstract PrintStream getPrintStream();

    @Override
    public void close() throws Exception {
        if (getBufferedReader().ready()) {
            getBufferedReader().close();
        }
    }

    @Override
    public void destroy() throws Exception {
        if (getBufferedReader().ready()) {
            getBufferedReader().close();
        }
    }
}
