package ru.skillbox.albert_raianov.cliutils.console_menu;

public interface MenuOption {
    String getCaption();
    @SuppressWarnings("RedundantThrows") // Exceptions are handled by menu itself
    void doAction() throws Exception;
}
