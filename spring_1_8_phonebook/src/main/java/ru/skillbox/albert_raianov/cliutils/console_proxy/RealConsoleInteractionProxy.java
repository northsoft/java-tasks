package ru.skillbox.albert_raianov.cliutils.console_proxy;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintStream;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RealConsoleInteractionProxy extends AbstractConsoleInteractionProxy {

    private final Logger logger = LogManager.getLogger(RealConsoleInteractionProxy.class);

    private final BufferedReader bufferedReader;

    private final PrintStream printStream;

    public BufferedReader getBufferedReader() {
        return bufferedReader;
    }

    public PrintStream getPrintStream() {
        return printStream;
    }

    @Autowired
    public RealConsoleInteractionProxy() {
        logger.debug("Console proxy started with default streams for interaction with console");
        this.bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        this.printStream = System.out;
    }

    public RealConsoleInteractionProxy(BufferedReader bufferedReader, PrintStream printStream) {
        logger.debug("Console proxy started with custom streams for interaction with console");
        this.bufferedReader = bufferedReader;
        this.printStream = printStream;
    }

}
