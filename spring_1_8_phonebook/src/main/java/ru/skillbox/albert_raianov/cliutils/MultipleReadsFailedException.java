package ru.skillbox.albert_raianov.cliutils;

public class MultipleReadsFailedException extends Exception {

    public MultipleReadsFailedException(Throwable cause) {
        super("Unable to read value multiple times, last error is " + cause.getMessage(), cause);
    }
}
