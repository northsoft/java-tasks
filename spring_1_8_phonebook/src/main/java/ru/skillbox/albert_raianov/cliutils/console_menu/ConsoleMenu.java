package ru.skillbox.albert_raianov.cliutils.console_menu;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.function.Function;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.skillbox.albert_raianov.cliutils.MultipleAttemptsLineReader;
import ru.skillbox.albert_raianov.cliutils.MultipleReadsFailedException;
import ru.skillbox.albert_raianov.cliutils.console_proxy.AbstractConsoleInteractionProxy;

@Component
@Scope(value = "prototype")
public class ConsoleMenu {
    private final Logger logger = LogManager.getLogger(ConsoleMenu.class);
    private final AbstractConsoleInteractionProxy consoleProxy;
    private final MultipleAttemptsLineReader lineReader;

    private final int maxAttempts;



    public ConsoleMenu(
            AbstractConsoleInteractionProxy consoleProxy,
            MultipleAttemptsLineReader lineReader,
            @Value("${cliutils.menu.max_attempts}") int maxAttempts
    ) {
        this.consoleProxy = consoleProxy;
        this.lineReader = lineReader;
        this.maxAttempts = maxAttempts;
    }

    private List<MenuOption> menuOptions = new ArrayList<>();

    public List<MenuOption> getMenuOptions() {
        return menuOptions;
    }

    public void resetMenuOptions() {
        this.menuOptions = new ArrayList<>();
    }

    private List<ShutdownHandler> shutdownHandlers = new ArrayList<>();

    public List<ShutdownHandler> getShutdownHandlers() {
        return shutdownHandlers;
    }

    public void resetShutdownHandlers() {
        this.shutdownHandlers = new ArrayList<>();
    }

    public void launchMenu() {
        PrintStream printStream = consoleProxy.getPrintStream();
        String quotedLogfile = "\"" + System.getProperty("java.io.tmpdir") + "/.skillbox/Phonebook.log\"";

        if (menuOptions == null || menuOptions.isEmpty()) {
            logger.warn("menuOptions is null, terminating menu");
            printStream.println("Unable to launch menu without items");
            return;
        }
        int exitOptionNumber = menuOptions.size() + 1;
        int currentOption;
        do {
            outputMenu();
            try {
                currentOption = readNumber();
                if (currentOption < exitOptionNumber) {
                    menuOptions.get(currentOption - 1).doAction();
                }
                if (currentOption == exitOptionNumber) {
                    printStream.println("Good bye!");
                }
            } catch (MultipleReadsFailedException exception) {
                logger.error("Multiple reads failed", exception);
                printStream.println("Application was unable to process your input multiple times, menu is interrupting");
                return;
            } catch (Exception exception) {
                logger.error("Unable to read number and perform an action", exception);
                printStream.println(
                        "Unhandled exception occurred, please report it to application developer " +
                        "and provide the file " + quotedLogfile);
                return;
            }
        } while (currentOption != exitOptionNumber);
        shutdownHandlers.forEach(shutdownHandler -> {
            try {
                shutdownHandler.doAction();
            } catch (Exception e) {
                logger.error("Failed to call shutdown handler " + shutdownHandler.getClass().getCanonicalName(), e);
                printStream.println(
                        "Attention: some of shutdown handlers have failed to perform their actions. Some data " +
                        "may be lost. Please contact application developer and provide the file " + quotedLogfile
                );
            }
        });
    }

    private void outputMenu() {
        PrintStream printStream = consoleProxy.getPrintStream();
        int optIndex = 1;
        printStream.println("Please choose your action:");
        for (MenuOption option : menuOptions) {
            printStream.printf("%d: %s\n", optIndex++, option.getCaption());
        }
        printStream.printf("%d: Exit\n", optIndex);
    }

    private int readNumber() throws MultipleReadsFailedException {
        int maxChoice = menuOptions.size() + 1;

        Function<String, Integer> convertMenuChoice = s -> {
            try (Scanner numScanner = new Scanner(s)) {
                int result = numScanner.nextInt();
                if (result <= 0 || result > maxChoice) {
                    throw new RuntimeException("No such choice, number should be from 1 to " + maxChoice);
                }
                return result;
            }
        };

        try {
            return lineReader.readValue(maxAttempts, convertMenuChoice);
        } catch (Exception e) {
            logger.error("Unable to read a menu choice number", e);
            throw new MultipleReadsFailedException(e);
        }
    }


}
