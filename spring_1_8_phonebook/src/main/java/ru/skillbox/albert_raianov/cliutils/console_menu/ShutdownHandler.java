package ru.skillbox.albert_raianov.cliutils.console_menu;

public interface ShutdownHandler {
    void doAction() throws Exception;
}
