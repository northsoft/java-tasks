package ru.skillbox.albert_raianov.cliutils;

import java.io.BufferedReader;
import java.io.PrintStream;
import java.util.function.Function;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;
import ru.skillbox.albert_raianov.cliutils.console_proxy.AbstractConsoleInteractionProxy;


@Component
public class MultipleAttemptsLineReader {
    private final Logger logger = LogManager.getLogger(MultipleAttemptsLineReader.class);
    private final AbstractConsoleInteractionProxy consoleProxy;

    public MultipleAttemptsLineReader(AbstractConsoleInteractionProxy consoleProxy) {
        this.consoleProxy = consoleProxy;
    }

    public <ResultType> ResultType readValue(int nAttempts, Function<String, ResultType> converter)
    throws MultipleReadsFailedException
    {
        PrintStream out = consoleProxy.getPrintStream();
        BufferedReader reader = consoleProxy.getBufferedReader();
        Exception lastException;
        do {
            try {
                String line = reader.readLine();
                return converter.apply(line);
            } catch (Exception e) {
                logger.info("Exception during attempt of reading and returning a value", e);
                --nAttempts;
                lastException = e;
                String exceptionMessage = e.getMessage() == null ? "" : " (" + e.getMessage() + ")";
                if (nAttempts > 0) {
                    out.print("Error reading message" + exceptionMessage + ", please try again: ");
                } else {
                    out.println("Error reading message" + exceptionMessage);
                }
            }
        } while (nAttempts > 0);
        logger.warn("Unable to read value in requested number of attempts");
        throw new MultipleReadsFailedException(lastException);
    }
}
