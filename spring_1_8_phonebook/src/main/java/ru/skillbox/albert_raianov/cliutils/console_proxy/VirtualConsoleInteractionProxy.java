package ru.skillbox.albert_raianov.cliutils.console_proxy;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.io.StringReader;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class VirtualConsoleInteractionProxy extends AbstractConsoleInteractionProxy {

    private final BufferedReader bufferedReader;

    private final PrintStream printStream;

    private final ByteArrayOutputStream virtualOutputFinalConsumer;

    public BufferedReader getBufferedReader() {
        return bufferedReader;
    }

    public PrintStream getPrintStream() {
        return printStream;
    }

    public VirtualConsoleInteractionProxy(String input) {
        Logger logger = LogManager.getLogger(VirtualConsoleInteractionProxy.class);
        logger.debug("Console proxy started with custom streams for interaction with console");

        this.virtualOutputFinalConsumer = new ByteArrayOutputStream();
        this.bufferedReader = new BufferedReader(new StringReader(input));
        this.printStream = new PrintStream(virtualOutputFinalConsumer);
    }

    public String getVirtualOutput() {
        return virtualOutputFinalConsumer.toString();
    }
}
