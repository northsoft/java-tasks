package ru.skillbox.albert_raianov;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.env.Environment;
import ru.skillbox.albert_raianov.config.AppConfig;
import ru.skillbox.albert_raianov.phonebook.cli.PhoneBookAppCli;

public class Main {

    public static void main(String[] args) {
        Logger logger = LogManager.getLogger(Main.class);
        ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
        String profiles = String.join(", ", context.getBean(Environment.class).getActiveProfiles());
        logger.debug("Application started with profile " + profiles);
        context.getBean(PhoneBookAppCli.class).run();
    }
}
