package ru.skillbox.albert_raianov.phonebook.app;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;
import ru.skillbox.albert_raianov.phonebook.persistence.InitProvider;
import ru.skillbox.albert_raianov.phonebook.persistence.PersistProvider;


@Component
public class PhoneBookApp {
    private final PersistProvider persistProvider;

    private final List<StoredUser> storedUsers = new ArrayList<>();

    private final Map<String, StoredUser> emailsToStoredUsers = new TreeMap<>();

    public PhoneBookApp(InitProvider initProvider, PersistProvider persistProvider)
    throws PhoneBookAppException
    {
        this.persistProvider = persistProvider;

        try {
            storedUsers.addAll(initProvider.getStoredUsers());
            emailsToStoredUsers.putAll(
                    storedUsers.stream().collect(Collectors.toMap(StoredUser::email, Function.identity()))
            );
        } catch (Exception e) {
            Logger logger = LogManager.getLogger(PhoneBookApp.class);
            logger.error("Unable to initialize application data", e);
            throw new PhoneBookAppException("Unable to initialize application data: " + e.getMessage(), e);
        }
    }

    public AddResult addRecord(StoredUser newUser) {
        if (emailsToStoredUsers.containsKey(newUser.email())) {
            StoredUser userToUpdate = emailsToStoredUsers.get(newUser.email());
            int idx = storedUsers.indexOf(userToUpdate);
            if (idx != -1) {
                storedUsers.set(idx, newUser);
                return AddResult.UPDATED_EXISTENT;
            }
        }
        storedUsers.add(newUser);
        emailsToStoredUsers.put(newUser.email(), newUser);
        return AddResult.CREATED_NEW;
    }

    public RemoveResult removeRecord(String email) {
        StoredUser userToDelete = emailsToStoredUsers.get(email);
        if (userToDelete == null) {
            return RemoveResult.NOT_FOUND;
        }
        boolean wasRemoved = storedUsers.remove(userToDelete);
        emailsToStoredUsers.remove(email);
        return wasRemoved ? RemoveResult.REMOVED : RemoveResult.NOT_FOUND;
    }

    public void persistAll() throws PhoneBookAppException {
        try {
            persistProvider.persistUsers(storedUsers);
        } catch (Exception e) {
            throw new PhoneBookAppException("Unable to save data: " + e.getMessage(), e);
        }
    }

    public enum AddResult {CREATED_NEW, UPDATED_EXISTENT}
    public enum RemoveResult {REMOVED, NOT_FOUND}

    public List<StoredUser> getAllUsers() {
        return Collections.unmodifiableList(storedUsers);
    }
}
