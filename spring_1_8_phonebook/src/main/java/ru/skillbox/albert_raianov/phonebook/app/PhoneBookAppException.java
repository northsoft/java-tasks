package ru.skillbox.albert_raianov.phonebook.app;

public class PhoneBookAppException extends Exception {
    public PhoneBookAppException(String message, Throwable cause) {
        super(message, cause);
    }
}
