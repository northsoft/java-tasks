package ru.skillbox.albert_raianov.phonebook.persistence;

public class PersistenceException extends Exception {
    public PersistenceException(String message, Throwable cause) {
        super(message, cause);
    }
}
