package ru.skillbox.albert_raianov.phonebook.persistence;

import java.util.List;
import ru.skillbox.albert_raianov.phonebook.app.StoredUser;


public interface InitProvider {
    List<StoredUser> getStoredUsers() throws PersistenceException;
}
