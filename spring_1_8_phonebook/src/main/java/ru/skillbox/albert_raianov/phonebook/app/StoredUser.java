package ru.skillbox.albert_raianov.phonebook.app;

import java.util.InputMismatchException;
import java.util.regex.Pattern;

public record StoredUser(String name, String phone, String email) {
    public static StoredUser fromCsvString(String csvString) {
        String[] fields = csvString.split(";");
        if (fields.length != 3) {
            throw new InputMismatchException(String.format(
                    "Should be 3 fields in a line «%s», got %d",
                    csvString,
                    fields.length
            ));
        }
        String name = fields[0];
        String phone = fields[1];
        String email = fields[2];

        StoredUser result = new StoredUser(name, phone, email);
        result.assertIsValid();
        return result;
    }
    public static StoredUser fromUserInputString(String userInputString) {
        String[] fields = userInputString.split("; *");
        if (fields.length != 3) {
            throw new InputMismatchException(
                    "Unable to parse input string, it should contain three fields divided by semicolon"
            );
        }
        String name = fields[0];
        String phone = fields[1];
        String email = fields[2];

        StoredUser result = new StoredUser(name, phone, email);
        result.assertIsValid();
        return result;
    }

    public void assertIsValid() {
        String[] fields = new String[]{name, phone, email};
        String[] fieldCaptions = new String[]{"Name", "Phone", "Email"};
        for (int i = 0; i <= 2; ++i) {
            if (fields[i].isBlank()) {
                throw new RuntimeException(fieldCaptions[i] + " should not be blank");
            }
        }
        // © https://emailregex.com/
        String emailRegex =
                "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c" +
                "\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:" +
                "[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|" +
                "[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-" +
                "\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";

        boolean isEmailGood = Pattern.compile(emailRegex).matcher(email).matches();
        if (!isEmailGood) {
            throw new RuntimeException("Input string is not a valid email address");
        }
    }

    public String toString() {
        return name + " | " + phone + " | " + email;
    }

    public String toString(int minNameWidth, int minPhoneWidth, int minEmailWidth) {
        return String.format(
                "%" + minNameWidth + "s | %" + minPhoneWidth + "s | %" + minEmailWidth + "s",
                name,
                phone,
                email
        );
    }

    public String toCsvString() {
        return String.format("%s;%s;%s", name, phone, email);
    }
}
