package ru.skillbox.albert_raianov.phonebook.persistence;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import ru.skillbox.albert_raianov.phonebook.app.StoredUser;

public class FileInitProvider implements InitProvider {
    private final Logger logger = LogManager.getLogger(FileInitProvider.class);

    private final String filename;

    public FileInitProvider(@Value("${phonebook.persistence.filename}") String filename) {
        this.filename = filename;
    }

    @Override
    public List<StoredUser> getStoredUsers() throws PersistenceException {
        try {
            List<StoredUser> result = new ArrayList<>();
            List<String> inputFile = Files.readAllLines(Paths.get(filename));
            for (String inputLine : inputFile) {
                if (!inputLine.isBlank()) {
                    try {
                        result.add(StoredUser.fromCsvString(inputLine));
                    } catch (Exception e) {
                        logger.warn("Unable to convert string \"" + inputLine + "\" to record", e);
                    }
                }
            }
            return result;
        } catch (Exception e) {
            String message = "Unable to init data from file \"" + filename + "\"";
            logger.error(message, e);
            throw new PersistenceException(message, e);
        }
    }
}
