package ru.skillbox.albert_raianov.phonebook.persistence;

import java.util.List;
import ru.skillbox.albert_raianov.phonebook.app.StoredUser;


public interface PersistProvider {
    void persistUsers(List<StoredUser> users) throws PersistenceException;
}
