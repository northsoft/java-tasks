package ru.skillbox.albert_raianov.phonebook.persistence;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.skillbox.albert_raianov.phonebook.app.StoredUser;

public class BuiltinContactsInitProvider implements InitProvider {
    private final Logger logger = LogManager.getLogger(BuiltinContactsInitProvider.class);
    private final String filename;

    public BuiltinContactsInitProvider(String filename) {
        this.filename = filename;
    }

    @Override
    public List<StoredUser> getStoredUsers() throws PersistenceException {
        try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream(filename)) {
            if (inputStream == null) {
                throw new RuntimeException("Unable to load builtin contacts from \"" + filename + "\"");
            }
            List<String> lines = new BufferedReader(new InputStreamReader(inputStream)).lines().toList();
            List<StoredUser> result = new ArrayList<>();
            for (String line : lines) {
                try {
                    result.add(StoredUser.fromCsvString(line));
                } catch (Exception e) {
                    logger.warn("Unable to read default contact from line \"" + line + "\"");
                }
            }
            return result;
        } catch (Exception e) {
            throw new PersistenceException("Unable to load default contacts", e);
        }
    }
}
