package ru.skillbox.albert_raianov.phonebook.persistence;

import java.io.PrintStream;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ru.skillbox.albert_raianov.phonebook.app.StoredUser;

@Component
public class FilePersistProvider implements PersistProvider {
    private final Logger logger = LogManager.getLogger(FilePersistProvider.class);

    private final String filename;

    public FilePersistProvider(@Value("${phonebook.persistence.filename}") String filename) {
        this.filename = filename;
    }

    @Override
    public void persistUsers(List<StoredUser> users) throws PersistenceException {
        try (PrintStream fileStream = new PrintStream(filename)) {
            users.stream().map(StoredUser::toCsvString).forEach(fileStream::println);
        } catch (Exception e) {
            String message = "Unable to save data to file \"" + filename + "\"";
            logger.error(message, e);
            throw new PersistenceException(message, e);
        }
    }
}
