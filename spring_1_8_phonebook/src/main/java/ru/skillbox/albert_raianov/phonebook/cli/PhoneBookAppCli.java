package ru.skillbox.albert_raianov.phonebook.cli;

import java.io.PrintStream;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.regex.Pattern;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ru.skillbox.albert_raianov.cliutils.console_menu.ConsoleMenu;
import ru.skillbox.albert_raianov.cliutils.console_menu.MenuOption;
import ru.skillbox.albert_raianov.cliutils.MultipleAttemptsLineReader;
import ru.skillbox.albert_raianov.cliutils.MultipleReadsFailedException;
import ru.skillbox.albert_raianov.cliutils.console_menu.ShutdownHandler;
import ru.skillbox.albert_raianov.cliutils.console_proxy.AbstractConsoleInteractionProxy;
import ru.skillbox.albert_raianov.phonebook.app.PhoneBookApp;
import ru.skillbox.albert_raianov.phonebook.app.StoredUser;

@Component
public class PhoneBookAppCli {
    private final Logger logger = LogManager.getLogger(PhoneBookAppCli.class);

    // these two fields use lazy initialization
    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    private Optional<List<MenuOption>> optMenuOptions = Optional.empty();

    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    private Optional<ShutdownHandler> optShutdownHandler = Optional.empty();

    private final int maxAttempts;

    private final ConsoleMenu consoleMenu;
    private final PhoneBookApp phoneBookApp;

    private final MultipleAttemptsLineReader lineReader;

    private final PrintStream out;

    public final Function<String, String> emailConverter = s -> {
        // © https://emailregex.com/
        String emailRegex =
                "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c" +
                "\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:" +
                "[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4]" +
                "[0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:" +
                "[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";

        boolean isEmailGood = Pattern.compile(emailRegex).matcher(s).matches();
        if (!isEmailGood) {
            throw new RuntimeException("Input string is not a valid email address");
        }
        return s;
    };

    @Autowired
    public PhoneBookAppCli(
            ConsoleMenu consoleMenu,
            PhoneBookApp phoneBookApp,
            MultipleAttemptsLineReader lineReader,
            AbstractConsoleInteractionProxy consoleProxy,
            @Value("${phonebook.cli.max_input_attempts}") int maxAttempts
    ) {
        this.consoleMenu = consoleMenu;
        this.phoneBookApp = phoneBookApp;
        this.lineReader = lineReader;
        this.out = consoleProxy.getPrintStream();
        this.maxAttempts = maxAttempts;
    }

    public void showAllRecordsAction() {
        List<StoredUser> users = phoneBookApp.getAllUsers();
        if (users.isEmpty()) {
            out.println("No records stored");
        } else {
            int nameFieldWidth  = users.stream().map(StoredUser::name) .map(String::length).max(Integer::compareTo).get();
            int phoneFieldWidth = users.stream().map(StoredUser::phone).map(String::length).max(Integer::compareTo).get();
            int emailFieldWidth = users.stream().map(StoredUser::email).map(String::length).max(Integer::compareTo).get();

            users.forEach(user -> out.println(user.toString(nameFieldWidth, phoneFieldWidth, emailFieldWidth)));
        }
    }

    public class ShowAllRecordsOption implements MenuOption {
        @Override
        public String getCaption() {
            return "Show all records";
        }
        @Override
        public void doAction() {
            PhoneBookAppCli.this.showAllRecordsAction();
        }
    }

    public void addNewRecordAction() {
        try {
            out.print("Please type data in format “Name; Phone; Email”: ");
            StoredUser newUser = lineReader.readValue(maxAttempts, StoredUser::fromUserInputString);
            PhoneBookApp.AddResult addResult = phoneBookApp.addRecord(newUser);
            switch (addResult) {
                case CREATED_NEW        -> out.println("New record created");
                case UPDATED_EXISTENT   -> out.println("Record updated");
            }
        } catch (MultipleReadsFailedException e) {
            logger.warn("Multiple failures to read input value", e);
            out.println("Unable to read record data: " + e.getMessage());
        }
    }

    public class AddNewRecordOption implements MenuOption {

        @Override
        public String getCaption() {
            return "Add new record";
        }
        @Override
        public void doAction() {
            PhoneBookAppCli.this.addNewRecordAction();
        }

    }

    public void removeRecordByEmailAction() {
        try {
            out.print("Please type email to remove: ");
            String email = lineReader.readValue(5, emailConverter);
            PhoneBookApp.RemoveResult removeResult = phoneBookApp.removeRecord(email);
            switch (removeResult) {
                case REMOVED -> out.println("Record removed");
                case NOT_FOUND -> out.println("Record with this email was not found");
            }
        } catch (MultipleReadsFailedException e) {
            out.println("Unable to process email input");
        }
    }

    public class RemoveRecordByEmailOption implements MenuOption {
        @Override
        public String getCaption() {
            return "Remove record by email";
        }
        @Override
        public void doAction() {
            PhoneBookAppCli.this.removeRecordByEmailAction();
        }

    }
    public void persistAction() throws Exception {
        phoneBookApp.persistAll();
    }

    public class PersistHandler implements ShutdownHandler {
        @Override
        public void doAction() throws Exception {
            PhoneBookAppCli.this.persistAction();
        }
    }

    public List<MenuOption> getMenuOptions() {
        if (optMenuOptions.isEmpty()) {
            optMenuOptions = Optional.of(List.of(
                new ShowAllRecordsOption(),
                new AddNewRecordOption(),
                new RemoveRecordByEmailOption()
            ));
        }
        return optMenuOptions.get();
    }

    public ShutdownHandler getShutdownHandler() {
        if (optShutdownHandler.isEmpty()) {
            optShutdownHandler = Optional.of(new PersistHandler());
        }
        return optShutdownHandler.get();
    }

    public void run() {
        consoleMenu.getMenuOptions().addAll(this.getMenuOptions());
        consoleMenu.getShutdownHandlers().add(this.getShutdownHandler());
        consoleMenu.launchMenu();
    }
}
