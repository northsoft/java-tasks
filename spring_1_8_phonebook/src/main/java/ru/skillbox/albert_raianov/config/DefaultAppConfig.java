package ru.skillbox.albert_raianov.config;

import java.util.Objects;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import ru.skillbox.albert_raianov.phonebook.persistence.FileInitProvider;
import ru.skillbox.albert_raianov.phonebook.persistence.InitProvider;


@Configuration
@Profile("default")
public class DefaultAppConfig {
    private final Logger logger = LogManager.getLogger(DefaultAppConfig.class);


    private final String filename;

    public DefaultAppConfig(
            @Value("${phonebook.persistence.filename}") String filename
    ) {
        this.filename = Objects.requireNonNull(filename);
    }

    @Bean
    public InitProvider initProvider() {
        logger.debug("Initializing previously persisted data via file");
        return new FileInitProvider(filename);
    }
}
