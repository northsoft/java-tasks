package ru.skillbox.albert_raianov.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ComponentScan("ru.skillbox.albert_raianov")
@PropertySource(value = "classpath:application.yaml", factory = YamlPropertySourceFactory.class)
public class AppConfig {
    @SuppressWarnings("EmptyMethod")
    public void doNothing() {
        // this stub is created to count this class in test coverage
        //noinspection UnnecessaryReturnStatement
        return;
    }
}
