package ru.skillbox.albert_raianov.config;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import ru.skillbox.albert_raianov.phonebook.persistence.BuiltinContactsInitProvider;
import ru.skillbox.albert_raianov.phonebook.persistence.InitProvider;

@Configuration
@Profile("init")
public class InitAppConfig {
    private final static String DEFAULT_CONTACTS_FILE_NAME = "default-contacts.txt";
    private final Logger logger = LogManager.getLogger(InitAppConfig.class);
    @Bean
    public InitProvider initProvider() {
        logger.debug("Initializing data from hardcoded values");
        return new BuiltinContactsInitProvider(DEFAULT_CONTACTS_FILE_NAME);
    }
}
