package ru.skillbox.albert_raianov.config;

import org.junit.jupiter.api.Test;
import org.springframework.core.env.PropertySource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.support.EncodedResource;

import static org.junit.jupiter.api.Assertions.*;

class YamlPropertySourceFactoryTest {
    @Test
    public void whenPropertySourceCreated_thenOk() {
        EncodedResource resource = new EncodedResource(new ClassPathResource("test.yaml"));
        PropertySource<?> propertySource = new YamlPropertySourceFactory().createPropertySource(null, resource);
        assertEquals("truetestvalue", propertySource.getProperty("testroot.testchapter.testpart.testproperty"));
    }
}