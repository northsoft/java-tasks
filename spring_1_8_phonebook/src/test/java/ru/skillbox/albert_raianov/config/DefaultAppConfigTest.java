package ru.skillbox.albert_raianov.config;

import org.junit.jupiter.api.Test;
import ru.skillbox.albert_raianov.phonebook.persistence.FileInitProvider;
import ru.skillbox.albert_raianov.phonebook.persistence.InitProvider;

import static org.junit.jupiter.api.Assertions.*;

class DefaultAppConfigTest {
    @Test
    public void whenConfigIsCreated_thenOk() {
        DefaultAppConfig cfg = new DefaultAppConfig("test.txt");
        InitProvider initProvider = cfg.initProvider();
        assertEquals(
                FileInitProvider.class,
                initProvider.getClass()
        );
    }
}