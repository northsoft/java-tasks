package ru.skillbox.albert_raianov.config;

import org.junit.jupiter.api.Test;
import ru.skillbox.albert_raianov.phonebook.persistence.BuiltinContactsInitProvider;

import static org.junit.jupiter.api.Assertions.*;

class InitAppConfigTest {
    @Test
    public void whenInitProviderCreated_thenOk() {
        var initProvider = new InitAppConfig().initProvider();
        assertEquals(
                BuiltinContactsInitProvider.class,
                initProvider.getClass()
        );
    }
}