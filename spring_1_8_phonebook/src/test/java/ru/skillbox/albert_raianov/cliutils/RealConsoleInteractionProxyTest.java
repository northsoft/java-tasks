package ru.skillbox.albert_raianov.cliutils;

import java.io.BufferedReader;
import java.io.PrintStream;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import ru.skillbox.albert_raianov.cliutils.console_proxy.RealConsoleInteractionProxy;

import static org.junit.jupiter.api.Assertions.*;

class RealConsoleInteractionProxyTest {
    @Test
    void whenCreateDefaultConsoleProxy_thenOk() throws Exception {
        try (RealConsoleInteractionProxy consoleInteractionProxy = new RealConsoleInteractionProxy()) {
            assertEquals(BufferedReader.class, consoleInteractionProxy.getBufferedReader().getClass());
            assertEquals(PrintStream.class, consoleInteractionProxy.getPrintStream().getClass());
        }
    }

    @Test
    void  whenCloseBufferedReader_thenOk() throws Exception {
        BufferedReader mockReader = Mockito.mock(BufferedReader.class);
        Mockito.when(mockReader.ready()).thenReturn(true);
        PrintStream mockPrintStream = Mockito.mock(PrintStream.class);
        RealConsoleInteractionProxy consoleProxy = new RealConsoleInteractionProxy(mockReader, mockPrintStream);
        consoleProxy.close();
        Mockito.verify(mockReader).ready();
        Mockito.verify(mockReader).close();
    }

    @Test
    void  whenDestroyBufferedReader_thenOk() throws Exception {
        BufferedReader mockReader = Mockito.mock(BufferedReader.class);
        Mockito.when(mockReader.ready()).thenReturn(true);
        PrintStream mockPrintStream = Mockito.mock(PrintStream.class);
        RealConsoleInteractionProxy consoleProxy = new RealConsoleInteractionProxy(mockReader, mockPrintStream);
        consoleProxy.destroy();
        Mockito.verify(mockReader).ready();
        Mockito.verify(mockReader).close();
    }
}