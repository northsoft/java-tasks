package ru.skillbox.albert_raianov.cliutils;

import java.util.function.Function;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import ru.skillbox.albert_raianov.cliutils.console_menu.ConsoleMenu;
import ru.skillbox.albert_raianov.cliutils.console_menu.MenuOption;
import ru.skillbox.albert_raianov.cliutils.console_menu.ShutdownHandler;
import ru.skillbox.albert_raianov.cliutils.console_proxy.AbstractConsoleInteractionProxy;
import ru.skillbox.albert_raianov.cliutils.console_proxy.VirtualConsoleInteractionProxy;

@SuppressWarnings("unchecked")
@RunWith(MockitoJUnitRunner.class)

@Timeout(1000)
class ConsoleMenuTest {
    private final static int DEF_MAX_ATTEMPTS = 5;

    @Test
    void whenEverythingIsOk_thenOk() throws Exception {
        MenuOption opt1 = Mockito.mock(MenuOption.class);
        Mockito.when(opt1.getCaption()).thenReturn("First method");
        AbstractConsoleInteractionProxy virtualProxy = new VirtualConsoleInteractionProxy("1\n2\n");

        MultipleAttemptsLineReader mockLineReader = Mockito.mock(MultipleAttemptsLineReader.class);
        Mockito.when(mockLineReader.readValue(Mockito.anyInt(), Mockito.any(Function.class))).thenReturn(1, 2);

        ShutdownHandler sh1 = Mockito.mock(ShutdownHandler.class);

        ConsoleMenu consoleMenu = new ConsoleMenu(virtualProxy, mockLineReader, DEF_MAX_ATTEMPTS);
        consoleMenu.getMenuOptions().add(opt1);
        consoleMenu.getShutdownHandlers().add(sh1);
        consoleMenu.launchMenu();

        Mockito.verify(opt1, Mockito.atLeastOnce()).getCaption();
        Mockito.verify(opt1, Mockito.times(1)).doAction();
        Mockito.verify(sh1, Mockito.times(1)).doAction();
    }

    @Test
    void whenEverythingIsOkWithReset_thenOk() throws Exception {
        MenuOption opt1 = Mockito.mock(MenuOption.class);
        Mockito.when(opt1.getCaption()).thenReturn("First method");
        AbstractConsoleInteractionProxy virtualProxy = new VirtualConsoleInteractionProxy("1\n2\n");

        MultipleAttemptsLineReader mockLineReader = Mockito.mock(MultipleAttemptsLineReader.class);
        Mockito.when(mockLineReader.readValue(Mockito.anyInt(), Mockito.any(Function.class))).thenReturn(1, 2);

        ShutdownHandler sh1 = Mockito.mock(ShutdownHandler.class);

        ConsoleMenu consoleMenu = new ConsoleMenu(virtualProxy, mockLineReader, DEF_MAX_ATTEMPTS);
        consoleMenu.getMenuOptions().add(opt1);
        consoleMenu.getShutdownHandlers().add(sh1);
        consoleMenu.resetMenuOptions();
        consoleMenu.resetShutdownHandlers();
        consoleMenu.getMenuOptions().add(opt1);
        consoleMenu.getShutdownHandlers().add(sh1);

        consoleMenu.launchMenu();

        Mockito.verify(opt1, Mockito.atLeastOnce()).getCaption();
        Mockito.verify(opt1, Mockito.times(1)).doAction();
        Mockito.verify(sh1, Mockito.times(1)).doAction();
    }

    @Test
    void whenMenuIsEmpty_thenNothing() throws Exception {
        AbstractConsoleInteractionProxy virtualProxy = new VirtualConsoleInteractionProxy("");
        MultipleAttemptsLineReader mockReader = Mockito.mock(MultipleAttemptsLineReader.class);

        ConsoleMenu consoleMenu = new ConsoleMenu(virtualProxy, mockReader, DEF_MAX_ATTEMPTS);
        consoleMenu.launchMenu();

        Mockito.verify(mockReader, Mockito.never()).readValue(Mockito.anyInt(), Mockito.any(Function.class));
    }

    @Test
    void whenMenuOptionThrowsException_thenNothing() throws Exception {
        AbstractConsoleInteractionProxy virtualProxy = new VirtualConsoleInteractionProxy("");
        MultipleAttemptsLineReader mockReader = Mockito.mock(MultipleAttemptsLineReader.class);
        Mockito.when(mockReader.readValue(Mockito.anyInt(), Mockito.any(Function.class)))
                .thenReturn(1, 2);

        MenuOption explosiveOption = Mockito.mock(MenuOption.class);
        Mockito.when(explosiveOption.getCaption()).thenReturn("First option");
        Mockito.doThrow(new Exception()).when(explosiveOption).doAction();

        ShutdownHandler sh1 = Mockito.mock(ShutdownHandler.class);

        ConsoleMenu consoleMenu = new ConsoleMenu(virtualProxy, mockReader, DEF_MAX_ATTEMPTS);
        consoleMenu.getMenuOptions().add(explosiveOption);
        consoleMenu.getShutdownHandlers().add(sh1);

        consoleMenu.launchMenu();

        Mockito.verify(explosiveOption, Mockito.atLeastOnce()).getCaption();
        Mockito.verify(explosiveOption).doAction();
        Mockito.verify(sh1, Mockito.never()).doAction();
    }

    @Test
    void whenMenuOptionThrowsMultipleReadException_thenNothing() throws Exception {
        AbstractConsoleInteractionProxy virtualProxy = new VirtualConsoleInteractionProxy("");
        MultipleAttemptsLineReader mockReader = Mockito.mock(MultipleAttemptsLineReader.class);
        Mockito.when(mockReader.readValue(Mockito.anyInt(), Mockito.any(Function.class)))
                .thenReturn(1, 2);

        MenuOption explosiveOption = Mockito.mock(MenuOption.class);
        Mockito.when(explosiveOption.getCaption()).thenReturn("First option");
        Mockito.doThrow(new MultipleReadsFailedException(new RuntimeException("test"))).when(explosiveOption).doAction();

        ShutdownHandler sh1 = Mockito.mock(ShutdownHandler.class);

        ConsoleMenu consoleMenu = new ConsoleMenu(virtualProxy, mockReader, DEF_MAX_ATTEMPTS);
        consoleMenu.getMenuOptions().add(explosiveOption);
        consoleMenu.getShutdownHandlers().add(sh1);

        consoleMenu.launchMenu();

        Mockito.verify(explosiveOption, Mockito.atLeastOnce()).getCaption();
        Mockito.verify(explosiveOption).doAction();
        Mockito.verify(sh1, Mockito.never()).doAction();
    }



    @Test
    void whenShutdownHandlerThrowsException_thenNothing() throws Exception {
        MenuOption opt1 = Mockito.mock(MenuOption.class);
        Mockito.when(opt1.getCaption()).thenReturn("First method");
        AbstractConsoleInteractionProxy virtualProxy = new VirtualConsoleInteractionProxy("1\n2\n");

        MultipleAttemptsLineReader mockLineReader = Mockito.mock(MultipleAttemptsLineReader.class);
        Mockito.when(mockLineReader.readValue(Mockito.anyInt(), Mockito.any(Function.class))).thenReturn(1, 2);

        ShutdownHandler sh1 = Mockito.mock(ShutdownHandler.class);
        Mockito.doThrow(Exception.class).when(sh1).doAction();

        ConsoleMenu consoleMenu = new ConsoleMenu(virtualProxy, mockLineReader, DEF_MAX_ATTEMPTS);
        consoleMenu.getMenuOptions().add(opt1);
        consoleMenu.getShutdownHandlers().add(sh1);
        consoleMenu.launchMenu();

        Mockito.verify(opt1, Mockito.atLeastOnce()).getCaption();
        Mockito.verify(opt1, Mockito.times(1)).doAction();
        Mockito.verify(sh1, Mockito.times(1)).doAction();
    }

    @Test
    void whenWorkingWithRealReader_thenOk() throws Exception {
        AbstractConsoleInteractionProxy virtualConsoleProxy = new VirtualConsoleInteractionProxy("1\n2\n");
        MultipleAttemptsLineReader virtualReader = new MultipleAttemptsLineReader(virtualConsoleProxy);

        MenuOption mockOption = Mockito.mock(MenuOption.class);
        Mockito.when(mockOption.getCaption()).thenReturn("First option");

        ConsoleMenu menu = new ConsoleMenu(virtualConsoleProxy, virtualReader, DEF_MAX_ATTEMPTS);
        menu.getMenuOptions().add(mockOption);
        menu.launchMenu();

        Mockito.verify(mockOption, Mockito.atLeastOnce()).getCaption();
        Mockito.verify(mockOption).doAction();
    }

    @Test
    void whenWorkingWithRealReaderAndBadDataTwice_thenOk() throws Exception {
        AbstractConsoleInteractionProxy virtualConsoleProxy = new VirtualConsoleInteractionProxy("x\n3\n1\n2\n");
        MultipleAttemptsLineReader virtualReader = new MultipleAttemptsLineReader(virtualConsoleProxy);

        MenuOption mockOption = Mockito.mock(MenuOption.class);
        Mockito.when(mockOption.getCaption()).thenReturn("First option");

        ConsoleMenu menu = new ConsoleMenu(virtualConsoleProxy, virtualReader, DEF_MAX_ATTEMPTS);
        menu.getMenuOptions().add(mockOption);
        menu.launchMenu();

        Mockito.verify(mockOption, Mockito.atLeastOnce()).getCaption();
        Mockito.verify(mockOption).doAction();
    }

    @Test
    void whenWorkingWithExhaustedDataAtRealReader_thenNothing() {
        AbstractConsoleInteractionProxy virtualConsoleProxy = new VirtualConsoleInteractionProxy("");
        MultipleAttemptsLineReader virtualReader = new MultipleAttemptsLineReader(virtualConsoleProxy);

        MenuOption mockOption = Mockito.mock(MenuOption.class);
        Mockito.when(mockOption.getCaption()).thenReturn("First option");

        ConsoleMenu menu = new ConsoleMenu(virtualConsoleProxy, virtualReader, DEF_MAX_ATTEMPTS);
        menu.getMenuOptions().add(mockOption);
        menu.launchMenu();

        Mockito.verify(mockOption, Mockito.atLeastOnce()).getCaption();
    }

}