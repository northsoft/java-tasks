package ru.skillbox.albert_raianov.phonebook.persistence;

import org.junit.jupiter.api.Test;
import ru.skillbox.albert_raianov.phonebook.app.StoredUser;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class FilePersistProviderTest {
    @Test
    public void whenPersistOk_thenOk() throws Exception {
        List<StoredUser> usersToPersist = List.of(
                new StoredUser("First Test",  "123-73-53", "first@test.com"),
                new StoredUser("Second Test", "456-86-96", "second@test.com")
        );

        File outputFile = File.createTempFile("temp-", ".txt");
        try {
            FilePersistProvider filePersistProvider = new FilePersistProvider(outputFile.getPath());
            filePersistProvider.persistUsers(usersToPersist);
            List<String> expectedOutput = List.of(
                    "First Test;123-73-53;first@test.com",
                    "Second Test;456-86-96;second@test.com"
            );
            List<String> actualOutput = Files.readAllLines(outputFile.toPath());
            assertEquals(expectedOutput, actualOutput);
        } finally {
            //noinspection ResultOfMethodCallIgnored
            outputFile.delete();
        }
    }

    @Test
    public void whenPersistFailed_thenPersistenceException() throws Exception {
        List<StoredUser> usersToPersist = List.of(
                new StoredUser("First Test",  "123-73-53", "first@test.com"),
                new StoredUser("Second Test", "456-86-96", "second@test.com")
        );
        Path tmpdir = null;
        try {
            tmpdir = Files.createTempDirectory("temp-");
            FilePersistProvider filePersistProvider = new FilePersistProvider(tmpdir.toString());
            assertThrows(PersistenceException.class, () -> filePersistProvider.persistUsers(usersToPersist));
        } finally {
            if (tmpdir != null)
                Files.delete(tmpdir);
        }
    }
}