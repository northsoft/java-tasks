package ru.skillbox.albert_raianov.phonebook.persistence;

import org.junit.jupiter.api.Test;
import ru.skillbox.albert_raianov.phonebook.app.StoredUser;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

class FileInitProviderTest {
    private static class TemporaryFile implements AutoCloseable {
        private final File file;

        public TemporaryFile() throws IOException {
            this.file = File.createTempFile("temp-", ".txt");
        }

        public File getFile() {
            return file;
        }

        @Override
        public void close() {
            //noinspection ResultOfMethodCallIgnored
            this.file.delete();
        }
    }

    @Test
    public void whenFileLoadsOk_thenOk() throws Exception {
        try (TemporaryFile temporaryFile = new TemporaryFile()) {
            String testData = """
                    First Test;255-45-75;first@test.com
                    This line is intentionally malformed
                    Second Test;377-27-57;second@test.com
                    """;
            Files.writeString(Path.of(temporaryFile.getFile().getPath()), testData);

            FileInitProvider fileInitProvider = new FileInitProvider(temporaryFile.getFile().getPath());
            List<StoredUser> storedUsers = fileInitProvider.getStoredUsers();
            List<StoredUser> expected = List.of(
                    new StoredUser("First Test", "255-45-75", "first@test.com"),
                    new StoredUser("Second Test", "377-27-57", "second@test.com")
            );
            assertEquals(expected, storedUsers);
        }
    }

    @Test
    public void whenFileLoadIsFailed_thenException() {
        String aprioriNonexistentPath = UUID.randomUUID().toString();
        assertThrows(PersistenceException.class, () -> {
            FileInitProvider fileInitProvider = new FileInitProvider(aprioriNonexistentPath);
            fileInitProvider.getStoredUsers();
        });
    }
}