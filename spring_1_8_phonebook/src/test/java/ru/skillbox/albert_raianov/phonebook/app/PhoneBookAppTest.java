package ru.skillbox.albert_raianov.phonebook.app;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import ru.skillbox.albert_raianov.phonebook.persistence.InitProvider;
import ru.skillbox.albert_raianov.phonebook.persistence.PersistProvider;
import ru.skillbox.albert_raianov.phonebook.persistence.PersistenceException;

import static org.junit.jupiter.api.Assertions.*;


import java.util.List;

class PhoneBookAppTest {
    private static class ProxyInitProvider implements InitProvider {
        private List<StoredUser> storedUsers = null;

        public void setStoredUsers(List<StoredUser> storedUsers) {
            this.storedUsers = storedUsers;
        }

        @Override
        public List<StoredUser> getStoredUsers() {
            return storedUsers;
        }
    }

    private static class ProxyPersistProvider implements PersistProvider {
        private List<StoredUser> storedUsers = null;

        public List<StoredUser> getStoredUsers() {
            return storedUsers;
        }

        @Override
        public void persistUsers(List<StoredUser> users) {
            this.storedUsers = users;
        }
    }

    @Test
    void whenItemsAreLoadedAddedAlteredAndRemoved_thenOk() throws Exception {
        ProxyInitProvider proxyInitProvider = new ProxyInitProvider();
        proxyInitProvider.setStoredUsers(List.of(
                new StoredUser("Alpha Bravo", "+1(234)5678901", "alphabravo@test.us"),
                new StoredUser("Charlie Delta", "+234(1567)8901", "charliedelta@test.ng"),
                new StoredUser("Echo Foxtrot", "+56(789)012345", "echofoxtrot@test.cl")
        ));
        ProxyPersistProvider proxyPersistProvider = new ProxyPersistProvider();

        PhoneBookApp phoneBookApp = new PhoneBookApp(proxyInitProvider, proxyPersistProvider);

        PhoneBookApp.AddResult addResult = phoneBookApp.addRecord(
                new StoredUser("Golf Hotel", "+7(809)1234567", "golfhotel@test.ru")
        );
        assertSame(PhoneBookApp.AddResult.CREATED_NEW, addResult);
        addResult = phoneBookApp.addRecord(
                new StoredUser("India Juliet", "+7(426)5310987", "golfhotel@test.ru")
        );
        assertSame(PhoneBookApp.AddResult.UPDATED_EXISTENT, addResult);
        PhoneBookApp.RemoveResult removeResult = phoneBookApp.removeRecord("alphabravo@test.us");
        assertSame(PhoneBookApp.RemoveResult.REMOVED, removeResult);
        removeResult = phoneBookApp.removeRecord("fake@fake.com");
        assertSame(PhoneBookApp.RemoveResult.NOT_FOUND, removeResult);

        List<StoredUser> controlValues = List.of(
                new StoredUser("Charlie Delta", "+234(1567)8901", "charliedelta@test.ng"),
                new StoredUser("Echo Foxtrot", "+56(789)012345", "echofoxtrot@test.cl"),
                new StoredUser("India Juliet", "+7(426)5310987", "golfhotel@test.ru")
        );
        List<StoredUser> requestedValues = phoneBookApp.getAllUsers();
        assertEquals(controlValues, requestedValues);
        phoneBookApp.persistAll();
        assertEquals(controlValues, proxyPersistProvider.getStoredUsers());
    }

    @Test
    void whenExceptionDuringInit_thenPhoneBookAppException() throws Exception {
        InitProvider explosiveInitProvider = Mockito.mock(InitProvider.class);
        Mockito.when(explosiveInitProvider.getStoredUsers())
                .thenThrow(new PersistenceException("test", new RuntimeException("test")));
        PersistProvider mockPersistProvider = Mockito.mock(PersistProvider.class);
        assertThrows(PhoneBookAppException.class, () -> new PhoneBookApp(explosiveInitProvider, mockPersistProvider));
    }

    @Test
    void whenExceptionDuringPersist_thenPhoneBookAppException() throws Exception {
        InitProvider mockInitProvider = Mockito.mock(InitProvider.class);
        Mockito.when(mockInitProvider.getStoredUsers()).thenReturn(List.of(
                new StoredUser("Alpha Bravo", "+1(234)567890", "alphabravo@test.us")
        ));

        PersistProvider explosivePersistProvider = Mockito.mock(PersistProvider.class);
        Mockito.doThrow(new PersistenceException("test", new RuntimeException("test")))
                .when(explosivePersistProvider).persistUsers(Mockito.anyList());
        PhoneBookApp phoneBookApp = new PhoneBookApp(mockInitProvider, explosivePersistProvider);
        phoneBookApp.addRecord(
                new StoredUser("Charlie Delta", "+234(1567)8901", "charliedelta@test.ng")
        );
        assertThrows(PhoneBookAppException.class, phoneBookApp::persistAll);
    }
}