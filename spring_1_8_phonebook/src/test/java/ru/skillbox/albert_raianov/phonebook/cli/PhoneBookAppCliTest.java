package ru.skillbox.albert_raianov.phonebook.cli;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import ru.skillbox.albert_raianov.cliutils.MultipleReadsFailedException;
import ru.skillbox.albert_raianov.cliutils.console_menu.ConsoleMenu;
import ru.skillbox.albert_raianov.cliutils.MultipleAttemptsLineReader;
import ru.skillbox.albert_raianov.cliutils.console_menu.MenuOption;
import ru.skillbox.albert_raianov.cliutils.console_menu.ShutdownHandler;
import ru.skillbox.albert_raianov.cliutils.console_proxy.AbstractConsoleInteractionProxy;
import ru.skillbox.albert_raianov.cliutils.console_proxy.VirtualConsoleInteractionProxy;
import ru.skillbox.albert_raianov.phonebook.app.PhoneBookApp;
import ru.skillbox.albert_raianov.phonebook.app.StoredUser;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

@SuppressWarnings("unchecked")
class PhoneBookAppCliTest {
    private final int DEFAULT_MAX_ATTEMPTS = 5;

    @Test
    void showAllRecords_thenOk() {
        VirtualConsoleInteractionProxy virtualConsoleProxy = new VirtualConsoleInteractionProxy("");
        PhoneBookApp mockApp = Mockito.mock(PhoneBookApp.class);
        Mockito.when(mockApp.getAllUsers()).thenReturn(List.of(
                new StoredUser("Alpha Bravo", "+1234567890", "alphabravo@test.com"),
                new StoredUser("Charlie Delta", "+2345678901", "charliedelta@test.com")
        ));

        PhoneBookAppCli phoneBookAppCli = new PhoneBookAppCli(
                Mockito.mock(ConsoleMenu.class),
                mockApp,
                Mockito.mock(MultipleAttemptsLineReader.class),
                virtualConsoleProxy,
                DEFAULT_MAX_ATTEMPTS
        );
        phoneBookAppCli.showAllRecordsAction();

        String expectedValue = """
                  Alpha Bravo | +1234567890 |   alphabravo@test.com
                Charlie Delta | +2345678901 | charliedelta@test.com
                """;
        assertEquals(expectedValue, virtualConsoleProxy.getVirtualOutput());
        Mockito.verify(mockApp).getAllUsers();
    }

    @Test
    void showAllRecordsWhenEmpty_thenOk() throws Exception {
        VirtualConsoleInteractionProxy virtualConsoleProxy = new VirtualConsoleInteractionProxy("");
        PhoneBookApp mockApp = Mockito.mock(PhoneBookApp.class);
        Mockito.when(mockApp.getAllUsers()).thenReturn(new ArrayList<>());

        PhoneBookAppCli phoneBookAppCli = new PhoneBookAppCli(
                Mockito.mock(ConsoleMenu.class),
                mockApp,
                Mockito.mock(MultipleAttemptsLineReader.class),
                virtualConsoleProxy,
                DEFAULT_MAX_ATTEMPTS
        );

        MenuOption option = getMenuOptionByClass(phoneBookAppCli, PhoneBookAppCli.ShowAllRecordsOption.class);
        assertEquals("Show all records", option.getCaption());

        option.doAction();
        assertEquals("No records stored\n", virtualConsoleProxy.getVirtualOutput());
        Mockito.verify(mockApp).getAllUsers();
    }

    private <T> MenuOption getMenuOptionByClass(PhoneBookAppCli appCli, Class<T> classToFind) {
        List<MenuOption> candidates = appCli.getMenuOptions().stream().
                filter(menuOption -> menuOption.getClass().equals(classToFind))
                .toList();
        assertEquals(1, candidates.size());
        return candidates.get(0);
    }

    @Test
    void emailConverter() {
        PhoneBookAppCli phoneBookAppCli = new PhoneBookAppCli(
                Mockito.mock(ConsoleMenu.class),
                Mockito.mock(PhoneBookApp.class),
                Mockito.mock(MultipleAttemptsLineReader.class),
                Mockito.mock(AbstractConsoleInteractionProxy.class),
                DEFAULT_MAX_ATTEMPTS);
        assertEquals("test@test.com", phoneBookAppCli.emailConverter.apply("test@test.com"));
        assertThrows(RuntimeException.class, () -> phoneBookAppCli.emailConverter.apply("fake.mail"));
    }

    @Test
    void addNewRecordAction() throws Exception {
        String input = "First Test;2554505;test@ngs.ru\nSecond Test;4597929;test2@ngs.ru\n";
        VirtualConsoleInteractionProxy virtualConsoleProxy = new VirtualConsoleInteractionProxy(input);
        MultipleAttemptsLineReader mockLineReader = Mockito.mock(MultipleAttemptsLineReader.class);
        PhoneBookApp mockApp = Mockito.mock(PhoneBookApp.class);
        PhoneBookAppCli phoneBookAppCli = new PhoneBookAppCli(
                Mockito.mock(ConsoleMenu.class),
                mockApp,
                mockLineReader,
                virtualConsoleProxy,
                DEFAULT_MAX_ATTEMPTS
        );
        MenuOption addNewRecordOption = getMenuOptionByClass(phoneBookAppCli, PhoneBookAppCli.AddNewRecordOption.class);
        assertEquals("Add new record", addNewRecordOption.getCaption());

        Mockito.when(mockLineReader.readValue(Mockito.anyInt(), Mockito.any(Function.class)))
                .thenReturn(new StoredUser("Third Test", "6932373", "test3@ngs.ru"));
        Mockito.when(mockApp.addRecord(Mockito.any(StoredUser.class))).thenReturn(PhoneBookApp.AddResult.CREATED_NEW);
        addNewRecordOption.doAction();

        Mockito.when(mockLineReader.readValue(Mockito.anyInt(), Mockito.any(Function.class)))
                .thenReturn(new StoredUser("Newfirst Test", "5227050", "test@ngs.ru"));
        Mockito.when(mockApp.addRecord(Mockito.any(StoredUser.class))).thenReturn(PhoneBookApp.AddResult.UPDATED_EXISTENT);
        phoneBookAppCli.addNewRecordAction();

        Mockito.when(mockLineReader.readValue(Mockito.anyInt(), Mockito.any(Function.class)))
                .thenThrow(new MultipleReadsFailedException(new RuntimeException("test")));
        phoneBookAppCli.addNewRecordAction();

        String expectedOutput = """
                Please type data in format “Name; Phone; Email”: New record created
                Please type data in format “Name; Phone; Email”: Record updated
                Please type data in format “Name; Phone; Email”: Unable to read record data: Unable to read value multiple times, last error is test
                """;
        assertEquals(expectedOutput, virtualConsoleProxy.getVirtualOutput());
    }

    @Test
    public void removeRecordByEmailAction() throws Exception {
        String input = "First Test;2554505;test@ngs.ru\nSecond Test;4597929;test2@ngs.ru\n";
        VirtualConsoleInteractionProxy virtualConsoleProxy = new VirtualConsoleInteractionProxy(input);
        MultipleAttemptsLineReader mockLineReader = Mockito.mock(MultipleAttemptsLineReader.class);
        PhoneBookApp mockApp = Mockito.mock(PhoneBookApp.class);
        PhoneBookAppCli phoneBookAppCli = new PhoneBookAppCli(
                Mockito.mock(ConsoleMenu.class),
                mockApp,
                mockLineReader,
                virtualConsoleProxy,
                DEFAULT_MAX_ATTEMPTS
        );
        MenuOption removeOption = getMenuOptionByClass(phoneBookAppCli, PhoneBookAppCli.RemoveRecordByEmailOption.class);
        assertEquals("Remove record by email", removeOption.getCaption());

        Mockito.when(mockLineReader.readValue(Mockito.anyInt(), Mockito.any(Function.class)))
                .thenReturn("test@ngs.ru");
        Mockito.when(mockApp.removeRecord(Mockito.anyString())).thenReturn(PhoneBookApp.RemoveResult.REMOVED);
        removeOption.doAction();

        Mockito.when(mockLineReader.readValue(Mockito.anyInt(), Mockito.any(Function.class)))
                .thenReturn("never@existed.com");
        Mockito.when(mockApp.removeRecord(Mockito.anyString())).thenReturn(PhoneBookApp.RemoveResult.NOT_FOUND);
        phoneBookAppCli.removeRecordByEmailAction();

        Mockito.when(mockLineReader.readValue(Mockito.anyInt(), Mockito.any(Function.class)))
                .thenThrow(new MultipleReadsFailedException(new RuntimeException("test")));
        phoneBookAppCli.removeRecordByEmailAction();

        String expectedOutput = """
                Please type email to remove: Record removed
                Please type email to remove: Record with this email was not found
                Please type email to remove: Unable to process email input
                """;
        assertEquals(expectedOutput, virtualConsoleProxy.getVirtualOutput());
    }

    @Test
    public void persistAction() throws Exception {
        PhoneBookApp mockApp = Mockito.mock(PhoneBookApp.class);
        PhoneBookAppCli phoneBookAppCli = new PhoneBookAppCli(
                Mockito.mock(ConsoleMenu.class),
                mockApp,
                Mockito.mock(MultipleAttemptsLineReader.class),
                Mockito.mock(AbstractConsoleInteractionProxy.class),
                DEFAULT_MAX_ATTEMPTS
        );
        phoneBookAppCli.getShutdownHandler().doAction();

        Mockito.verify(mockApp).persistAll();
    }

    @Test
    public void run() {
        ConsoleMenu mockMenu = Mockito.mock(ConsoleMenu.class);
        PhoneBookAppCli phoneBookAppCli = new PhoneBookAppCli(
                mockMenu,
                Mockito.mock(PhoneBookApp.class),
                Mockito.mock(MultipleAttemptsLineReader.class),
                Mockito.mock(AbstractConsoleInteractionProxy.class),
                DEFAULT_MAX_ATTEMPTS
        );
        List<MenuOption> menuOptions = new ArrayList<>();
        Mockito.when(mockMenu.getMenuOptions()).thenReturn(menuOptions);
        List<ShutdownHandler> shutdownHandlers = new ArrayList<>();
        Mockito.when(mockMenu.getShutdownHandlers()).thenReturn(shutdownHandlers);

        phoneBookAppCli.run();

        Mockito.verify(mockMenu).launchMenu();

        assertEquals(
                Set.of(
                        PhoneBookAppCli.ShowAllRecordsOption.class,
                        PhoneBookAppCli.AddNewRecordOption.class,
                        PhoneBookAppCli.RemoveRecordByEmailOption.class
                ),
                menuOptions.stream().map(Object::getClass).collect(Collectors.toSet())
        );

        assertEquals(PhoneBookAppCli.PersistHandler.class, shutdownHandlers.get(0).getClass());
    }
}