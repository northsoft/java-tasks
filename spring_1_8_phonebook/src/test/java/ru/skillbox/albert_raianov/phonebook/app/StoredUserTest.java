package ru.skillbox.albert_raianov.phonebook.app;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StoredUserTest {

    @Test
    void fromCsvString() {
        assertEquals(
                new StoredUser("Abc def", "88001234567", "abc@def.ghi"),
                StoredUser.fromCsvString("Abc def;88001234567;abc@def.ghi")
        );
        assertThrows(Exception.class, () -> StoredUser.fromCsvString("a"));
        assertThrows(Exception.class, () -> StoredUser.fromCsvString("b;c"));
        assertThrows(Exception.class, () -> StoredUser.fromCsvString("d;e;f"));
        assertThrows(Exception.class, () -> StoredUser.fromCsvString("g;h;i;j"));
        assertThrows(Exception.class, () -> StoredUser.fromCsvString(" ;  ;   "));
        assertThrows(Exception.class, () -> StoredUser.fromCsvString("a;bc;   "));
        assertThrows(Exception.class, () -> StoredUser.fromCsvString("a;  ;bcd"));
        assertThrows(Exception.class, () -> StoredUser.fromCsvString(" ;ab;cde"));

    }

    @Test
    void fromUserInputString() {
        assertEquals(
                new StoredUser("Abc def", "8(800)800-0800", "abc@def.ghi"),
                StoredUser.fromUserInputString("Abc def; 8(800)800-0800; abc@def.ghi")
        );
        assertThrows(Exception.class, () -> StoredUser.fromUserInputString("a;b;c;d"));
    }

    @Test
    void testToString() {
        assertEquals(
                "abc | 8(800)800-0800 | test@test.com",
                new StoredUser("abc", "8(800)800-0800", "test@test.com").toString()
        );
    }

    @Test
    void testToString1() {
        assertEquals(
                "  abc |   112 |    112@hilfe.de",
                new StoredUser("abc", "112", "112@hilfe.de")
                        .toString(5, 5, 15)
        );
    }

    @Test
    void toCsvString() {
        assertEquals(
                "Abc Def;8(800)800-0800;test@test.com",
                new StoredUser("Abc Def", "8(800)800-0800", "test@test.com").toCsvString()
        );
    }
}