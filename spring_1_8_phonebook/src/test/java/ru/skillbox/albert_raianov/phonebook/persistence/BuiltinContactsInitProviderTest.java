package ru.skillbox.albert_raianov.phonebook.persistence;

import org.junit.jupiter.api.Test;
import ru.skillbox.albert_raianov.phonebook.app.StoredUser;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class BuiltinContactsInitProviderTest {
    @Test
    public void whenInitIsOk_thenOk() throws Exception {
        List<StoredUser> actual = new BuiltinContactsInitProvider("default-contacts.txt").getStoredUsers();
        List<StoredUser> expected = List.of(
                new StoredUser("Раянов Альберт Маратович", "+7(953)7890123", "albert@novosibirsk.ru"),
                new StoredUser("ԱԼԲԵՐՏ ՌԱՅԱՆՈՎ", "+374(55)456789", "albert@yerevan.am"),
                new StoredUser("Albert Rajanow", "+49(174)2791357", "albert@darmstadt.de")
        );
        assertEquals(expected, actual);
    }

    @Test
    public void whenInitIsFailed_thenPersistenceException() {
        assertThrows(
                PersistenceException.class,
                () -> new BuiltinContactsInitProvider("fake-filename.txt").getStoredUsers()
        );
    }
}