package ru.skillbox.albert_raianov.spring_2_7_student_list;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Spring_2_7_StudentListApplication {

	public static void main(String[] args) {
		SpringApplication.run(Spring_2_7_StudentListApplication.class, args);
	}

}
