package ru.skillbox.albert_raianov.spring_2_7_student_list.app;

import lombok.Getter;
import org.springframework.context.ApplicationEvent;
import ru.skillbox.albert_raianov.spring_2_7_student_list.data.Student;

@Getter
public class StudentAddedEvent extends ApplicationEvent {
    private final Student student;

    public StudentAddedEvent(Object source, Student student) {
        super(source);
        this.student = student;
    }
}
