package ru.skillbox.albert_raianov.spring_2_7_student_list.app;

import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.skillbox.albert_raianov.spring_2_7_student_list.data.StudentWithoutId;


@Component
@ConditionalOnProperty("student-list.hardcoded-records-inserter.enabled")
@RequiredArgsConstructor
public class HardcodedRecordsInserter {
    private final StudentListApp studentListApp;

    @EventListener(ApplicationStartedEvent.class)
    public void addHardcodedRecords() {
        List<StudentWithoutId> hardcodedStudents = List.of(
                new StudentWithoutId("Альберт", "Раянов", 31),
                new StudentWithoutId("ԱԼԲԵՐՏ", "ՌԱՅԱՆՈՎ", 32),
                new StudentWithoutId("Albert", "Rajanow", 33)
        );
        hardcodedStudents.forEach(studentListApp::addStudent);
    }
}
