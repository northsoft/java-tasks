package ru.skillbox.albert_raianov.spring_2_7_student_list.data;

public record StudentWithoutId(String name, String surname, int age) {
    public StudentWithoutId {
        if (name.isBlank()) {
            throw new IllegalArgumentException("Student's name should not be blank");
        }
        if (surname.isBlank()) {
            throw new IllegalArgumentException("Student's surname should not be blank");
        }
        if (age < 0) {
            throw new IllegalArgumentException("Student's age should not be negative");
        }
    }
}
