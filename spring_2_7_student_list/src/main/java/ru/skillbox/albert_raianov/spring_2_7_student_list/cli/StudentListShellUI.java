package ru.skillbox.albert_raianov.spring_2_7_student_list.cli;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.function.Function;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import lombok.RequiredArgsConstructor;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import ru.skillbox.albert_raianov.spring_2_7_student_list.data.Student;
import ru.skillbox.albert_raianov.spring_2_7_student_list.data.StudentWithoutId;
import ru.skillbox.albert_raianov.spring_2_7_student_list.app.StudentListApp;


@ShellComponent
@RequiredArgsConstructor
public class StudentListShellUI {
    private final StudentListApp studentListApp;

    @ShellMethod(key = "list-students")
    public String listStudents() {
        List<Student> students = studentListApp.getStudents();
        if (students.isEmpty()) {
            return "No students stored";
        }

        int idWidth      = fieldsMaxWidth( "ID",        students, student -> Integer.toString(student.id()));
        int nameWidth    = fieldsMaxWidth( "Name",      students, Student::name);
        int surnameWidth = fieldsMaxWidth( "Surname",   students, Student::surname);
        int ageWidth     = fieldsMaxWidth( "Age",       students, student -> Integer.toString(student.age()));

        String caption = String.format(
                "%" + idWidth + "s | %" + nameWidth + "s %" + surnameWidth + "s | %" + ageWidth + "s",
                "ID", "Name", "Surname", "Age"
        );
        List<String> studentStrings = students.stream()
                .map(student -> student.toString(idWidth, nameWidth, surnameWidth, ageWidth))
                .toList();
        return caption + "\n" + String.join("\n", studentStrings);
    }

    private <Type> int fieldsMaxWidth(String caption, List<Type> list, Function<Type, String> fieldExtractor) {
        IntStream widthsWithoutCaption = list.stream().map(fieldExtractor).mapToInt(String::length);
        IntStream widthsWithCaption = IntStream.concat(
                Stream.of(caption.length()).mapToInt(Integer::valueOf),
                widthsWithoutCaption
        );
        return widthsWithCaption.max().orElse(0);
    }

    @ShellMethod(key = "add-student")
    public String addStudent(String name, String surname, int age) {
        studentListApp.addStudent(new StudentWithoutId(name, surname, age));
        return "Student added";
    }

    @ShellMethod(key = "remove-student")
    public String removeStudent(int id) {
        try {
            studentListApp.removeStudent(id);
            return "Student removed";
        } catch (NoSuchElementException e) {
            return "No such record found";
        }
    }

    @ShellMethod(key = "remove-all")
    public String clearAll() {
        try {
            studentListApp.clearAll();
            return "All records are removed";
        } catch (NoSuchElementException e) {
            return "Nothing to delete";
        }
    }

}
