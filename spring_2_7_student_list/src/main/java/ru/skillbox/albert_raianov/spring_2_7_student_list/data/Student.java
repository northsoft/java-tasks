package ru.skillbox.albert_raianov.spring_2_7_student_list.data;

public record Student(int id, String name, String surname, int age) {
    public Student {
        if (id < 0) {
            throw new IllegalArgumentException("Student's ID should not be negative");
        }
        if (name.isBlank()) {
            throw new IllegalArgumentException("Student's name should not be blank");
        }
        if (surname.isBlank()) {
            throw new IllegalArgumentException("Student's surname should not be blank");
        }
        if (age < 0) {
            throw new IllegalArgumentException("Student's age should not be negative");
        }
    }

    public Student(int id, StudentWithoutId studentWithoutId) {
        this(
                id,
                studentWithoutId.name(),
                studentWithoutId.surname(),
                studentWithoutId.age()
        );
    }

    public String toString(int idWidth, int nameWidth, int surnameWidth, int ageWidth) {
        String format = String.format("%%%dd | %%%ds %%%ds | %%%dd",idWidth, nameWidth, surnameWidth, ageWidth);
        return String.format(format, id, name, surname, age);
    }
}
