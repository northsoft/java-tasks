package ru.skillbox.albert_raianov.spring_2_7_student_list.app;

import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.skillbox.albert_raianov.spring_2_7_student_list.util.IntRangeManager;

@Component
public class IdManager {
    private final IntRangeManager intRangeManager = new IntRangeManager();

    @EventListener
    public void onStudentAdded(StudentAddedEvent event) {
        int addedId = event.getStudent().id();
        intRangeManager.addInt(addedId);
    }

    @EventListener
    public void onStudentRemoved(StudentRemovedEvent event) {
        int removedId = event.getRemovedStudentId();
        intRangeManager.removeInt(removedId);
    }

    public int getFreeId() {
        return intRangeManager.getFirstFreeValue();
    }
}
