package ru.skillbox.albert_raianov.spring_2_7_student_list.util;

import java.util.TreeMap;

public class IntRangeManager {
    private final TreeMap<Integer, Integer> storedRanges = new TreeMap<>();

    public void addInt(int value) {
        if (storedRanges.isEmpty()) {
            storedRanges.put(value, value);
            return;
        }

        if (valueIsAlreadyStored(value)) {
            return;
        }

        if (valueWillJoinTwoRanges(value)) {
            int leftValueBeforeKey = storedRanges.floorKey(value);
            int leftValueAfterKey = storedRanges.ceilingKey(value);
            int rightValueAfterKey = storedRanges.get(leftValueAfterKey);
            storedRanges.remove(leftValueAfterKey);
            storedRanges.put(leftValueBeforeKey, rightValueAfterKey);
            return;
        }

        if (valueWillJoinToLeftRange(value)) {
            int leftValueBeforeKey = storedRanges.floorKey(value);
            storedRanges.put(leftValueBeforeKey, value);
            return;
        }

        if (valueWillJoinToRightRange(value)) {
            int leftValueAfterKey = storedRanges.ceilingKey(value);
            int rightValueAfterKey = storedRanges.get(leftValueAfterKey);
            storedRanges.remove(leftValueAfterKey);
            storedRanges.put(value, rightValueAfterKey);
            return;
        }

        storedRanges.put(value, value);
    }

    public void removeInt(int valueToRemove) {
        if (storedRanges.isEmpty() || !valueIsAlreadyStored(valueToRemove)) {
            return;
        }

        if (valueIsSingle(valueToRemove)) {
            storedRanges.remove(valueToRemove);
        }

        if (valueIsInsideRangeBorders(valueToRemove)) {
            int oldLeftValue = storedRanges.floorKey(valueToRemove);
            int oldRightValue = storedRanges.get(oldLeftValue);
            storedRanges.put(oldLeftValue, valueToRemove - 1);
            storedRanges.put(valueToRemove + 1, oldRightValue);
            return;
        }

        if (valueIsAtLeftBorder(valueToRemove)) {
            int oldRightValue = storedRanges.get(valueToRemove);
            storedRanges.remove(valueToRemove);
            storedRanges.put(valueToRemove + 1, oldRightValue);
            return;
        }

        if (valueIsAtRightBorder(valueToRemove)) {
            int oldLeftValue = storedRanges.floorKey(valueToRemove);
            storedRanges.put(oldLeftValue, valueToRemove - 1);
        }
    }

    public int getFirstFreeValue() {
        if (storedRanges.isEmpty() || storedRanges.firstKey() != 1) {
            return 1;
        }
        return storedRanges.get(storedRanges.firstKey()) + 1;
    }

    private boolean valueIsAlreadyStored(int value) {
        Integer leftValueAfterKey = storedRanges.ceilingKey(value);
        if (leftValueAfterKey != null && leftValueAfterKey == value) {
            return true;
        }

        Integer leftValueBeforeKey = storedRanges.floorKey(value);
        if (leftValueBeforeKey != null) {
            int rightValueBeforeKey = storedRanges.get(leftValueBeforeKey);
            return rightValueBeforeKey >= value;
        }

        return false;
    }

    private boolean valueWillJoinTwoRanges(int valueToAdd) {
        Integer leftValueBeforeKey = storedRanges.floorKey(valueToAdd);
        if (leftValueBeforeKey == null) {
            return false;
        }
        Integer leftValueAfterKey = storedRanges.ceilingKey(valueToAdd);
        if (leftValueAfterKey == null) {
            return false;
        }
        int rightValueBeforeKey = storedRanges.get(leftValueBeforeKey);
        return rightValueBeforeKey == valueToAdd - 1 && leftValueAfterKey == valueToAdd + 1;
    }

    private boolean valueWillJoinToLeftRange(int valueToAdd) {
        Integer leftValueBeforeKey = storedRanges.floorKey(valueToAdd);
        if (leftValueBeforeKey == null) {
            return false;
        }
        int rightValueBeforeKey = storedRanges.get(leftValueBeforeKey);
        return rightValueBeforeKey == valueToAdd - 1;
    }

    private boolean valueWillJoinToRightRange(int valueToAdd) {
        Integer leftValueAfterKey = storedRanges.ceilingKey(valueToAdd);
        if (leftValueAfterKey == null) {
            return false;
        }

        return leftValueAfterKey == valueToAdd + 1;
    }

    private boolean valueIsInsideRangeBorders(int value) {
        Integer leftValueBeforeKey = storedRanges.floorKey(value);
        if (leftValueBeforeKey == null) {
            return false;
        }
        int rightValue = storedRanges.get(leftValueBeforeKey);
        return leftValueBeforeKey < value && value < rightValue;
    }

    private boolean valueIsSingle(int value) {
        Integer leftValueBeforeKey = storedRanges.floorKey(value);
        if (leftValueBeforeKey == null) {
            return false;
        }
        int rightValue = storedRanges.get(leftValueBeforeKey);
        return leftValueBeforeKey == value && rightValue == value;
    }

    private boolean valueIsAtLeftBorder(int value) {
        Integer leftValueBeforeKey = storedRanges.floorKey(value);
        if (leftValueBeforeKey == null) {
            return false;
        }
        int rightValue = storedRanges.get(leftValueBeforeKey);
        return leftValueBeforeKey == value && rightValue > value;
    }

    private boolean valueIsAtRightBorder(int value) {
        Integer leftValueBeforeKey = storedRanges.floorKey(value);
        if (leftValueBeforeKey == null) {
            return false;
        }
        int rightValue = storedRanges.get(leftValueBeforeKey);
        return leftValueBeforeKey < value && rightValue == value;
    }
}
