package ru.skillbox.albert_raianov.spring_2_7_student_list.app;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.NoSuchElementException;
import lombok.RequiredArgsConstructor;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import ru.skillbox.albert_raianov.spring_2_7_student_list.data.Student;
import ru.skillbox.albert_raianov.spring_2_7_student_list.data.StudentWithoutId;


@Component
@RequiredArgsConstructor
public class StudentListApp {
    private final ApplicationEventPublisher eventPublisher;
    private final IdManager idManager;

    private final List<Student> students = new ArrayList<>();

    public List<Student> getStudents() {
        return students.stream().sorted(Comparator.comparingInt(Student::id)).toList();
    }

    private int getFreeId() {
        return idManager.getFreeId();
    }

    public void addStudent(StudentWithoutId studentWithoutId) {
        Student newStudent = new Student(getFreeId(), studentWithoutId);
        students.add(newStudent);
        eventPublisher.publishEvent(new StudentAddedEvent(this, newStudent));
    }

    public void removeStudent(int id) throws NoSuchElementException {
        List<Student> studentsToRemove = students.stream().filter(student -> student.id() == id).toList();
        if (studentsToRemove.isEmpty()) {
            throw new NoSuchElementException("Student with id " + id + " was not found");
        } else {
            studentsToRemove.forEach(studentToRemove -> {
                students.remove(studentToRemove);
                eventPublisher.publishEvent(new StudentRemovedEvent(this, studentToRemove.id()));
            });
        }
    }

    public void clearAll() throws NoSuchElementException {
        if (students.isEmpty()) {
            throw new NoSuchElementException("List of students is empty");
        } else {
            students.clear();
        }
    }
}
