package ru.skillbox.albert_raianov.spring_2_7_student_list.app;

import lombok.Getter;
import org.springframework.context.ApplicationEvent;

@Getter
public class StudentRemovedEvent extends ApplicationEvent {
    private final int removedStudentId;

    public StudentRemovedEvent(Object source, int removedStudentId) {
        super(source);
        this.removedStudentId = removedStudentId;
    }
}
