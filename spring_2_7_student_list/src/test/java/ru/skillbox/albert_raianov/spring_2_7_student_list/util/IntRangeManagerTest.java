package ru.skillbox.albert_raianov.spring_2_7_student_list.util;

import org.junit.jupiter.api.Test;

import java.util.Random;
import java.util.TreeSet;

import static org.junit.jupiter.api.Assertions.assertEquals;

class IntRangeManagerTest {
    @Test
    public void bigTest() {
        Random random = new Random();
        int[] lens = new int[]{3, 10, 30, 100, 300, 1000};
        for (int len : lens) {
            int nTests = len * len;
            IntRangeManager intRangeManager = new IntRangeManager();
            TreeSet<Integer> controlData = new TreeSet<>();
            for (int i = 0; i < nTests; ++i) {
                boolean isAdding = random.nextBoolean();
                if (isAdding) {
                    int valueToAdd = random.nextInt(1, len);
                    intRangeManager.addInt(valueToAdd);
                    controlData.add(valueToAdd);
                } else {
                    int valueToRemove = random.nextInt(1, len);
                    intRangeManager.removeInt(valueToRemove);
                    controlData.remove(valueToRemove);
                }
                assertEquals(getFirstAbsentValue(controlData), intRangeManager.getFirstFreeValue());
            }
        }
    }

    private int getFirstAbsentValue(TreeSet<Integer> controlData) {
        int result = 1;
        while (controlData.contains(result))
            ++result;
        return result;
    }
}