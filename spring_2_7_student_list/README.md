# Task 2.7: Interactive list of students
This application stores a list of students in RAM.  

## Build and startup

This application is supposed to be built by Gradle. You can build a JAR-file by a command:  
`gradle buildJar`  
After successful build you can launch an application JAR file:  
`java -jar build/libs/spring_2_7_student_list-0.0.1-SNAPSHOT.jar`  

Maybe you prefer to launch the application via Docker. You can build an image by command:  
`docker build -t spring_2_7_student_list .`  
After build, you can create a container from the image and launch it:  
`docker run --rm -it spring_2_7_student_list`

## Available commands

1. `list-students`: to list all students stored at the moment;  
2. `add-student --name "Given Name" --surname "Family Name" --age 42`: to add a student with specified name, surname
and age;  
3. `remove-student --id 1`: to remove a student with id 1;  
4. `remove-all`: to clear all data stored in RAM at the moment.  
The task does not give requirements to persist data between launches of the program.

## Configuration

You may disable an automatic addition of hardcoded records per startup. Initially this option is set
at `src/main/resources/application.yaml` by key `student-list.hardcoded-records-inserter.enabled`.
But much more convenient option is to override an environment variable `HARDCODED_RECORDS_INSERTER_ENABLED` to `false`.
You can do it with JAR file:  
`java -DHARDCODED_RECORDS_INSERTER_ENABLED=false -jar build/libs/spring_2_7_student_list-0.0.1-SNAPSHOT.jar`  
And with Docker image:  
`docker run --rm -it -e HARDCODED_RECORDS_INSERTER_ENABLED=false spring_2_7_student_list`

## Copyright

© Albert Rayanov, December 2023, city of Darmstadt (Hesse, Germany).  
[My Java repository](https://gitlab.com/northsoft/java-tasks), 
[my CV](https://docdro.id/AjhwsdR),
[my LinkedIn](https://www.linkedin.com/in/albert-rayanov/).