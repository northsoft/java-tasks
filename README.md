# Java Tasks

Tasks from Java Developer course 
at [Skillbox.ru](https://skillbox.ru/course/profession-java/), solved by me
(Albert Rayanov, [CV](https://docdro.id/AjhwsdR)). Tasks are sorted
for more recent to go first.

## List of contacts
https://gitlab.com/northsoft/java-tasks/-/tree/main/spring_3_8_contactlist  
**Task:** to create a web application with list of contacts, with three
different persistence providers (in-memory, JDBC and JOOQ).  
**Tools used:** Spring Boot, Spring Web MVC, Thymeleaf, JdbcTemplate, JOOQ,
Docker Compose, Bash.  
**Interesting:** exception wrapper (`util.ExceptionWrapper`) to wrap
all intercepted exceptions to something corresponding to a tier of code
where an exception was caught; lots of JavaDocs for interfaces
and exception wrappers; Bash script to create local copies of generated
web pages, for having no need to rebuild application after edits in CSS.  

## Interactive list of students
https://gitlab.com/northsoft/java-tasks/-/tree/main/spring_2_7_student_list  
**Task:** to implement a list of students manageable interactively
from command line  
**Tools used:** Spring Boot, Spring Shell, event publishing and listening,
Docker, Gradle  
**Interesting:** manager of ranges of integers 
(`util.IntRangeManager`)
to reduce complexity of search of unused IDs.

## Console-based phonebook
https://gitlab.com/northsoft/java-tasks/-/tree/main/spring_1_8_phonebook  
**Task:** to implement a CLI-based phonebook with menu  
**Tools used:** Spring Core (no Spring Boot), jUnit, Log4j2,
YAML configuration, profiles, properties, records, function objects  
**Interesting:** console interaction proxy for testing purposes,
almost 100% coverage by unit tests.

## Form validation via Spring MVC
https://gitlab.com/northsoft/java-tasks/-/tree/main/old_spring_02_06  
**Task:** to add validation to given form, and to add form to remove items
by regular expression  
**Tools used:** Spring MVC, Thymeleaf, Streams API, Log4j2  

## Interaction with Scored set
https://gitlab.com/northsoft/java-tasks/-/tree/main/task_20_05  
**Task:** to show most cheap and most expensive cities to visit, and to loop
through user IDs with updating timestamp of their visit  
**Tools used:** Redis DB, Redisson  
**Interesting:** TWR-singletone.

## ToDo list
https://gitlab.com/northsoft/java-tasks/-/tree/main/task_19_09  
**Task:** to create an online todo list with REST API  
**Tools used:** Spring Boot, Spring MVC, Thymeleaf, jQuery, CrudRepository.

## Course data retrieval
https://gitlab.com/northsoft/java-tasks/-/tree/main/task_16_10  
**Task:** to fetch a data from MariaDB via Hibernate, output it to console  
**Tools used:** Hibernate  
**Interesting:** cascaded output to console, implementations of abstract class
used to build menu-based CLI

## Metro map data transform
https://gitlab.com/northsoft/java-tasks/-/tree/main/task_15_14  
**Task:** to fetch a data from HTML DOM Online, output it as JSON,
parse JSON again, output data to console  
**Tools used:** Streams API, Log4j, Json.simple, Jsoup  
**Interesting:** coded an algorithm to split a graph to set of complete subgraphs
